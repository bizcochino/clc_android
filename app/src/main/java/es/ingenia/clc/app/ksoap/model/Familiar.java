package es.ingenia.clc.app.ksoap.model;

import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

/**
 * Clase para modelar la información de los familiares obtenidos desde el servicio web de CLC.
 *
 * @author David Díaz
 * @version 1.0, 15/07/2014
 */
public class Familiar implements KvmSerializable {

    /**
     * Para trazas.
     */
    // private static final String TAG = "Familiar";

    // perscorrel
    // <nombre>Marcelo Arias Rojas</nombre>
    // <parentezco>Titular</parentezco>
    // <sexo>M</sexo>
    // <rut>13068862</rut>
    // <dv>4</dv>

    /**
     * persCorrel.
     */
    private String persCorrel;

    /**
     * Nombre.
     */
    private String nombre;

    /**
     * Parentesco.
     */
    private String parentesco;

    /**
     * Sexo.
     */
    private String sexo;

    /**
     * RUT.
     */
    private String rut;

    /**
     * dv.
     */
    private String dv;

    /**
     * 
     */
    public Familiar() {

    }

    /**
     * @param soapObject
     *            objeto.
     */
    public Familiar(final SoapObject soapObject) {

        if (soapObject == null) {
            return;
        }
        // perscorrel
        // <nombre>Marcelo Arias Rojas</nombre>
        // <parentezco>Titular</parentezco>
        // <sexo>M</sexo>
        // <rut>13068862</rut>
        // <dv>4</dv>

        persCorrel = soapObject.getAttribute("pers_correl").toString();

        loadNombre(soapObject);

        loadParentesco(soapObject);

        loadSexo(soapObject);

        loadRut(soapObject);

        loadDv(soapObject);

    }

    /**
     * @param soapObject
     */
    private void loadNombre(final SoapObject soapObject) {
        if (soapObject.hasProperty("nombre")) {
            final Object obj = soapObject.getProperty("nombre");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj;
                nombre = j.toString();
            } else if (obj instanceof String) {
                nombre = (String) obj;
            }
        }
    }

    /**
     * @param soapObject
     */
    private void loadParentesco(final SoapObject soapObject) {
        // #OJOCUIDAO, hay una errata en el campo XML
        if (soapObject.hasProperty("parentezco")) {
            final Object obj = soapObject.getProperty("parentezco");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj;
                parentesco = j.toString();
            } else if (obj instanceof String) {
                parentesco = (String) obj;
            }
        }
    }

    /**
     * @param soapObject
     */
    private void loadSexo(final SoapObject soapObject) {
        if (soapObject.hasProperty("sexo")) {
            final Object obj = soapObject.getProperty("sexo");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj;
                sexo = j.toString();
            } else if (obj instanceof String) {
                sexo = (String) obj;
            }
        }
    }

    /**
     * @param soapObject
     */
    private void loadRut(final SoapObject soapObject) {
        if (soapObject.hasProperty("rut")) {
            final Object obj = soapObject.getProperty("rut");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj;
                rut = j.toString();
            } else if (obj instanceof String) {
                rut = (String) obj;
            }
        }
    }

    /**
     * @param soapObject
     */
    private void loadDv(final SoapObject soapObject) {
        if (soapObject.hasProperty("dv")) {
            final Object obj = soapObject.getProperty("dv");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj;
                dv = j.toString();
            } else if (obj instanceof String) {
                dv = (String) obj;
            }
        }
    }

    @Override
    public Object getProperty(final int index) {

        switch (index) {
            case 0:
                return persCorrel;
            case 1:
                return nombre;
            case 2:
                return parentesco;
            case 3:
                return sexo;
            case 4:
                return rut;
            case 5:
                return dv;
            default:
                return null;
        }

    }

    @Override
    public int getPropertyCount() {
        return 6;
    }

    @Override
    public void getPropertyInfo(final int index, @SuppressWarnings("rawtypes") Hashtable arg1, final PropertyInfo info) {

        switch (index) {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "perscorrel";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "nombre";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "parentezco";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "sexo";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "rut";
                break;
            case 5:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "dv";
                break;
            default:
                break;
        }
    }

    @Override
    public void setProperty(final int index, final Object value) {

        switch (index) {
            case 0:
                persCorrel = value.toString();
                break;
            case 1:
                nombre = value.toString();
                break;
            case 2:
                parentesco = value.toString();
                break;
            case 3:
                sexo = value.toString();
                break;
            case 4:
                rut = value.toString();
                break;
            case 5:
                dv = value.toString();
                break;
            default:
                break;

        }
    }

    /**
     * Devuelve el persCorrel.
     *
     * @return
     */
    public String getPersCorrel() {
        return persCorrel;
    }

    /**
     * Devuelve el nombre.
     * 
     * @return
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Devuelve el parentesco.
     * 
     * @return
     */
    public String getParentesco() {
        return parentesco;
    }
}
