package es.ingenia.clc.app.banners;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.util.TypedValue;
import android.widget.ImageView;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.ksoap.model.Banner;

/**
 * Cargador de imágenes de los banners.
 *
 * @author David Díaz
 * @version 1.0, 13/10/2014
 */
public class ImageLoader {

    /**
     * Para trazas.
     */
    private static final String          TAG         = "ImageLoader";

    /**
     * Caché de imágenes en memoria.
     */
    private final MemoryCache            memoryCache = new MemoryCache();

    /**
     * 
     */
    private final Map<ImageView, String> imageViews  = Collections
                                                             .synchronizedMap(new WeakHashMap<ImageView, String>());

    /**
     * 
     */
    private final ExecutorService        executorService;

    /**
     * Manejador para mostrar las imágenes en el thread UI.
     */
    private final Handler                handler     = new Handler();

    /**
     * Identificador de la imagen por defecto
     */
    private final int                    defaultImageId;

    /**
     * Constructor.
     * <p/>
     * Invocado desde el ScreenSlidePagerAdapter.
     */
    public ImageLoader() {
        executorService = Executors.newFixedThreadPool(5);
        // El banner por defecto depende de si es teléfono o tableta
        defaultImageId = (CLCApp.isPortrait() ? R.drawable.banner_home_defecto : R.drawable.banner_home_defecto_tableta);
    }

    /**
     * Descarga las imágenes contenidas en la lista de las URLs de los banners.
     * <p/>
     * 
     * @param listaBanners
     *            lista de URLs de los banners
     * @param bannerConfig
     *            configuración de pantalla (xhdpi, etc.)
     */
    public void downloadImages(final List<Banner> listaBanners, final int bannerConfig) {

        // La foto no está en la caché de memoria, así que la encolamos para descarga y, mientras tanto, ponemos la
        // imagen por defecto
        for (final Banner banner : listaBanners) {
            queuePhoto(banner.getBannerConfiguration(bannerConfig));
        }
    }

    /**
     * Muestra en un elemento ImageView la imagen de una determinada URL.
     * <p/>
     * Es llamado desde el onResume() de la clase ScreenSlidePageFragment.
     * 
     * @param url
     *            URL desde la que leer la imagen.
     * @param imageView
     *            elemento ImageView.
     */
    public void displayImage(final String url, final ImageView imageView) {

        // Añadimos al mapa el imageView asociado a su URL correspondiente
        imageViews.put(imageView, url);

        // Intentamos leer el contenido asociado a esa URL en la caché de memoria.
        final Bitmap bitmap = memoryCache.get(url);

        // Si la imagen obtenida de la caché no es nula, la asignamos al ImageView
        if (bitmap != null) {

            imageView.setImageBitmap(bitmap);
            int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, bitmap.getWidth(), CLCApp
                    .getAppContext().getResources().getDisplayMetrics());
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, bitmap.getHeight(), CLCApp
                    .getAppContext().getResources().getDisplayMetrics());
            imageView.setMinimumWidth(width);
            imageView.setMinimumHeight(height);

        } else {
            // La foto no está en la caché de memoria, así que la encolamos para descarga y, mientras tanto, ponemos la
            // imagen por defecto
            if (url != null) {
                queuePhoto(url, imageView);
            }
            imageView.setImageResource(defaultImageId);
        }
    }

    /**
     * Añade una imagen a la cola de descargas, indicando además en qué ImageView debería mostrarse.
     * 
     * @param url
     *            URL desde la que leer la imagen.
     * @param imageView
     *            elemento ImageView donde mostrar la imagen.
     */
    private void queuePhoto(final String url, final ImageView imageView) {

        // Tenemos una nueva imagen que cargar. La incluimos con su url y el ImageView asociado.
        final ImageToLoad p = new ImageToLoad(url, imageView);
        // Lanzamos la tarea para que se ejecute en segundo plano.
        executorService.submit(new PhotosLoader(p));

    }

    /**
     * Añade una imagen a la cola de descargas.
     * 
     * @param url
     *            URL desde la que leer la imagen.
     */
    private void queuePhoto(final String url) {

        // Tenemos una nueva imagen que cargar.
        final ImageToLoad p = new ImageToLoad(url);
        // Lanzamos la tarea para que se ejecute en segundo plano.
        executorService.submit(new PhotosLoader(p));
    }

    /**
     * Método para escalar una imagen. De momento no se utiliza.
     * 
     * @param f
     *            fichero.
     * @return
     */
    private Bitmap decodeFile(final File f) {

        try {
            // decode image size
            final BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            final FileInputStream stream1 = new FileInputStream(f);
            BitmapFactory.decodeStream(stream1, null, o);
            stream1.close();

            // Find the correct scale value. It should be the power of 2.
            // final int REQUIRED_SIZE = 4096; //CLCApp.getScreenWidth()*2;
            // Log.i(TAG, "required_size: " + REQUIRED_SIZE);
            // int width_tmp = o.outWidth, height_tmp = o.outHeight;
            // Log.i(TAG, "width_tmp: " + width_tmp + ", height_tmp: " + height_tmp);
            // int scale = 1;
            // while (true) {
            // if (width_tmp / 2 < REQUIRED_SIZE || height_tmp / 2 < REQUIRED_SIZE) {
            // break;
            // }
            // width_tmp /= 2;
            // height_tmp /= 2;
            // scale *= 2;
            // Log.i(TAG, "width_tmp: " + width_tmp + ", height_tmp: " + height_tmp);
            // Log.i(TAG, "scale: " + scale);
            // }

            // decode with inSampleSize
            // final BitmapFactory.Options o2 = new BitmapFactory.Options();
            // Log.i(TAG, "scale: " + scale);
            // o2.inSampleSize = scale;
            final FileInputStream stream2 = new FileInputStream(f);
            final Bitmap bitmap = BitmapFactory.decodeStream(stream2); // stream2, null, o2);
            stream2.close();

            return Bitmap.createBitmap(bitmap);

        } catch (final FileNotFoundException fnfe) {
            Log.e(TAG, Log.getStackTraceString(fnfe));
        } catch (final IOException ioe) {
            Log.e(TAG, Log.getStackTraceString(ioe));
        }
        return null;
    }

    /**
     * @param photoToLoad
     * @return
     */
    boolean imageViewReused(final ImageToLoad photoToLoad) {

        final String tag = imageViews.get(photoToLoad.getImageView());
        if (tag == null || !tag.equals(photoToLoad.getUrl())) {
            Log.i(TAG, "imageViewReused true");
            return true;
        }

        Log.i(TAG, "imageViewReused false");
        return false;

    }

    /**
     * Tarea para la descarga de imágenes.
     *
     * @author David Díaz
     * @version 1.0, 13/10/2014
     */
    class PhotosLoader implements Runnable {

        /**
         * Datos de la imagen a cargar.
         */
        private final ImageToLoad photoToLoad;

        /**
         * Constructor.
         *
         * @param photo
         *            Foto a cargar.
         */
        PhotosLoader(final ImageToLoad photo) {
            this.photoToLoad = photo;
        }

        @Override
        public void run() {

            try {
                if (photoToLoad.getImageView() != null) {

                    // Hay que descargar la imagen y mostrarla
                    if (downloadAndDisplay()) {
                        return;
                    }

                } else {

                    justDownload();

                }

            } catch (final Throwable th) {
                Log.e(TAG, Log.getStackTraceString(th));
            }

        }

        /**
         * Descarga una imagen desde su URL, almacena el bitmap en la caché de memoria y muestra la imagen en el
         * ImageView correspondiente.
         * 
         * @return
         */
        private boolean downloadAndDisplay() {

            Log.e(TAG, "DOWNLOAD AND DISPLAY");

            if (imageViewReused(photoToLoad)) {
                return true;
            }
            // Hay que cargar la imagen desde la URL que corresponda
            if (photoToLoad.getUrl() != null) {

                Log.i(TAG, "Hay que cargar la imagen desde la URL: " + photoToLoad.getUrl());

                // Hay que cargar la imagen desde la URL que corresponda
                final Bitmap bmp = getBitmapFromURL(photoToLoad.getUrl());

                // Añadimos la imagen al mapa. La clave será la URL
                if (bmp != null) {
                    memoryCache.put(photoToLoad.getUrl(), bmp);
                }

                if (imageViewReused(photoToLoad)) {
                    return true;
                }

                // Una vez que tenemos la imagen, habrá que mostrarla
                Log.i(TAG, "Vamos a mostrar la imagen, llamando al BitmapDisplayer");
                final BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);

                handler.post(bd);

            } else {
                Log.e(TAG, "ERROR GRAVE: ¡la url es nula!");
            }

            return false;
        }

        /**
         * Descarga una imagen desde su URL y almacena el bitmap en la caché de memoria.
         */
        private void justDownload() {

            Log.e(TAG, "JUST DOWNLOAD");

            // Hay que cargar la imagen desde la URL que corresponda
            if (photoToLoad.getUrl() != null) {

                Log.i(TAG, "Hay que cargar la imagen desde la URL: " + photoToLoad.getUrl());

                final Bitmap bmp = getBitmapFromURL(photoToLoad.getUrl());

                Log.i(TAG, "Añadimos el bmp a nuestra caché de memoria");

                // Añadimos la imagen al mapa. La clave será la URL
                if (bmp != null) {
                    memoryCache.put(photoToLoad.getUrl(), bmp);
                }

            } else {
                Log.e(TAG, "ERROR GRAVE: ¡la url es nula!");
            }

        }

        /**
         * Lee una imagen desde una URL y devuelve un Bitmap.
         *
         * @param url
         *            URL desde la que leer la imagen.
         * @return
         */
        private Bitmap getBitmapFromURL(String url) {

            InputStream input = null;

            try {

                final URL imageUrl = new URL(url);
                final HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
                conn.setConnectTimeout(30000);
                conn.setReadTimeout(30000);
                conn.setInstanceFollowRedirects(true);
                conn.setDoInput(true);
                conn.connect();
                input = conn.getInputStream();

                return BitmapFactory.decodeStream(input);

            } catch (final IOException ioe) {

                Log.e(TAG, Log.getStackTraceString(ioe));

                return null;

            } catch (final Throwable ex) {

                Log.e(TAG, Log.getStackTraceString(ex));
                if (ex instanceof OutOfMemoryError) {
                    // Hay que borrar la caché
                    Log.i(TAG, "ImageLoader - Hay que borrar la caché");
                    memoryCache.clearMemoryCache();
                }

                return null;

            } finally {

                if (input != null) {
                    try {
                        input.close();
                    } catch (final IOException ioe) {
                        Log.e(TAG, Log.getStackTraceString(ioe));
                    }
                }

            }

        }

    }

    /**
     * Clase usada para mostrar la imagen sobre el thread de UI.
     * <p/>
     * Es usada desde el run() del ImagenLoader.PhotosLoader
     * 
     * @author David Díaz
     * @version 1.0, 13/10/2014
     */
    private class BitmapDisplayer implements Runnable {

        /**
         * Imagen.
         */
        private final Bitmap      bitmap;

        /**
         * 
         */
        private final ImageToLoad photoToLoad;

        /**
         * Constructor.
         * 
         * @param b
         * @param p
         */
        public BitmapDisplayer(final Bitmap b, final ImageToLoad p) {
            bitmap = b;
            photoToLoad = p;
        }

        /**
         * 
         */
        public void run() {

            if (imageViewReused(photoToLoad)) {
                return;
            }
            if (bitmap != null) {
                // OJO, hay que ajustar las dimensiones del ImageView teniendo en cuenta la densidad de la pantalla, si
                // no, la imagen sale demasiado pequeña
                int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, bitmap.getWidth(), CLCApp
                        .getAppContext().getResources().getDisplayMetrics());
                int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, bitmap.getHeight(), CLCApp
                        .getAppContext().getResources().getDisplayMetrics());
                photoToLoad.getImageView().setMinimumWidth(width);
                photoToLoad.getImageView().setMinimumHeight(height);
                photoToLoad.getImageView().setImageBitmap(bitmap);
            } else {
                photoToLoad.getImageView().setImageResource(defaultImageId);
            }

        }
    }

    /**
     * Imagen a cargar.
     * 
     * @author David Díaz
     * @version 1.0, 13/10/2014
     */
    private class ImageToLoad {

        /**
         * URL de la imagen.
         */
        private final String    mUrl;

        /**
         * Elemento ImageView donde mostrar la imagen.
         */
        private final ImageView mImageView;

        /**
         * Constructor.
         *
         * @param url
         *            URL.
         */
        public ImageToLoad(final String url) {
            this(url, null);
        }

        /**
         * Constructor.
         *
         * @param url
         *            URL.
         * @param iv
         *            ImageView.
         */
        public ImageToLoad(final String url, final ImageView iv) {

            this.mUrl = url;
            mImageView = iv;
        }

        /**
         *
         */
        public String getUrl() {
            return mUrl;
        }

        /**
         * @return
         */
        public ImageView getImageView() {
            return mImageView;
        }
    }

    /**
     * Caché de memoria.
     *
     * @author David Díaz
     * @version 1.0, 13/10/2014
     */
    private static class MemoryCache {

        /**
         * Para trazas.
         */
        private static final String       TAG   = "MemoryCache";

        /**
         * Last argument true for LRU ordering.
         */
        private final Map<String, Bitmap> cache = Collections.synchronizedMap(new LinkedHashMap<String, Bitmap>(10,
                                                        1.5f, true));

        /**
         * Current allocated size.
         */
        private long                      size  = 0;

        /**
         * Max memory in bytes.
         */
        private long                      limit = 1000000;

        /**
         * Consructor.
         */
        public MemoryCache() {
            // use 25% of available heap size
            setLimit(Runtime.getRuntime().maxMemory() / 4);
        }

        /**
         * Fija cuál será el tamaño máximo de memoria a utilizar.
         *
         * @param newLimit
         *            nuevo límite.
         */
        private void setLimit(final long newLimit) {
            limit = newLimit;
            Log.i(TAG, "MemoryCache will use up to " + limit / 1024. / 1024. + "MB");
        }

        /**
         * Devuelve una imagen de la caché
         *
         * @param id
         *            identificador de la imagen.
         * @return
         */
        public Bitmap get(final String id) {

            try {
                // Si la caché no contiene lo que buscamos, devolvemos null.
                if (!cache.containsKey(id)) {
                    return null;
                }
                // NullPointerException sometimes happen here http://code.google.com/p/osmdroid/issues/detail?id=78
                return cache.get(id);
            } catch (final NullPointerException ex) {
                Log.e(TAG, Log.getStackTraceString(ex)); // En caso de error devolvemos null.
                return null;
            }

        }

        /**
         * Inserta un elemento en la caché de imágenes
         *
         * @param id
         *            identificador de la imagen.
         * @param bitmap
         *            imagen.
         */
        protected void put(final String id, final Bitmap bitmap) {

            try {
                // Si el elemento ya estaba lo actualizaremos, y recalculamos el tamaño total descontando el de la
                // imagen a
                // sustituir.
                if (cache.containsKey(id)) {
                    size -= getSizeInBytes(cache.get(id));
                }
                // Insertamos
                cache.put(id, bitmap);
                // Actualizamos el tamaño
                size += getSizeInBytes(bitmap);
                // Comprobamos
                checkMemoryCacheSize();
            } catch (final Throwable th) {
                Log.e(TAG, Log.getStackTraceString(th));
            }

        }

        /**
         * Comprueba el tamaño de la caché, y elimina elementos si se supera el tamaño máximo permitido.
         */
        protected void checkMemoryCacheSize() {
            Log.i(TAG, "cache size=" + size + ", number of elements=" + cache.size());

            // Si superamos el límite...
            if (size > limit) {
                final Iterator<Map.Entry<String, Bitmap>> iter = cache.entrySet().iterator();
                while (iter.hasNext()) {
                    final Map.Entry<String, Bitmap> entry = iter.next();
                    size -= getSizeInBytes(entry.getValue());
                    iter.remove();
                    // Vamos eliminando elementos hasta que estemos dentro de los límites
                    if (size <= limit) {
                        break;
                    }
                }
                Log.i(TAG, "Clean cache. New size " + cache.size());
            }
        }

        /**
         * Borra la caché.
         * <p/>
         * Es llamado desde la clase ImageLoader.
         */
        protected void clearMemoryCache() {
            try {
                // NullPointerException sometimes happen here http://code.google.com/p/osmdroid/issues/detail?id=78
                cache.clear();
                size = 0;
            } catch (final NullPointerException ex) {
                Log.e(TAG, Log.getStackTraceString(ex));
            }
        }

        /**
         * Devuelve el tamaño en bytes de una imagen.
         *
         * @param bitmap
         *            imagen.
         * @return
         */
        private long getSizeInBytes(final Bitmap bitmap) {

            if (bitmap == null) {
                return 0;
            }
            return bitmap.getRowBytes() * bitmap.getHeight();

        }

    }
}
