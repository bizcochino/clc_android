package es.ingenia.clc.app.fragments.interfaces;

/**
 * Created by David on 22/09/14.
 */
public interface InterfaceCustomFragment {

    /**
     *
     * @return
     */
    boolean onBackPressed();

}
