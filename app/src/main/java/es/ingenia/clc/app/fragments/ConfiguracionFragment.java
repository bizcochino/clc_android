package es.ingenia.clc.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.PreferencesStore;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.TabConstants;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.view.OswaldEditText;
import es.ingenia.clc.app.view.OswaldTextView;

/**
 * Clase que gestiona la pantalla de configuración (número de teléfono / llamada de rescate).
 *
 * @author David Díaz
 * @version 1.0, 23/06/2014
 */
public class ConfiguracionFragment extends CustomFragment {

    /**
     * Para trazas.
     */
    private static final String TAG             = "ConfiguracionFragment";

    /**
     * Longitud mínima que debe tener el número de teléfono.
     */
    private static final int    LONG_MINIMA_TLF = 12;

    /**
     * LinearLayout que contiene la pantalla que indica al usuario que teclee su teléfono.
     */
    private LinearLayout        llSinConfigurar;

    /**
     * LinearLayout para cuando ya se ha almacenado el teléfono.
     */
    private LinearLayout        llConfigurado;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_configuracion, container, false);

    }

    @Override
    public void onActivityCreated(final Bundle savedInstance) {

        super.onActivityCreated(null);

        // Fijamos el texto de la barra superior
        final OswaldTextView tvTitle = (OswaldTextView) getView().findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.configuracion);

        // Programamos el botón atrás
        final ImageView ivLogoCLC = (ImageView) getView().findViewById(R.id.ivLogoCLC);
        ivLogoCLC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final EditText etTelefono = (EditText) getView().findViewById(R.id.etTelefono);
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etTelefono.getWindowToken(), 0);

                getMActivity().onBackPressed();
                CLCApp.setCurrentFragmentId(TabConstants.FRAG_1_OPCIONES);

            }
        });

        // Estos son los dos layouts que conviven (mostraremos uno u otro)
        llSinConfigurar = (LinearLayout) getView().findViewById(R.id.llSinConfigurar);
        llConfigurado = (LinearLayout) getView().findViewById(R.id.llConfigurado);

        // Configuramos las acciones de los elementos de ambos layouts
        setUpPendingConfScreen();

        // Obtenemos los datos almacenados en las preferencias
        PreferencesStore.restorePreferences();
        final String tlf = PreferencesStore.getCellNumber();

        setUpExistingConfScreen(tlf);

        // Si no había teléfono almacenado...
        if ("".equals(tlf)) {

            // Mostramos el layout para que el usuario teclee el suyo y ocultamos el otro
            llSinConfigurar.setVisibility(View.VISIBLE);
            llConfigurado.setVisibility(View.GONE);

            // Pedimos el foco y mostramos el teclado al cargar pantalla
            final EditText etTelefono = (EditText) getView().findViewById(R.id.etTelefono);
            etTelefono.requestFocus();
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(etTelefono, InputMethodManager.SHOW_IMPLICIT);

        } else {

            // Y aquí, hacemos lo contrario
            llSinConfigurar.setVisibility(View.GONE);
            llConfigurado.setVisibility(View.VISIBLE);

        }

        // Google Analytics... dejamos constancia de la vista de la página
        Utils.notificaGoogleAnalytics(Constants.SCREEN_CONFIGURACION,
            Constants.SCREEN_CONFIGURACION, Constants.EVENT_LABEL_NA);

    }

    /**
     * Formulario a mostrar en el caso de que el usuario aún no haya configurado el teléfono.
     */
    private void setUpPendingConfScreen() {

        final EditText etTelefono = (EditText) getView().findViewById(R.id.etTelefono);
        etTelefono.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(final TextView textView, final int actionId, final KeyEvent event) {

                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    handled = true;
                }
                try {
                    // Para ocultar el teclado
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
                } catch (final Exception ex) {
                    Log.e(TAG, Log.getStackTraceString(ex));
                }
                return handled;

            }
        });

        // Botón de activar (aunque es un TextView, en realidad)
        final OswaldTextView tvActivar = (OswaldTextView) getView().findViewById(R.id.tvActivar);
        tvActivar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                onClickTvActivar(etTelefono);
            }
        });

    }

    /**
     * @param etTelefono
     */
    private void onClickTvActivar(final EditText etTelefono) {
        final String value = etTelefono.getText().toString();

        // Hay que verificar el tamaño mínimo
        if (value.length() >= LONG_MINIMA_TLF) {

            // Almacenamos el valor en las preferencias
            PreferencesStore.savePreferences(value);

            // Actualizamos el número de teléfono en el campo correspondiente
            final OswaldTextView tvTelefono = (OswaldTextView) getView().findViewById(R.id.tvTelefono);
            tvTelefono.setText(value);

            // Informamos al usuario
            Utils.showOkDialogWithText(getActivity(), getString(R.string.configuracion),
                    getString(R.string.num_guardado));

            // Para ocultar el teclado
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etTelefono.getWindowToken(), 0);

            // Actualizamos la pantalla
            llSinConfigurar.setVisibility(View.GONE);
            llConfigurado.setVisibility(View.VISIBLE);

            // Google Analytics...
            Utils.notificaGoogleAnalytics(Constants.SCREEN_CONFIGURACION,
                Constants.EVENT_ACTION_ACTIVAR, Constants.EVENT_LABEL_NA);

        } else {
            // Mensaje de error al usuario
            Utils.showOkDialogWithText(getActivity(), getString(R.string.configuracion),
                    getString(R.string.introduzca_num));
        }
    }

    /**
     * Pantalla en el caso de que el usuario ya hubiese configurado el teléfono.
     *
     * @param numTlf
     *            teléfono a mostrar.
     */
    private void setUpExistingConfScreen(final String numTlf) {

        final OswaldTextView tvTelefono = (OswaldTextView) getView().findViewById(R.id.tvTelefono);
        tvTelefono.setText(numTlf);

        final OswaldTextView tvDesactivar = (OswaldTextView) getView().findViewById(R.id.tvDesactivar);
        tvDesactivar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                // Borramos el número en las preferencias e informamos al usuario
                PreferencesStore.savePreferences("");
                Utils.showOkDialogWithText(getActivity(), getString(R.string.configuracion),
                        getString(R.string.num_borrado));

                // Volvemos a mostrar la pantalla de configuración
                llSinConfigurar.setVisibility(View.VISIBLE);
                llConfigurado.setVisibility(View.GONE);

                // Y borramos el número de teléfono del formulario
                final OswaldEditText etTelefono = (OswaldEditText) getView().findViewById(R.id.etTelefono);
                etTelefono.setText("");
            }
        });
    }

    @Override
    public void onStop() {

        super.onStop();
        // Para ocultar el teclado
        final EditText etTelefono = (EditText) getView().findViewById(R.id.etTelefono);
        final InputMethodManager imm = (InputMethodManager) getActivity()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etTelefono.getWindowToken(), 0);

    }
}
