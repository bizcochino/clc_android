package es.ingenia.clc.app;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import es.ingenia.clc.app.ksoap.model.InicioSesion;

/**
 * Clase para la gestión de preferencias en la aplicación.
 *
 * @author David Díaz
 * @version 1.0, 09/07/14.
 */
public final class PreferencesStore {

    /**
     * Para trazas.
     */
    private static final String TAG               = "PreferencesStore";

    /**
     * Clave para el número de teléfono.
     */
    private static final String CELL_NUMBER       = "cell_number";

    /**
     * Sesión.
     */
    private static final String KEY               = "player_session";

    /**
     * Clave para la fecha de los banners.
     */
    private static final String LAST_SYNC_BANNERS = "last_sync_banners";

    /**
     * Clave para la fecha de las citas.
     */
    private static final String LAST_SYNC_CITAS   = "last_sync_citas";

    /**
     * Clave para los datos de inicio de sesión.
     */
    private static final String INICIO_SESION     = "InicioSesion";

    /**
     * Número de teléfono.
     */
    private static String       cellNumber;

    /**
     * Fecha de la última sincronización de los banners.
     */
    private static long         fechaUltimaSincBanners;

    /**
     * Fecha de la última sincronización de las citas.
     */
    private static long         fechaUltimaSincCitas;

    /**
     * Datos de la sesión de usuario.
     */
    // private static InicioSesion sesion;

    /**
     * Constructor privado.
     */
    private PreferencesStore() {

    }

    /**
     * Guarda las preferencias en el fichero.
     * 
     * @param value
     *            número de teléfono.
     * @return
     */
    public static boolean savePreferences(final String value) {

        if (CLCApp.getAppContext() != null) {
            final SharedPreferences.Editor editor = CLCApp.getAppContext()
                    .getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
            editor.putString(CELL_NUMBER, value);
            cellNumber = value;
            /*
             * // Prueba de serialización con Gson:<p/> Gson gson = new Gson(); String json =
             * gson.toJson(context.getInicioSesion()); editor.putString("InicioSesion", json);
             */
            return editor.commit();
        } else {
            return false;
        }

    }

    /**
     * Lee las preferencias del fichero.
     */
    public static void restorePreferences() {

        final SharedPreferences preferences = CLCApp.getAppContext().getSharedPreferences(KEY, Context.MODE_PRIVATE);
        cellNumber = preferences.getString(CELL_NUMBER, "");
        fechaUltimaSincBanners = preferences.getLong(LAST_SYNC_BANNERS, 0);
        fechaUltimaSincCitas = preferences.getLong(LAST_SYNC_CITAS, 0);
        /*
         * Gson gson = new Gson(); String json = preferences.getString("InicioSesion", ""); sesion = gson.fromJson(json,
         * InicioSesion.class);
         */
    }

    /**
     * Devuelve el número de teléfono almacenado.
     * 
     * @return
     */
    public static String getCellNumber() {
        return cellNumber;
    }

    /**
     * Devuelve cuál fue la última fecha en la que se sincronizaron los banners.
     * <p/>
     * Recuérdese que la sincronización no se reintenta si no ha pasado cierto periodo de tiempo (24 horas, en
     * principio).
     * 
     * @return
     */
    public static long getFechaUltimaSincBanners() {
        return fechaUltimaSincBanners;
    }

    /**
     * Fija la fecha de la última sincronización de los banners que se ha realizado con éxtio.
     * 
     * @return
     */
    public static boolean setFechaUltimaSincBanners() {

        if (CLCApp.getAppContext() != null) {
            final SharedPreferences.Editor editor = CLCApp.getAppContext()
                    .getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
            long lastSyncBanners = System.currentTimeMillis();
            editor.putLong(LAST_SYNC_BANNERS, lastSyncBanners);
            fechaUltimaSincBanners = lastSyncBanners;
            return editor.commit();
        } else {
            return false;
        }

    }

    /**
     * Devuelve cuál fue la última fecha en la que se sincronizaron las citas.
     * <p/>
     * Recuérdese que la sincronización no se reintenta si no ha pasado cierto periodo de tiempo (24 horas, en
     * principio).
     *
     * @return
     */
    public static long getFechaUltimaSincCitas() {
        return fechaUltimaSincCitas;
    }

    /**
     * Fija la fecha de la última sincronización de los banners que se ha realizado con éxtio.
     *
     * @return
     */
    public static boolean setFechaUltimaSincCitas() {

        if (CLCApp.getAppContext() != null) {
            final SharedPreferences.Editor editor = CLCApp.getAppContext()
                    .getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
            long lastSyncCitas = System.currentTimeMillis();
            editor.putLong(LAST_SYNC_CITAS, lastSyncCitas);
            fechaUltimaSincCitas = lastSyncCitas;
            return editor.commit();
        } else {
            return false;
        }

    }

    /**
     * Devuelve el objeto donde se almacena la información recibida del servicio web al iniciar sesión.
     * 
     * @return
     */
    private static InicioSesion getSession() {

        final SharedPreferences preferences = CLCApp.getAppContext().getSharedPreferences(KEY, Context.MODE_PRIVATE);

        final InicioSesion ses = new Gson().fromJson(preferences.getString(INICIO_SESION, ""), InicioSesion.class);

        return ses;

    }

    /**
     * Fija el objeto donde se almacena la información recibida del servicio web al iniciar sesión.
     * 
     * @return
     */
    private static boolean setSession() {

        final SharedPreferences.Editor editor = CLCApp.getAppContext().getSharedPreferences(KEY, Context.MODE_PRIVATE)
                .edit();

        editor.putString(INICIO_SESION, new Gson().toJson(CLCApp.getInicioSesion()));

        return editor.commit();

    }

}
