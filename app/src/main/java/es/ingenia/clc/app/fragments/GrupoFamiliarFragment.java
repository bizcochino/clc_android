package es.ingenia.clc.app.fragments;

import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.TabConstants;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.ksoap.model.Familiar;
import es.ingenia.clc.app.view.OswaldTextView;

/**
 * Clase para mostrar el fragmento con el grupo familiar.
 *
 * @author David Díaz
 * @version 1.0, 07/09/14
 */
public final class GrupoFamiliarFragment extends CustomListFragment {

    /**
     * Para trazas.
     */
    private static final String TAG = "GrupoFamiliarFragment";

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_grupo_familiar, container, false);

    }

    @Override
    public void onActivityCreated(final Bundle savedInstance) {

        super.onActivityCreated(savedInstance);

        // Fijamos el texto de la barra superior
        final OswaldTextView tvTitle = (OswaldTextView) getView().findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.grupo_familiar);

        // Programamos el botón atrás
        final ImageView ivLogoCLC = (ImageView) getView().findViewById(R.id.ivLogoCLC);
        ivLogoCLC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getMActivity().controlBackStack(TabConstants.FRAG_5_1_PROXIMAS_CITAS);
                //getFragmentManager().popBackStackImmediate();
            }
        });

        final ListView listView = (ListView) getView().findViewById(android.R.id.list);
        final FamiliaresArrayAdapter ladapter = new FamiliaresArrayAdapter(CLCApp.getAppContext(), CLCApp
                .getGrupoFamiliar().getListaFamiliares());
        listView.setAdapter(ladapter);

        // Google Analytics...
        Utils.notificaGoogleAnalytics(Constants.SCREEN_PROXIMAS_CITAS,
            Constants.EVENT_ACTION_SELECCION_UNIDAD_FAM, Constants.EVENT_LABEL_NA);

    }

    @Override
    public void onListItemClick(final ListView l, final View v, final int position, final long id) {

        CLCApp.setFamiliarSeleccionado(CLCApp.getGrupoFamiliar().getListaFamiliares().get(position));
        Log.d(TAG, "familiar seleccionado: " + CLCApp.getFamiliarSeleccionado().getPersCorrel() + ", "
                + CLCApp.getFamiliarSeleccionado().getNombre());

        // Volvemos atrás (OJO, cambio DDG 17/11/2014)
        getMActivity().controlBackStack(TabConstants.FRAG_5_1_PROXIMAS_CITAS);
        //getFragmentManager().popBackStackImmediate();

    }

    /**
     * Clase para gestionar el listado de familiares.
     *
     * @author David Díaz
     * @version 1.0, 23/06/2014
     */
    private static class FamiliaresArrayAdapter extends ArrayAdapter<Familiar> {

        /**
         * lista de familiares.
         */
        private final List<Familiar> listaFamiliares;

        /**
         * Constructor.
         *
         * @param theContext
         *            contexto de la aplicación.
         * @param items
         *            listado de elementos a mostrar.
         */
        FamiliaresArrayAdapter(final Context theContext, final List<Familiar> items) {

            super(theContext, R.layout.familiar_item, items);
            listaFamiliares = items;

        }

        /**
         * Para el relleno...
         *
         * @param aux
         *            lista con la que hacer el "rellenado".
         */
        public void refill(final List<Familiar> aux) {

            listaFamiliares.clear();
            for (final Familiar item : aux) {
                listaFamiliares.add(item);
            }
            notifyDataSetChanged();

        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {

            View view = convertView;

            if (view == null) {
                final LayoutInflater inflater = (LayoutInflater) CLCApp.getAppContext().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                // Patrón "View Holder"
                final ViewHolder viewHolder = new ViewHolder();
                view = inflater.inflate(R.layout.familiar_item, null);
                viewHolder.seleccionado = (ImageView) view.findViewById(R.id.ivSeleccionado);
                viewHolder.nombre = (OswaldTextView) view.findViewById(R.id.tvNombreFamiliar);
                view.setTag(viewHolder);
            }

            final Familiar familiar = listaFamiliares.get(position);

            if (familiar != null) {
                final ViewHolder holder = (ViewHolder) view.getTag();
                holder.nombre.setText(familiar.getNombre());
                // Si es el familiar seleccionado, mostramos a la derecha la imagen de "seleccionado"
                if (familiar.getPersCorrel().equals(CLCApp.getFamiliarSeleccionado().getPersCorrel())) {
                    holder.seleccionado.setImageResource(R.drawable.familiar_tick);
                }
            }

            return view;

        }

        /**
         * Clase estática interna para aplicar el patrón "View Holder".
         *
         * @author David Díaz García
         * @version 1.0, 23/09/2014
         */
        private static class ViewHolder {

            /**
             * Imagen que indica si está seleccionado.
             */
            private ImageView      seleccionado;

            /**
             * Nombre del familiar.
             */
            private OswaldTextView nombre;

        }
    }

}
