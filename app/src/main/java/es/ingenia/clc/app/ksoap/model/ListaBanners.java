package es.ingenia.clc.app.ksoap.model;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

import android.util.Log;

/**
 * Clase para modelar el listado de banners obtenidos desde el servicio web de CLC.
 *
 * @author David Díaz
 * @version 1.0, 15/07/2014
 */
public class ListaBanners implements KvmSerializable {

    /** Para trazas. */
    private static final String TAG = "ListaBanners";

    /**
     * Lista de banners.
     */
    private List<Banner>        listaBanners;

    // <?xml version="1.0" encoding="utf-8"?>
    // <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
    // xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    // <soap:Body>
    // <RecuperaBannersResponse xmlns="http://tempuri.org/">
    // <RecuperaBannersResult>
    // <banners>
    // <banner hora="10:20" fecha="06/12/2013" id="1">
    // <img id="ipadretina">http://www.clc.cl/appmovil/banner/image1.png</img>
    // <img id="ipad">http://www.clc.cl/appmovil/banner/image1.png</img>
    // <img id="iphoneretina">http://www.clc.cl/appmovil/banner/image1.png</img>
    // <img id="iphone">http://www.clc.cl/appmovil/banner/image1.png</img>
    // </banner>
    // <banner hora="10:20" fecha="06/12/2013" id="2">
    // <img id="ipadretina">http://www.clc.cl/appmovil/banner/image1.png</img>
    // <img id="ipad">http://www.clc.cl/appmovil/banner/image1.png</img>
    // <img id="iphoneretina">http://www.clc.cl/appmovil/banner/image1.png</img>
    // <img id="iphone">http://www.clc.cl/appmovil/banner/image1.png</img>
    // </banner>
    // </banners>
    // </RecuperaBannersResult>
    // </RecuperaBannersResponse>
    // </soap:Body>
    // </soap:Envelope>

    /**
     * Constructor sin parámetros.
     */
    // public ListaBanners() {
    // }

    /**
     * Constructor.
     *
     * @param soapObject
     *            objeto soap.
     */
    public ListaBanners(final SoapObject soapObject) {

        if (soapObject == null) {
            return;
        }

        if (soapObject.hasProperty("banners")) {
            final SoapObject object = (SoapObject) soapObject.getProperty("banners");
            final int totalCount = object.getPropertyCount();
            Log.i(TAG, "totalCount: " + totalCount);
            listaBanners = new VectorBanner(object);
        }

    }

    @Override
    public Object getProperty(final int arg0) {

        switch (arg0) {
            case 0:
                return listaBanners;
            default:
                return null;
        }

    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void getPropertyInfo(final int index, @SuppressWarnings("rawtypes") final Hashtable arg1,
            final PropertyInfo info) {

        switch (index) {
            case 0:
                info.type = PropertyInfo.VECTOR_CLASS;
                info.name = "listaBanners";
                break;
            default:
                break;
        }
    }

    @Override
    public void setProperty(final int index, final Object value) {

        switch (index) {
            case 0:
                listaBanners = (ArrayList<Banner>) value;
                break;
            default:
                break;
        }

    }

    // Nuevos métodos.

    /**
     * Devuelve la lista de citas.
     *
     * @return
     */
    public List<Banner> getListaBanners() {

        return listaBanners;

    }

    /**
     * Clase para generar un vector de objetos de tipo Banner.
     *
     * @author David Díaz
     * @version 1.0, 06/08/14
     */
    public class VectorBanner extends Vector<Banner> implements KvmSerializable {

        /**
         * Para trazas.
         */
        private static final String TAG = "VectorBanner";

        /**
         * Constructor sin parámetros.
         */
        public VectorBanner() {

        }

        /**
         * Constructor.
         * 
         * @param soapObject
         *            el objeto a tratar...
         */
        public VectorBanner(final SoapObject soapObject) {

            if (soapObject == null) {
                return;
            }
            final int size = soapObject.getPropertyCount();
            for (int i = 0; i < size; i++) {
                final Object obj = soapObject.getProperty(i);
                if (obj != null && obj.getClass().equals(SoapObject.class) && ((SoapObject) obj).hasProperty("img")) {
                    final SoapObject subobj = (SoapObject) soapObject.getProperty(i);
                    final Banner banner = new Banner(subobj);
                    add(banner);
                }
            }

        }

        @Override
        public Object getProperty(final int arg0) {
            return this.get(arg0);
        }

        @Override
        public int getPropertyCount() {
            return this.size();
        }

        @Override
        public void getPropertyInfo(final int index, @SuppressWarnings("rawtypes") final Hashtable arg1,
                final PropertyInfo info) {
            info.name = "familiar";
            info.type = Familiar.class;
        }

        @Override
        public void setProperty(final int arg0, final Object arg1) {
        }

    }

}
