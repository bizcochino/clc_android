package es.ingenia.clc.app.fragments.webviews;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import es.ingenia.clc.app.R;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.fragments.CustomFragment;

/**
 * Clase para el fragmento que muestra un "WebView" la app CLC.
 *
 * @author David Díaz
 * @version 1.0, 23/06/14
 */
class WebViewFragment extends CustomFragment {

    /**
     * Para trazas.
     */
    private static final String TAG = "WebViewFragment";

    /**
     * Barra de progreso.
     */
    private ProgressBar         mProgress;

    /**
     *
     */
    WebView                     mWebview;

    @Override
    public final void onActivityCreated(final Bundle savedInstance) {

        super.onActivityCreated(savedInstance);

        // Programamos el botón atrás
        final ImageView ivLogoCLC = (ImageView) getView().findViewById(R.id.ivLogoCLC);
        // Hay que comprobar que existe el logo...
        if (ivLogoCLC != null) {
            ivLogoCLC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // getFragmentManager().popBackStackImmediate();
                    getMActivity().onBackPressed();
                }
            });
        }

        // Barra de progreso...
        mProgress = (ProgressBar) getView().findViewById(R.id.pbWebview);
        mProgress.setVisibility(View.VISIBLE);

        // Configuramos el WebView...
        mWebview = (WebView) getView().findViewById(R.id.webView);

        mWebview.setBackgroundResource(R.drawable.fondo_webview);
        mWebview.getSettings().setJavaScriptEnabled(true);

        // Error "nativeOnDraw failed; clearing to background color."
        // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
        // mWebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        // }

        // Hay que reescribir un par de cositas... (si no, se abriría el navegador por defecto).
        mWebview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(final WebView view, final int progress) {
                if (getActivity() != null) {
                    getActivity().setProgress(progress * 1000);
                }
            }
        });
        mWebview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(final WebView view, final String theURL) {
                // Ocultamos la barra de progreso cuando se haya terminado la carga
                if (mProgress != null) {
                    Log.i(TAG, "ocultamos la barra de progreso");
                    mProgress.setVisibility(View.GONE);
                } else {
                    Log.i(TAG, "NO ocultamos la barra de progreso, porque no es visible");
                }
            }
        });

        setTitle();

    }

    @Override
    public void onStart() {
        super.onStart();
        checkAndLoad();
    }

    /**
     *
     */
    public void checkAndLoad() {

        final ProgressBar progressBar = (ProgressBar) getView().findViewById(R.id.pbWebview);

        // comprobar conexión a internet
        if (Utils.isOnline()) {
            progressBar.setVisibility(View.VISIBLE);
            invocarURL();
        } else {
            Utils.showOkDialogWithText(getActivity(), getString(R.string.aviso_no_internet),
                    getString(R.string.servicio_no_disponible));
            progressBar.setVisibility(View.GONE);
        }

    }

    /**
     *
     */
    public void setTitle() {
    }

    /**
     *
     */
    public void invocarURL() {
    }

}
