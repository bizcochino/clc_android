package es.ingenia.clc.app.activities;

import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Window;
import es.ingenia.clc.app.R;

/**
 * Created by David Díaz on 19/06/14.
 * 
 * @author David Díaz
 * @version 1.0, 23/06/2014
 */
class BaseActivity extends FragmentActivity {

    @Override
    public void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (getResources().getBoolean(R.bool.portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

    }

    @Override
    public void onAttachedToWindow() {

        super.onAttachedToWindow();
        final Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);

    }

}
