package es.ingenia.clc.app.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;

import es.ingenia.clc.app.activities.MainActivity;
import es.ingenia.clc.app.fragments.interfaces.InterfaceCustomFragment;

/**
 * Clase que extiende de Fragment. Es usada por las clases HomeFragment y OpcionesFragment.
 *
 * @author David Díaz
 * @version 1.0, 07/08/14
 */
public class CustomListFragment extends ListFragment implements InterfaceCustomFragment {

    /** Para trazas. */
    private static final String TAG = "CustomListFragment";

    /**
     *
     */
    private MainActivity        mActivity;

    /**
     * @return
     */
    MainActivity getMActivity() {
        return mActivity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) this.getActivity();
    }

    /**
     * @return
     */
    public boolean onBackPressed() {
        Log.i(TAG, "onBackPressed");
        return false;
    }

    /**
     * @param requestCode
     * @param resultCode
     * @param data
     */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

}
