package es.ingenia.clc.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import android.support.v4.app.FragmentActivity;
import android.util.Log;

import es.ingenia.clc.app.ksoap.model.Cita;
import es.ingenia.clc.app.ksoap.model.ProximasCitas;

/**
 * Clase para gestionar las citas.
 *
 * @author David Díaz
 * @version 1.0, 26/09/14
 */
public final class GestorCitas {

    /**
     * Para trazas.
     */
    private static final String TAG         = "GestorCitas";

    /**
     * Actividad.
     */
    private FragmentActivity    mActivity   = null;

    /**
     * PersCorrel del que gestionar las citas.
     */
    private String              mPersCorrel = "";

    /**
     * Constructor.
     * 
     * @param activity
     *            actividad.
     * @param persCorrel
     *            persCorrel.
     */
    public GestorCitas(final FragmentActivity activity, final String persCorrel) {

        this.mActivity = activity;
        this.mPersCorrel = persCorrel;

    }

    /**
     * Gestión de citas, comparando las nuevas con las que había guardadas en el dispositivo.
     *
     * @param citas
     *            objeto devuelto por el servicio web.
     */
    public void gestionarCitas(final ProximasCitas citas) {

        if (!comprobacionesPrevias(citas)) {
            return;
        }

        // TODO QUITAR!!!
        // if (VariantConstants.DEBUG) {
        // UtilsCitas.borrarFicheroCitasAnteriores(mPersCorrel);
        // UtilsCitas.borrarFicheroCitasNoVisualizadas(mPersCorrel);
        // UtilsCitas.borrarFicheroCitasPorNotificar();
        // }

        // Intentamos recuperar las citas almacenadas en los ficheros json. Si no hay fichero, la lista estará vacía
        final List<Cita> citasAnteriores = UtilsCitas.leerCitasAnteriores(mPersCorrel);
        // final List<Cita> citasNoVisualizadas = UtilsCitas.leerCitasNoVisualizadas(mPersCorrel);

        // Utilizaremos una lista auxiliar para la nueva lista de citas no visualizadas
        final List<Cita> citasNoVisualizadasAux = new ArrayList<Cita>();

        // Y aquí guardaremos las citas que nos ha devuelto el servicio web
        final List<Cita> citasNuevas = new ArrayList<Cita>(citas.getListaCitas());

        // OJOJOJOJOJOJOJO
        // OJOJOJOJOJOJOJO
        // OJOJOJOJOJOJOJO
        // OJOJOJOJOJOJOJO
        if (VariantConstants.DEBUG) {
            final Random rand = new Random();
            int MAX = citasNuevas.size();
            int MIN = 1;

            int randomNum1 = rand.nextInt((MAX - MIN) + 1) + MIN;

            Log.i(TAG, "Vamos a borrar de la lista la cita " + citasNuevas.get(randomNum1 - 1).getCamId());
            citasNuevas.remove(randomNum1 - 1);

            MAX = citasNuevas.size();
            int randomNum3 = rand.nextInt((MAX - MIN) + 1) + MIN;
            Log.i(TAG, "Vamos a modificar en la lista la cita " + citasNuevas.get(randomNum3 - 1).getCamId());
            Cita aux = citasNuevas.get(randomNum3 - 1);
            citasNuevas.remove(randomNum3 - 1);
            aux.setNombreMedico(aux.getNombreMedico() + "***");
            citasNuevas.add(aux);
        }

        // Hay que ordenar la lista de nuevas citas por el valor cam_id
        Collections.sort(citasNuevas);

        // Contadores
        int contadorCitasNuevas = 0;
        int contadorCitasBorradas = 0;
        int contadorCitasActualizadas = 0;

        int totalAnteriores = citasAnteriores.size();
        int totalNuevas = citasNuevas.size();

        // Índices para recorrer los listados
        int indiceAnteriores = 0;
        int indiceNuevas = 0;

        // Si ya teníamos citas guardadas, habrá que compararlas con lo nuevo que hemos obtenido
        if (citasAnteriores != null && !citasAnteriores.isEmpty()) {

            // Comparamos el array de citas anteriores con el de citas nuevas para ver cuántos avisos mostrar
            while ((indiceAnteriores < totalAnteriores) && (indiceNuevas < totalNuevas)) {

                final Cita citaAnterior = citasAnteriores.get(indiceAnteriores);
                final Cita citaNueva = citasNuevas.get(indiceNuevas);

                if (citaAnterior.equals(citaNueva)) {

                    // No hay cambios en la cita, pasamos a la siguiente
                    indiceAnteriores++;
                    indiceNuevas++;

                    // Asignamos un id a la notificación de la cita.
                    if (citaAnterior.getIdNotificacion() == 0) {
                        citaNueva.setIdNotificacion((int) (System.currentTimeMillis() % Integer.MAX_VALUE));
                    } else {
                        citaNueva.setIdNotificacion(citaAnterior.getIdNotificacion());
                    }

                    citasNoVisualizadasAux.add(citaNueva);
                    // Aquí no hay que crear la notificación, porque esta cita ya se había procesado previamente.

                } else {

                    // Ha habido algún cambio en los datos

                    // Cogemos los dos ids de las citas que estamos comparando
                    int oldCamId = Integer.parseInt(citaAnterior.getCamId());
                    int newCamId = Integer.parseInt(citaNueva.getCamId());

                    if (oldCamId == newCamId) {

                        // Significa que sus ids son iguales pero algún campo es diferente lo que significa que la
                        // cita se ha actualizado
                        contadorCitasActualizadas++;
                        indiceAnteriores++;
                        indiceNuevas++;

                        // El estado de la cita es "Modificado"
                        citaNueva.setEstado(Constants.CITA_MODIFICADA);

                        // Modificamos la notificación, pero antes hay que indicarle a la cita nueva el id de la
                        // notificación de la cita previa
                        citaNueva.setIdNotificacion(citaAnterior.getIdNotificacion());

                        // Añadimos la cita a la nueva lista de citas no visualizadas (si usáramos la misma lista, la
                        // cita estaría duplicada)
                        citasNoVisualizadasAux.add(citaNueva);

                        // Borramos la notificación de la cita y creamos una nueva
                        UtilsCitas.removeScheduledNotification(citaAnterior);
                        UtilsCitas.createScheduledNotification(citaNueva);

                    } else if (oldCamId > newCamId) {

                        // Significa que en la lista de citas nuevas hay ahora una que antes no estaba
                        contadorCitasNuevas++;
                        indiceNuevas++;

                        // Preparamos la cita, la añadimos a la lista no visualizadas, y creamos la notificación
                        prepararCitaNueva(citaNueva, citasNoVisualizadasAux);

                    } else { // oldCamId < newCamId

                        // Significa que en la lista de citas nuevas falta una que antes sí estaba
                        contadorCitasBorradas++;
                        indiceAnteriores++;

                        // El estado de la cita es "Borrada"
                        citaAnterior.setEstado(Constants.CITA_BORRADA);

                        Log.i(TAG, "ponemos estado BORRADA a cita " + citaAnterior.getCamId());
                        // Añadimos la cita a la lista de citas no visualizadas
                        citasNoVisualizadasAux.add(citaAnterior);

                        // Borramos la notificación de la cita
                        UtilsCitas.removeScheduledNotification(citaAnterior);

                    }
                }

            }

        }

        // Si no hemos llegado al final de las anteriores es que las que faltan se han borrado.
        // Las añadimos de las pendientes de notificar como borrado
        if (indiceAnteriores < totalAnteriores) {
            for (int i = indiceAnteriores; i < totalAnteriores; i++) {
                final Cita citaAnterior = citasAnteriores.get(i);
                citaAnterior.setEstado(Constants.CITA_BORRADA);
                // Todavía que mostrarla al usuario, para vea que el estado de la cita indica "borrada"
                citasNoVisualizadasAux.add(citaAnterior);
                // Borramos la notificación de la cita
                UtilsCitas.removeScheduledNotification(citaAnterior);
            }
        }

        // Si no hemos llegado al final de las nuevas es que las que faltan son
        // nuevas. Las añadimos a las pendientes de visualizar
        if (indiceNuevas < totalNuevas) {
            for (int i = indiceNuevas; i < totalNuevas; i++) {
                prepararCitaNueva(citasNuevas.get(i), citasNoVisualizadasAux);
            }
        }

        // Aquí deberíamos guardar las nuevas citas ordenadas a disco sobreescribiendo las anteriores
        if (!UtilsCitas.guardarFicheroCitasAnteriores(citasNuevas, mPersCorrel)) {
            Log.e(TAG, "ERROR: No se ha podido guardar la lista de citas nuevas");
        }

        if (!UtilsCitas.guardarFicheroCitasNoVisualizadas(citasNoVisualizadasAux, mPersCorrel)) {
            Log.e(TAG, "ERROR: No se ha podido guardar la lista de citas modificadas");
        }

        contadorCitasNuevas = contadorCitasNuevas + (totalNuevas - indiceNuevas);
        contadorCitasBorradas = contadorCitasBorradas + (totalAnteriores - indiceAnteriores);

        Log.i(TAG, "NUEVAS = " + contadorCitasNuevas + ", BORRADAS = " + contadorCitasBorradas + ", ACTUALIZADAS = "
                + contadorCitasActualizadas);

        UtilsCitas.setCirculoRojo(mActivity, citasNoVisualizadasAux.size());

    }

    /**
     * Prepara una nueva cita, asignándole el estado correspondiente y el identificador de la notificación, la añade a
     * la lista de citas no visualizadas y prepara la notificación local de aviso.
     * 
     * @param cita
     * @param listaNoVisualizadas
     */
    private void prepararCitaNueva(final Cita cita, List<Cita> listaNoVisualizadas) {

        // El estado de la cita es "Nueva"
        cita.setEstado(Constants.CITA_NUEVA);

        // Indicamos la id de la notificación que estará asociada a la cita
        cita.setIdNotificacion((int) (System.currentTimeMillis() % Integer.MAX_VALUE));

        // Añadimos la cita a la lista de citas no visualizadas
        listaNoVisualizadas.add(cita);

        // Creamos la notificación local para la cita
        UtilsCitas.createScheduledNotification(cita);

    }

    /**
     * Algunas comprobaciones previas, para saber si tenemos que seguir procesando las citas o no. Es llamado desde el
     * método gestionarCitas.
     * 
     * @param citas
     *            citas a comprobar.
     */
    private boolean comprobacionesPrevias(final ProximasCitas citas) {

        // Si no hay datos, o la lista de citas es vacía, no hacemos nada
        if (citas == null || citas.getListaCitas() == null || citas.getListaCitas().isEmpty()) {
            // No tenemos datos. Bye, bye...
            return false;
        }
        // Si la sesión de usuario ha caducado, tampoco podemos continuar
        return citas.isAutenticado();

    }

}
