package es.ingenia.clc.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import es.ingenia.clc.app.ksoap.model.Cita;
import es.ingenia.clc.app.notifs.NotificationTimeAlarm;

/**
 * Utilidades relacionadas con la gestión de citas. Clase no instanciable.
 *
 * @author David Díaz
 * @version 1.0, 16/10/14.
 */
public final class UtilsCitas {

    /**
     * Para trazas.
     */
    private static final String TAG = "UtilsCitas";

    /**
     * Constructor privado.
     */
    private UtilsCitas() {

    }

    /**
     * Guarda en un fichero el mapa de citas anteriores.
     *
     * @param lista
     *            citas a guardar.
     * @param persCorrel
     *            persCorrel, para calcular el nombre del fichero.
     */
    public static boolean guardarFicheroCitasAnteriores(final List<Cita> lista, final String persCorrel) {

        return guardarCitas(lista, persCorrel + Constants.CITAS_ACTUALES_FICHERO);

    }

    /**
     * Guarda en un fichero las citas que el usuario no ha visualizado.
     *
     * @param lista
     *            citas a guardar.
     * @param persCorrel
     *            persCorrel, para calcular el nombre del fichero.
     */
    public static boolean guardarFicheroCitasNoVisualizadas(final List<Cita> lista, final String persCorrel) {

        return guardarCitas(lista, persCorrel + Constants.CITAS_NO_VISUALIZADAS_FICHERO);

    }

    /**
     * Guarda en un fichero las citas pendientes de notificar.
     *
     * @param mapa
     *            citas a guardar.
     */
    public static boolean guardarFicheroCitasPorNotificar(final Map<String, Cita> mapa) {

        return guardarCitas(mapa);

    }

    /**
     * Guarda un fichero en formato JSON con la información de las citas, a partir de un mapa de objetos de tipo Cita.
     *
     * @param lista
     *            citas a guardar (puede ser vacía).
     * @param nombreFichero
     *            nombreFichero.
     */
    private static boolean guardarCitas(final List<Cita> lista, final String nombreFichero) {

        return Utils.escribirFichero(nombreFichero, new Gson().toJson(lista));

    }

    /**
     * Guarda un fichero en formato JSON con la información de las citas, a partir de un mapa de objetos de tipo Cita.
     * 
     * @param mapa
     *            citas a guardar (puede ser vacía).
     */
    private static boolean guardarCitas(final Map<String, Cita> mapa) {

        return Utils.escribirFichero(Constants.CITAS_POR_NOTIFICAR_FICHERO, new Gson().toJson(mapa));

    }

    /**
     * Lee el fichero de texto que contiene las citas anteriores y devuelve una lista.
     *
     * @param persCorrel
     *            persCorrel.
     * @return
     */
    public static List<Cita> leerCitasAnteriores(final String persCorrel) {

        return leerCitas(persCorrel + Constants.CITAS_ACTUALES_FICHERO);

    }

    /**
     * Lee el fichero de texto que contiene las citas que el usuario no ha visualizado y devuelve una lista.
     *
     * @param persCorrel
     *            persCorrel.
     * @return
     */
    public static List<Cita> leerCitasNoVisualizadas(final String persCorrel) {

        return leerCitas(persCorrel + Constants.CITAS_NO_VISUALIZADAS_FICHERO);

    }

    /**
     * Lee el fichero de texto que contiene las citas sin notificar y devuelve una lista.
     *
     * @return
     */
    public static Map<String, Cita> leerMapaCitasPorNotificar() {

        return leerMapaCitas();

    }

    /**
     * Lee un fichero JSON con citas y devuelve una lista con los objetos.
     *
     * @param nombreFichero
     *            nombre del fichero a leer.
     * @return
     */
    private static ArrayList<Cita> leerCitas(final String nombreFichero) {

        final String eol = System.getProperty("line.separator");
        BufferedReader input = null;

        // Por eficiencia, usamos directamente ArrayList
        ArrayList<Cita> lista = new ArrayList<Cita>();

        try {
            input = new BufferedReader(new InputStreamReader(CLCApp.getAppContext().openFileInput(nombreFichero)));
            String line;
            final StringBuffer buffer = new StringBuffer();
            while ((line = input.readLine()) != null) {
                buffer.append(line);
                buffer.append(eol);
            }
            // Aquí generamos la lista de objetos de tipo Cita
            lista = new Gson().fromJson(buffer.toString(), new TypeToken<ArrayList<Cita>>() {
            }.getType());

        } catch (final FileNotFoundException e) {
            Log.i(TAG, "El fichero " + nombreFichero + " no existe...");
        } catch (final Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (final IOException e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }

        }

        return lista;

    }

    /**
     * Lee un fichero JSON con citas y devuelve una lista con los objetos.
     *
     * @return
     */
    private static Map<String, Cita> leerMapaCitas() {

        final String eol = System.getProperty("line.separator");
        BufferedReader input = null;

        Map<String, Cita> mapa = new HashMap<String, Cita>();

        try {
            input = new BufferedReader(new InputStreamReader(CLCApp.getAppContext().openFileInput(
                    Constants.CITAS_POR_NOTIFICAR_FICHERO)));
            String line;
            final StringBuffer buffer = new StringBuffer();
            while ((line = input.readLine()) != null) {
                buffer.append(line);
                buffer.append(eol);
            }
            // Aquí generamos la lista de objetos de tipo Cita
            mapa = new Gson().fromJson(buffer.toString(), new TypeToken<Map<String, Cita>>() {
            }.getType());

        } catch (final FileNotFoundException e) {
            Log.i(TAG, "El fichero " + Constants.CITAS_POR_NOTIFICAR_FICHERO + " no existe...");
        } catch (final Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (final IOException e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }

        }

        return mapa;

    }

    /**
     * Borra un fichero.
     *
     * @param nombreFichero
     *            nombre del fichero a borrar.
     * @return
     */
    // private static boolean borrarFichero(final String nombreFichero) {
    //
    // // OJO, hay que añadir como parámetro la ruta al fichero
    // final File file = new File(CLCApp.getAppContext().getFilesDir(), nombreFichero);
    //
    // if (file.exists()) {
    // file.delete();
    // return true;
    // }
    //
    // Log.d(TAG, "El fichero " + nombreFichero + " no existe");
    // return false;
    //
    // }

    /**
     * Gestiona el círculo rojo del menú inferior, donde se indica el número de nuevos avisos.
     *
     * @param activity
     *            actividad llamante.
     * @param totalAvisos
     *            número de avisos (>=0).
     */
    public static void setCirculoRojo(final Activity activity, final int totalAvisos) {

        if (activity != null) {
            final LinearLayout llCirculoRojo = (LinearLayout) activity.findViewById(R.id.llCirculoAvisos);

            if (totalAvisos > 0) {
                final TextView tvNumAvisos = (TextView) activity.findViewById(R.id.tvNumAvisos);
                // Ojo, antes hay que pasar el int a cadena, si no intenta buscarlo con recurso.
                tvNumAvisos.setText(String.valueOf(totalAvisos));
                llCirculoRojo.setVisibility(View.VISIBLE);
            } else {
                llCirculoRojo.setVisibility(View.INVISIBLE);
            }
        }

    }

    /**
     * Crea una alarma en el AlarmManager para enviar la notificación de una cita con un preaviso de N días.
     *
     * @param cita
     *            cita a notificar.
     */
    public static void createScheduledNotification(final Cita cita) {

        try {

            final Date fechaHoy = new Date();
            final Date fechaCita = new SimpleDateFormat("dd-MM-yyyy HH:mm", CLCApp.getLocale()).parse(cita
                    .getFechaHora());

            // Sólo si la fecha de la cita es posterior a la fecha de hoy
            if (fechaHoy.compareTo(fechaCita) < 0) {

                final SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
                final SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm");

                // Formateamos el mensaje de la notificación
                StringBuilder msg = new StringBuilder(CLCApp.getAppContext().getString(R.string.recuerde_que));
                msg.append(sdfFecha.format(fechaCita)).append(CLCApp.getAppContext().getString(R.string.a_las))
                        .append(sdfHora.format(fechaCita)).append(CLCApp.getAppContext().getString(R.string.en))
                        .append(cita.getAreaMedica());

                // Preparamos la fecha del aviso
                final Calendar calendar = UtilsCitas.calcularFechaAviso(fechaCita);

                // OJOJOJOJOJOJOJO
                // OJOJOJOJOJOJOJO
                // OJOJOJOJOJOJOJO
                // OJOJOJOJOJOJOJO
                // En pruebas cambiamos el texto y el preaviso de las notificaciones, en un tiempo entre 2 y 6 minutos
                if (VariantConstants.DEBUG) {
                    final Random rand = new Random();
                    int MAX = 7;
                    int MIN = 3;
                    int randomNum = rand.nextInt((MAX - MIN) + 1) + MIN;
                    long horaActual = System.currentTimeMillis();
                    calendar.setTimeInMillis(horaActual + randomNum * 60 * 1000);
                    msg = new StringBuilder();
                    msg.append("Aviso ").append(cita.getCamId()).append(". ").append(randomNum)
                            .append(" mins después ").append(sdfHora.format(horaActual));
                }

                final AlarmManager alarmManager = (AlarmManager) CLCApp.getAppContext().getSystemService(
                        CLCApp.getAppContext().ALARM_SERVICE);

                // Hay que asignar un id a la alarma
                int id = (int) System.currentTimeMillis();

                // A la clase que se encargará de crear la notificación le pasamos el texto y el id de la misma.
                final Intent intent = new Intent(CLCApp.getAppContext(), NotificationTimeAlarm.class);
                // Si el id de la notificación ya está asociado a una existente, el resultado será que se modificará
                intent.putExtra(Constants.ID_NOTIFICACION, cita.getIdNotificacion());
                intent.putExtra(Constants.TEXTO_NOTIFICACION, msg.toString());

                // Preparamos el "PendingIntent"
                final PendingIntent pendingIntent = PendingIntent.getBroadcast(CLCApp.getAppContext(), id, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                // Registramos la alerta en el sistema.
                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

                // Leemos la lista de citas pendientes de notificar y le añadimos la actual
                Log.d(TAG, "Creada la notificación para la cita: " + cita.getCamId());

                final Map<String, Cita> mapa = UtilsCitas.leerMapaCitasPorNotificar();
                mapa.put(cita.getCamId(), cita);
                UtilsCitas.guardarFicheroCitasPorNotificar(mapa);

                UtilsCitas.listarMapaCitasPorNotificar("UtilsCitas");

            }

        } catch (ParseException pe) {
            Log.e(TAG, Log.getStackTraceString(pe));
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    /**
     * Borra la alarma de una cita que ha sido borrada.
     *
     * @param cita
     *            cita a notificar.
     */
    public static void removeScheduledNotification(final Cita cita) {

        try {
            final NotificationManager mNotificationManager = (NotificationManager) CLCApp.getAppContext()
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.cancel(cita.getIdNotificacion());
            // mNotificationManager.cancel(1234567);
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    /**
     * Borra del fichero de notificaciones pendientes una cita cuya notificación haya sido enviada correctamente.
     *
     * @param idNotif
     *            identificador de la notificación.
     */
    public static void borrarNotificacionPendiente(final int idNotif) {

        final Map<String, Cita> mapa = leerMapaCitasPorNotificar();

        // Ojo a la ConcurrentModificationException: hay que usar un Iterator y hacer el remove sobre él
        final Iterator iterator = mapa.entrySet().iterator();
        while (iterator.hasNext()) {
            final Map.Entry<String, Cita> pairs = (Map.Entry) iterator.next();
            final Cita aux = pairs.getValue();
            // Si es la cita que queremos borrar de la lista de pendientes...
            if (aux.getIdNotificacion() == idNotif) {
                iterator.remove();
                break;
            }
        }
        // Guardamos una nueva versión del fichero, ya sin la cita notificada
        guardarFicheroCitasPorNotificar(mapa);

    }

    /**
     * Borra del fichero de notificaciones pendientes una cita que ya esté obsoleta.
     */
    public static void borrarNotificacionesObsoletas() {

        final Map<String, Cita> mapa = leerMapaCitasPorNotificar();

        if (!mapa.isEmpty()) {
            final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm", CLCApp.getLocale());
            final Calendar fechaCita = Calendar.getInstance();
            final Calendar fechaHoy = Calendar.getInstance();

            // Ojo a la ConcurrentModificationException: hay que usar un Iterator y hacer el remove sobre él
            Iterator iterator = mapa.entrySet().iterator();
            while (iterator.hasNext()) {
                try {
                    final Map.Entry<String, Cita> pairs = (Map.Entry) iterator.next();
                    final Cita cita = pairs.getValue();
                    // Si es la cita que queremos borrar de la lista de pendientes...
                    fechaCita.setTime(sdf.parse(cita.getFechaHora()));
                    if (fechaHoy.compareTo(fechaCita) > 0) {
                        Log.i(TAG, "hay que quitar la cita de la lista, está obsoleta");
                        iterator.remove();
                    }
                } catch (ParseException pe) {
                    Log.e(TAG, Log.getStackTraceString(pe));
                }
            }

            // Log.i(TAG, "tamaño de la lista después de borrar notificaciones obsoletas: " + mapa.size());
            // Guardamos una nueva versión del fichero, ya sin la cita notificada
            guardarFicheroCitasPorNotificar(mapa);

        }

    }

    /**
     * Calcula la fecha en la que habrá que enviar el aviso.
     *
     * @param fechaCita
     */
    public static Calendar calcularFechaAviso(final Date fechaCita) {

        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(fechaCita.getTime());

        // OJO, estamos restando dos días a la fecha de la cita
        calendar.add(Calendar.DAY_OF_MONTH, -Constants.DIAS_PREAVISO_CITA);

        return calendar;

    }

    /**
     * Trazas. Sólo en modo debug.
     */
    public static void listarMapaCitasPorNotificar(final String str) {

        if (VariantConstants.DEBUG) {
            final Map<String, Cita> mapa = leerMapaCitasPorNotificar();
            Log.i(TAG, str + " - listarMapaCitasPorNotificar. Hay " + mapa.size());
            final Iterator iterator = mapa.entrySet().iterator();
            if (iterator.hasNext()) {
                while (iterator.hasNext()) {
                    final Map.Entry<String, Cita> pairs = (Map.Entry) iterator.next();
                    Log.i(TAG, pairs.getValue().toString());
                }
            } else {
                Log.i(TAG, "No hay ninguna cita por notificar");
            }

        }
    }

}
