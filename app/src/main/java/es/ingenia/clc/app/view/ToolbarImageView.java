package es.ingenia.clc.app.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;
import es.ingenia.clc.app.R;

/**
 * Extensión de la clase ImageView, para permitir dos estados y dos imágenes asociadas.
 * <p/>
 * Se usa principalmente desde el MainActivity.
 * 
 * @author David Díaz
 * @version 1.0, 08/07/2014
 */
public final class ToolbarImageView extends ImageView {

    /**
     * Para trazas.
     */
    // private static final String TAG = "ToolbarImageView";

    /**
     * Imagen cuando el elemento está activo.
     */
    private final Drawable imageOn;

    /**
     * Imagen cuando el elemento está inactivo.
     */
    private final Drawable imageOff;

    /**
     * Constructor.
     * 
     * @param context
     *            contexto de la aplicación.
     * @param attrs
     *            atributos.
     */
    public ToolbarImageView(final Context context, final AttributeSet attrs) {

        super(context, attrs);

        final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ToolbarImageView, 0, 0);

        imageOn = a.getDrawable(R.styleable.ToolbarImageView_imgOn);
        imageOff = a.getDrawable(R.styleable.ToolbarImageView_imgOff);
        setImageDrawable(imageOff);
        a.recycle();

    }

    /**
     * Fija la imagen que corresponde al modo "on"
     */
    public void seleccionar() {

        setImageDrawable(imageOn);
        setClickable(false);

    }

    /**
     * Fija la imagen que corresponde al modo "off".
     */
    public void deseleccionar() {

        setImageDrawable(imageOff);
        setClickable(true);

    }

}
