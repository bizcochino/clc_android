package es.ingenia.clc.app.fragments;

import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.GestorCitas;
import es.ingenia.clc.app.PreferencesStore;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.TabConstants;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.VariantConstants;
import es.ingenia.clc.app.fragments.interfaces.InterfaceMainMenuFragment;
import es.ingenia.clc.app.ksoap.AppCLC;
import es.ingenia.clc.app.ksoap.IWsdl2CodeEvents;
import es.ingenia.clc.app.ksoap.model.Cita;
import es.ingenia.clc.app.ksoap.model.Familiar;
import es.ingenia.clc.app.ksoap.model.GrupoFamiliar;
import es.ingenia.clc.app.ksoap.model.InicioSesion;
import es.ingenia.clc.app.ksoap.model.ProximasCitas;
import es.ingenia.clc.app.view.OswaldTextView;

/**
 * Clase para el fragmento que muestra el formulario de login de usuario de la app CLC.
 *
 * @author David Díaz
 * @version 1.0, 16/07/14
 */
public class UsuarioLoginFragment extends CustomFragment implements InterfaceMainMenuFragment {

    /**
     * Para trazas.
     */
    private static final String TAG              = "UsuarioLoginFragment";

    /**
     * Constante para definir el tamaño mínimio del RUT.
     */
    private static final int    LONGITUD_USUARIO = 9;

    /**
     * Para el acceso a los servicios web.
     */
    private AppCLC              appCLC;

    /**
     * Campo con el RUT del usuario.
     */
    private EditText            etRUT;

    /**
     * Campo con la clave del usuario.
     */
    private EditText            etClaveRUT;

    /**
     * Semáforo para evitar varios clics sobre "Ingresar".
     */
    private boolean             semaforo         = false;

    @Override
    public final void onCreate(final Bundle savedInstance) {

        super.onCreate(savedInstance);

        // Añadimos el listener para detectar que se ha pulsado "atrás"
        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(getListener());

    }

    /**
     * @param inflater
     *            inflater
     * @param container
     *            container
     * @param savedInstanceState
     *            savedInstanceState
     * @return
     */
    @Override
    public final View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_usuario_login, container, false);

    }

    @Override
    public final void onActivityCreated(final Bundle savedInstance) {

        super.onActivityCreated(null);

        // Fijamos el texto de la barra superior
        final OswaldTextView tvTitle = (OswaldTextView) getView().findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.usuario);

        configuraRUT();

        configuraPassword();

        configuraIngresar();

        configuraCancelar();

        configuraBtnProximasCitas();

        configuraBtnCerrarSesion();

        // Google Analytics...
        Utils.notificaGoogleAnalytics(Constants.SCREEN_MI_CLC, Constants.SCREEN_MI_CLC, Constants.EVENT_LABEL_NA);

    }

    /**
     * Sólo para desarrollo, para facilitar las pruebas.
     */
    private void configuraPassword() {
        etClaveRUT = (EditText) getView().findViewById(R.id.etClaveRUT);

        etClaveRUT.setText(VariantConstants.MOCKUP_PASSWORD);
    }

    @Override
    public void onResume() {

        super.onResume();

        determineFragmentLayout();

    }

    /**
     *
     */
    private void configuraRUT() {

        etRUT = (EditText) getView().findViewById(R.id.etRUT);

        etRUT.setText(VariantConstants.MOCKUP_USER);

        // Hay que estar pendientes de lo que se teclea, para incluir un "-" cuando se llegue al carácter noveno
        final TextWatcher textWatcher = new TextWatcher() {

            private int    start   = 0;
            private int    after   = 0;
            private String oldText = "";

            /**
             * Se ha intentado teclear un carácter no permitido.
             */
            private void stringNotAllowed() {
                etRUT.setText(this.oldText);
                etRUT.setSelection(start);
            }

            /**
             * @param editable
             */
            private void addCharacter(final Editable editable, final int newStart) {

                final StringBuilder aux = new StringBuilder(editable.toString().substring(0, LONGITUD_USUARIO - 1));
                aux.append("-").append(editable.toString().substring(LONGITUD_USUARIO - 1, LONGITUD_USUARIO));
                etRUT.setText(aux.toString());
                etRUT.setSelection(newStart);

            }

            @Override
            public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {

                // This method is called to notify you that, within s, the count characters beginning at start are about
                // to be replaced by new text with length after.

                this.start = start;
                this.after = after;
                this.oldText = s.toString();

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, final int start, final int before,
                    final int count) {

            }

            @Override
            public void afterTextChanged(final Editable editable) {

                etRUT.removeTextChangedListener(this);

                // Sólo se admiten letras si la longitud es superior a 8 y la posición de inicio es la última
                final int longitud = editable.length();

                // Cadena tecleada
                final String lastString = editable.toString().substring(start, start + after);

                // Si la longitud es menor de 9
                if (longitud < LONGITUD_USUARIO) {

                    if (!Utils.isNumber(lastString) && !"".equals(lastString)) {
                        // Carácter no permitido: el texto se mantiene igual
                        stringNotAllowed();
                    }

                } else {

                    if ((start != (LONGITUD_USUARIO - 1)) && Utils.isNumber(lastString)) {

                        // Número válido para cualquier posición
                        addCharacter(editable, start + 1);

                    } else if ((start == (LONGITUD_USUARIO - 1)) && (Utils.isNumberOrLetter(lastString))) {

                        // Estamos en la última posición, donde valen letras y números
                        addCharacter(editable, LONGITUD_USUARIO + 1);

                    } else if ("".equals(lastString)) {

                        // Estamos borrando, además hay que quitar el guión
                        etRUT.setText(editable.toString().replace("-", ""));
                        int newStart = (start == LONGITUD_USUARIO ? start - 1 : start);
                        etRUT.setSelection(newStart);

                    } else {

                        // Carácter no permitido
                        stringNotAllowed();

                    }

                }

                etRUT.addTextChangedListener(this);

            }
        };

        etRUT.addTextChangedListener(textWatcher);
    }

    /**
     *
     */
    private void configuraBtnCerrarSesion() {

        // Cerrar sesión
        final OswaldTextView tvCerrarSesion = (OswaldTextView) getActivity().findViewById(R.id.tvCerrarSesion);
        tvCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                cerrarSesion();
            }
        });

    }

    /**
     *
     */
    private void configuraBtnProximasCitas() {

        // Próximas citas
        final OswaldTextView tvProximasCitas = (OswaldTextView) getActivity().findViewById(R.id.tvProximasCitas);
        tvProximasCitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                mostrarProximasCitas();
            }
        });

    }

    /**
     *
     */
    private void configuraCancelar() {

        final OswaldTextView tvCancelar = (OswaldTextView) getActivity().findViewById(R.id.tvCancelar);
        tvCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                if (!semaforo) {
                    // 2014-11-19 // semaforo = false;
                    semaforo = true;

                    // Borramos los campos
                    etRUT.setText("");
                    etClaveRUT.setText("");

                    // Para ocultar el teclado
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etRUT.getWindowToken(), 0);

                    // Y ocultamos la barra de progreso
                    final ProgressBar pbLogin = (ProgressBar) getActivity().findViewById(R.id.pbLogin);
                    pbLogin.setVisibility(View.INVISIBLE);

                    // Google Analytics...
                    Utils.notificaGoogleAnalytics(Constants.SCREEN_MI_CLC, Constants.EVENT_ACTION_CANCELAR,
                            Constants.EVENT_LABEL_NA);

                    semaforo = false;
                }

            }
        });
    }

    /**
     *
     */
    private void configuraIngresar() {

        final OswaldTextView tvIngresar = (OswaldTextView) getView().findViewById(R.id.tvIngresar);
        tvIngresar.setOnClickListener(new View.OnClickListener() {

            /**
             * Método para validar un nombre de usuario.
             *
             * @param user
             *            nombre de usuario a validar.
             * @return true si el usuario es válido.
             */
            private boolean validarUsuario(final String user) {

                final int length = user.length();
                // Comprobamos que se alcanza la longitud mínima
                if (length < LONGITUD_USUARIO) {
                    return false;
                }

                // Comprobamos que los 8 primeros caracteres son numéricos
                final String firstPart = user.substring(0, 8);
                if (!Utils.isNumber(firstPart)) {
                    return false;
                }

                // Comprobamos que el último carácter es un número o una letra
                if (!Character.isLetterOrDigit(user.charAt(length - 1))) {
                    return false;
                }

                // Si la longitud es exacta, es un usuario válido
                if (length == LONGITUD_USUARIO) {
                    return true;
                } else if ((length == (LONGITUD_USUARIO + 1)) && (user.substring(8, LONGITUD_USUARIO).equals("-"))) {
                    // Hay diez caracteres, así que el noveno debe ser un "-"
                    return true;
                }

                // En cualquier otro caso, devolvemos "falso"
                return false;

            }

            @Override
            public void onClick(final View view) {

                if (!semaforo) {

                    // Hay que validar el RUT que el usuario ha indicado
                    if (validarUsuario(etRUT.getText().toString())) {

                        // Semáforo on, para impedir múltiples pulsaciones
                        semaforo = true;

                        // Ocultamos el teclado
                        ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))
                                .hideSoftInputFromWindow(etRUT.getWindowToken(), 0);

                        final IWsdl2CodeEvents events = new InicioSesionWsdl2CodeEvents();
                        appCLC = new AppCLC(events, VariantConstants.WSDL_URL_LOGIN, AppCLC.TIMEOUT);
                        try {
                            appCLC.inicioSesionAsync(etRUT.getText().toString().replace("-", ""), etClaveRUT.getText()
                                    .toString());
                        } catch (final Exception e) {
                            Log.e(TAG, Log.getStackTraceString(e));
                            semaforo = false;
                        }

                    } else {
                        // El RUT tecleado no es válido: avisamos al usuario
                        Utils.showOkDialogWithText(getActivity(), getString(R.string.login),
                                getString(R.string.rut_no_valido));
                    }

                    // Google Analytics...
                    Utils.notificaGoogleAnalytics(Constants.SCREEN_MI_CLC, Constants.EVENT_ACTION_INGRESAR,
                            Constants.EVENT_LABEL_NA);
                }

            }
        });
    }

    /**
     *
     */
    private void determineFragmentLayout() {

        if (CLCApp.getInicioSesion() != null && CLCApp.getFamiliarSeleccionado() != null) {

            // Hay sesión, ocultamos formulario de login
            final LinearLayout llLogin = (LinearLayout) getView().findViewById(R.id.llLogin);
            llLogin.setVisibility(View.GONE);

            // Mostramos la barra de progreso.
            final ProgressBar pbLogin = (ProgressBar) getView().findViewById(R.id.pbLogin);
            pbLogin.setVisibility(View.VISIBLE);

            final OswaldTextView tvUsuarioActivo = (OswaldTextView) getView().findViewById(R.id.tvUsuarioActivo);
            tvUsuarioActivo.setText(CLCApp.getFamiliarSeleccionado().getNombre());

            final RecuperarGrupoFamiliarLoginWsdl2CodeEvents events = new RecuperarGrupoFamiliarLoginWsdl2CodeEvents();
            appCLC = new AppCLC(events, VariantConstants.WSDL_URL_GRUPO_FAMILIAR, AppCLC.TIMEOUT);
            try {
                appCLC.recuperaGrupoFamiliarAsync(CLCApp.getInicioSesion().getPersCorrel(), CLCApp.getInicioSesion()
                        .getToken());
            } catch (final Exception e) {
                Log.e(TAG, Log.getStackTraceString(e));
                // 18/11/2014
                if (CLCApp.getInicioSesion() == null) {
                    // No hay sesión, ocultamos enlaces de usuario logado
                    final LinearLayout llLogged = (LinearLayout) getView().findViewById(R.id.llLogged);
                    llLogged.setVisibility(View.GONE);
                }
            }

        } else {

            // Hay sesión, ocultamos formulario de login
            final LinearLayout llLogin = (LinearLayout) getView().findViewById(R.id.llLogin);
            llLogin.setVisibility(View.VISIBLE);

            // No hay sesión, ocultamos enlaces de usuario logado
            final LinearLayout llLogged = (LinearLayout) getView().findViewById(R.id.llLogged);
            llLogged.setVisibility(View.GONE);

        }

    }

    /**
     * Aquí capturamos el cambio que se produce en la pila, y volvemos a llamar a onResume().
     *
     * @return
     */
    private FragmentManager.OnBackStackChangedListener getListener() {

        return new FragmentManager.OnBackStackChangedListener() {

            public void onBackStackChanged() {
                if (CLCApp.getCurrentFragmentId() == TabConstants.FRAG_5_LOGIN) {
                    // Log.e(TAG, "EEEEEEEEE FragmentManager.OnBackStackChangedListener");
                    determineFragmentLayout();
                }

            }
        };

    }

    /**
     * Carga el fragmento con las próximas citas.
     * <p/>
     * Es llamado en el onActivityCreated si hay sesión de usuario o desde wsdl2CodeFinished de la clase
     * RecuperarProximasCitasWsdl2CodeEvents, cuando se hayan obtenido las citas del servicio web.
     */
    private void mostrarProximasCitas() {

        // Click en configuración (si es teléfono) / cita previa (si es tableta)
        getMActivity().pushFragments(TabConstants.FRAG_5_LOGIN, new ProximasCitasFragment(), true, true,
                TabConstants.FRAG_5_1_PROXIMAS_CITAS);

        // Desactivamos semáforo
        semaforo = false;

        // Google Analytics...
        Utils.notificaGoogleAnalytics(Constants.SCREEN_MI_CLC, Constants.EVENT_ACTION_PROXIMAS_CITAS,
                Constants.EVENT_LABEL_NA);

    }

    /**
     * Cierra la sesión y muestra nuevamente el formulario de login.
     */
    private void cerrarSesion() {

        CLCApp.setInicioSesion(null);

        CLCApp.setFamiliarSeleccionado(null);

        CLCApp.setGrupoFamiliar(null);

        // Borramos los campos
        etRUT.setText("");
        etClaveRUT.setText("");

        // Ocultamos la barra de progreso
        final ProgressBar pbLogin = (ProgressBar) getView().findViewById(R.id.pbLogin);
        pbLogin.setVisibility(View.INVISIBLE);

        // Mostramos formulario de login
        final LinearLayout llLogin = (LinearLayout) getView().findViewById(R.id.llLogin);
        llLogin.setVisibility(View.VISIBLE);

        // Ocultamos enlaces de usuario logado
        final LinearLayout llLogged = (LinearLayout) getView().findViewById(R.id.llLogged);
        llLogged.setVisibility(View.GONE);

        // Google Analytics...
        Utils.notificaGoogleAnalytics(Constants.SCREEN_MI_CLC, Constants.EVENT_ACTION_CERRAR_SESION,
                Constants.EVENT_LABEL_NA);

    }

    /**
     * Clase que gestiona los eventos del servicio web de inicio de sesión.
     *
     * @author David Díaz
     * @version 1.0, 16/07/14
     */
    private class InicioSesionWsdl2CodeEvents implements IWsdl2CodeEvents {

        /**
         * Para trazas.
         */
        private static final String TAG = "InicioSesionWsdl2CodeEvents";

        /**
         * Barra de progreso.
         */
        private ProgressBar         pbLogin;

        /**
         * Método a ejecutar cuando se inicia la petición.
         */
        public void wsdl2CodeStartedRequest() {

            // Mostramos la barra de progreso.
            pbLogin = (ProgressBar) getActivity().findViewById(R.id.pbLogin);
            pbLogin.setVisibility(View.VISIBLE);

        }

        /**
         * El servicio web ha finalizado correctamente.
         *
         * @param methodName
         *            nombre del método.
         * @param data
         *            datos.
         */
        public void wsdl2CodeFinished(final String methodName, final Object data) {

            // Log.d(TAG, "wsdl2CodeFinished, method: " + methodName);

            if (data instanceof InicioSesion) {

                CLCApp.setInicioSesion((InicioSesion) data);

                if (((InicioSesion) data).isSuccess()) {

                    // Guardamos el objeto InicioSesion en las preferencias
                    final RecuperarProximasCitasWsdl2CodeEvents eventsCitas = new RecuperarProximasCitasWsdl2CodeEvents();
                    appCLC = new AppCLC(eventsCitas, VariantConstants.WSDL_URL_PROXIMAS_CITAS, AppCLC.TIMEOUT);
                    try {
                        appCLC.recuperaProximasCitasAsync(CLCApp.getInicioSesion().getPersCorrel(), CLCApp
                                .getInicioSesion().getToken(), "");
                    } catch (final Exception e) {
                        Log.e(TAG, Log.getStackTraceString(e));
                        pbLogin.setVisibility(View.INVISIBLE);
                        semaforo = false;
                    }

                } else {
                    // No se ha podido hacer login: las credenciales no son correctas
                    Utils.showOkDialogWithText(getActivity(), getString(R.string.login),
                            getString(R.string.error_credenciales));
                    pbLogin.setVisibility(View.INVISIBLE);
                    semaforo = false;
                }

            }

        }

        /**
         * El servicio web ha finalizado dando una excepción.
         *
         * @param ex
         *            excepción producida.
         */
        public void wsdl2CodeFinishedWithException(final Exception ex) {

            Log.d(TAG, "wsdl2CodeFinishedWithException");
            Log.e(TAG, Log.getStackTraceString(ex));

            try {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // Aviso al usuario y ocultamos la barra de progreso
                            Utils.showOkDialogWithText(getActivity(), CLCApp.getAppContext().getString(R.string.login),
                                    getString(R.string.error_inicio_sesion));
                        }
                    });
                }
            } catch (final NullPointerException npe) {
                Log.e(TAG, Log.getStackTraceString(npe));
            }
            semaforo = false;

        }

        /**
         *
         */
        public void wsdl2CodeEndedRequest() {

            Log.d(TAG, "wsdl2CodeEndedRequest");
            pbLogin.setVisibility(View.INVISIBLE);

        }
    }

    /**
     * Clase que gestiona los eventos del servicio web de recuperación de las próximas citas.
     *
     * @author David Díaz
     * @version 1.0, 16/07/14
     */
    private class RecuperarProximasCitasWsdl2CodeEvents implements IWsdl2CodeEvents {

        /**
         * Para trazas.
         */
        private static final String TAG = "RecuperarProximasCitasWsdl2CodeEvents";

        /**
         * Clase para modelar la respuesta del servicio web de próximas citas.
         */
        private ProximasCitas       citas;

        /**
         * Barra de progreso.
         */
        private ProgressBar         pbLogin;

        /**
         *
         */
        public void wsdl2CodeStartedRequest() {

            Log.d(TAG, "wsdl2CodeStartedRequest");
            // Mostramos la barra de progreso.
            // ProgressBar pbLogin = (ProgressBar) getActivity().findViewById(R.id.pbLogin);
            // Mostramos la barra de progreso.
            pbLogin = (ProgressBar) getActivity().findViewById(R.id.pbLogin);

        }

        /**
         * El servicio web ha finalizado correctamente.
         *
         * @param methodName
         *            nombre del método.
         * @param data
         *            datos.
         */
        public void wsdl2CodeFinished(final String methodName, final Object data) {

            this.citas = (ProximasCitas) data;

            final GestorCitas gestor = new GestorCitas(getActivity(), CLCApp.getInicioSesion().getPersCorrel());
            gestor.gestionarCitas(citas);

            // TODO Probar a pasar una lista nula
            final List<Cita> lista = citas.getListaCitas();
            if (lista != null) {
                PreferencesStore.setFechaUltimaSincCitas();
                pbLogin.setVisibility(View.INVISIBLE);
                // Cargamos el fragmento que muestra el listado de citas
                mostrarProximasCitas();
            }
        }

        /**
         * Se ha producido una excepción en la invocación al servicio web.
         *
         * @param ex
         *            excepción producida.
         */
        public void wsdl2CodeFinishedWithException(final Exception ex) {

            Log.d(TAG, "wsdl2CodeFinishedWithException");
            Log.e(TAG, Log.getStackTraceString(ex));

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Aviso al usuario y ocultamos la barra de progreso
                    Utils.showOkDialogWithText(getActivity(), getString(R.string.login),
                            getString(R.string.error_general));
                    pbLogin.setVisibility(View.INVISIBLE);
                    cerrarSesion();
                    semaforo = false;
                }
            });

        }

        /**
         *
         */
        public void wsdl2CodeEndedRequest() {

            Log.d(TAG, "wsdl2CodeEndedRequest");

        }

    }

    /**
     * Clase que gestiona los eventos de la recuperación del grupo familiar.
     *
     * @author David Díaz
     * @version 1.0, 26/08/14
     */
    private class RecuperarGrupoFamiliarLoginWsdl2CodeEvents implements IWsdl2CodeEvents {

        /**
         * Para trazas.
         */
        private static final String TAG = "RecuperarGrupoFamiliarLoginWsdl2CodeEvents";

        /**
         *
         */
        public void wsdl2CodeStartedRequest() {

            Log.i(TAG, "wsdl2CodeStartedRequest");

        }

        /**
         * @param methodName
         *            nombre del método.
         * @param data
         *            datos.
         */
        public void wsdl2CodeFinished(final String methodName, final Object data) {

            if (((GrupoFamiliar) data).isAutenticado()) {

                final List<Familiar> lista = ((GrupoFamiliar) data).getListaFamiliares();

                if (lista != null) {

                    // Log.i(TAG, "Tiene " + lista.size() + " grupos familiares");

                    final View view = getView();

                    if (view != null) {

                        if (CLCApp.getInicioSesion() != null) {
                            final LinearLayout llLogged = (LinearLayout) view.findViewById(R.id.llLogged);
                            llLogged.setVisibility(View.VISIBLE);

                            // Mostramos la barra de progreso.
                            final ProgressBar pbLogin = (ProgressBar) getActivity().findViewById(R.id.pbLogin);
                            pbLogin.setVisibility(View.INVISIBLE);

                            for (final Familiar item : lista) {
                                // Log.d(TAG,
                                // "DEBUG - item: " + item.getProperty(0) + ", " + item.getNombre() + ", "
                                // + item.getParentesco());
                                // Volvemos a poner como seleccionado el usuario que hizo el login
                                if (item.getParentesco() != null && item.getParentesco().equals("Titular")) {
                                    final OswaldTextView tvUsuarioActivo = (OswaldTextView) view
                                            .findViewById(R.id.tvUsuarioActivo);
                                    tvUsuarioActivo.setText(item.getNombre());
                                    Log.d(TAG, "DEBUG - El familiar seleccionado será: " + item.getNombre());
                                    CLCApp.setFamiliarSeleccionado(item);
                                    break;
                                }
                            }
                        } else {

                            // El usuario ha cerrado sesión mientras estaba en marcha esto, así que hay que evitar que
                            // se muestren los dos (login/logado).
                            final LinearLayout llLogged = (LinearLayout) view.findViewById(R.id.llLogin);
                            llLogged.setVisibility(View.VISIBLE);

                            // Mostramos la barra de progreso.
                            final ProgressBar pbLogin = (ProgressBar) getActivity().findViewById(R.id.pbLogin);
                            pbLogin.setVisibility(View.INVISIBLE);

                        }

                    } else {
                        Log.e(TAG, "La vista ya no está disponible");
                    }
                }

            } else {
                // La sesión ha caducado
                CLCApp.setInicioSesion(null);

                // Informamos al usuario
                Utils.showOkDialogWithText(getActivity(), getString(R.string.sesion_caducada),
                        getString(R.string.sesion_caducada_info));

            }
        }

        /**
         * @param ex
         *            excepción producida.
         */
        public void wsdl2CodeFinishedWithException(final Exception ex) {

            Log.d(TAG, "wsdl2CodeFinishedWithException");
            Log.e(TAG, Log.getStackTraceString(ex));

        }

        /**
         *
         */
        public void wsdl2CodeEndedRequest() {

            Log.d(TAG, "wsdl2CodeEndedRequest");

        }
    }

}
