package es.ingenia.clc.app.fragments;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.PreferencesStore;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.VariantConstants;
import es.ingenia.clc.app.banners.ScreenSlidePagerAdapter;
import es.ingenia.clc.app.fragments.interfaces.InterfaceMainMenuFragment;
import es.ingenia.clc.app.ksoap.AppCLC;
import es.ingenia.clc.app.ksoap.IWsdl2CodeEvents;
import es.ingenia.clc.app.ksoap.model.Banner;
import es.ingenia.clc.app.ksoap.model.GISEmergencia;
import es.ingenia.clc.app.view.OswaldTextView;
import es.ingenia.clc.app.view.ToolbarImageView;

/**
 * Clase para el fragmento "Home" de la app CLC.
 *
 * @author David Díaz
 * @version 1.0, 23/06/2014
 */
public class HomeFragment extends CustomFragment implements InterfaceMainMenuFragment {

    /**
     * Para trazas.
     */
    private static final String            TAG                = "HomeFragment";

    /**
     * Número de la página del carrusel que se está mostrando.
     */
    private static int                     currentPage        = 0;

    /**
     * Pager.
     */
    private ViewPager                      mPager;

    /**
     * Retraso en comenzar transiciones automáticas del carrusel.
     */
    private static final long              LAUNCH_INTERVAL    = 35000;

    /**
     * Intervalo entre transiciones automáticas del carrusel.
     */
    private static final long              REPEATING_INTERVAL = 10000;

    /**
     * Adaptador para el carrusel de imágenes.
     */
    private static ScreenSlidePagerAdapter mPagerAdapter;

    /**
     * Timer para las transiciones automáticas de las imágenes del carrusel.
     */
    private Timer                          swipeTimer;

    /**
     * @param inflater
     *            inflater.
     * @param container
     *            container.
     * @param savedInstanceState
     *            saveInstanceState.
     * @return
     */
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        // Log.i(TAG, "onCreateView");

        Utils.checkDisplayDensity();

        Utils.checkScreenSize();

        // bannerConfig = Utils.checkScreenAndDensity();

        // gestor = new GestorBanners();
        // gestor.cargarBanners();

        return inflater.inflate(R.layout.fragment_home, container, false);

    }

    @Override
    public void onActivityCreated(final Bundle savedInstance) {

        super.onActivityCreated(savedInstance);

        // Log.i(TAG, "onActivityCreated");

        // Marcamos el botón correspondiente en la botonera
        final ToolbarImageView ivHome = (ToolbarImageView) getActivity().findViewById(R.id.ivHome);
        ivHome.seleccionar();

        // Configuración de los botones de llamada
        setUpPhoneCallButtons();

        // Configuración de botones para redes sociales
        setUpSocialButtons();

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) getActivity().findViewById(R.id.pager);

        // http://stackoverflow.com/questions/18444264/unable-to-resume-an-activity-when-using-a-viewpager

        // Obtenemos la instancia del adaptador...
        mPagerAdapter = ScreenSlidePagerAdapter.getInstance(getChildFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
                currentPage = position;
                // Cada vez que cambia la imagen del carrusel, hay que actualizar la botonera de círculos
                refreshCircles(currentPage);
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();

        // Log.i(TAG, "onResume");

        // Hay que fijar cuál es el punto actual en el carrusel
        mPager.setCurrentItem(currentPage);

        mPagerAdapter.refreshBannersList();

        activarTransicionesAutomaticas();

        // Botonera de círculos (carrusel)
        drawCircles(currentPage);

        // Google Analytics... dejamos constancia de la vista de la página
        Utils.notificaGoogleAnalytics(Constants.SCREEN_INICIO, Constants.SCREEN_INICIO, Constants.EVENT_LABEL_NA);

    }

    @Override
    public void onStop() {

        super.onStop();

        // Cancelamos la actividad del timer, si lo hubiere
        if (swipeTimer != null) {
            swipeTimer.cancel();
        }

        final ToolbarImageView ivHome = (ToolbarImageView) getActivity().findViewById(R.id.ivHome);
        if (CLCApp.isPortrait()) {
            ivHome.deseleccionar();
        } else {
            ivHome.setImageResource(R.drawable.ico_bar_home);
        }

    }

    /**
     * Activa las transiciones automáticas en el carrusel de imágenes. Llamado desde el onResume().
     */
    private void activarTransicionesAutomaticas() {

        // Transiciones automáticas en el carrusel
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            public void run() {
                if (currentPage == mPagerAdapter.getCount()) {
                    currentPage = 0;
                }
                // Si sólo hay un banner, no hay que hacer nada
                if (mPagerAdapter.getCount() > 1) {
                    mPager.setCurrentItem(currentPage++, true);
                }
            }
        };

        // Sólo si tenemos más de una imagen activamos el timer...
        if (mPagerAdapter.getCount() > 1) {
            try {
                swipeTimer = new Timer();
                swipeTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(update);
                    }
                }, LAUNCH_INTERVAL, REPEATING_INTERVAL);

                Log.e(TAG, "TRANSITIONS GOING ON!");

            } catch (Exception ex) {
                Log.e(TAG, Log.getStackTraceString(ex));
            }
        } else {
            Log.e(TAG, "SÓLO HAY UN BANNER: NO TRANSITION NEEDED, HONEY");
        }
    }

    /**
     * Comprueba si el dispositivo es capaz de hacer llamadas teléfonicas, configura los botones en caso afirmativo y
     * los oculta en caso contrario.
     */
    private void setUpPhoneCallButtons() {

        // Lo primero es controlar si tenemos teléfono o no...
        if (CLCApp.isHasPhone()) {

            final ImageView ivReservas = (ImageView) getActivity().findViewById(R.id.ivReservas);
            final ImageView ivCentral = (ImageView) getActivity().findViewById(R.id.ivCentral);
            final ImageView ivRescate = (ImageView) getActivity().findViewById(R.id.ivRescate);

            // También serán "clickables" los textos
            final OswaldTextView tvReservas = (OswaldTextView) getActivity().findViewById(R.id.tvReservas);
            final OswaldTextView tvCentral = (OswaldTextView) getActivity().findViewById(R.id.tvCentral);
            final OswaldTextView tvRescate = (OswaldTextView) getActivity().findViewById(R.id.tvRescate);

            // Tenemos teléfono, así que hay que programar los botones
            ivReservas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    onClickReservas(view);
                }
            });
            tvReservas.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    onClickReservas(view);
                }
            });
            ivCentral.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    onClickCentral(view);
                }
            });
            tvCentral.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    onClickCentral(view);
                }
            });
            ivRescate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    onClickRescate(view);
                }
            });
            tvRescate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    onClickRescate(view);
                }
            });

        } else {

            // Si el dispositivo no tiene teléfono, ocultamos los botones de llamada
            final LinearLayout llTelefonos = (LinearLayout) getActivity().findViewById(R.id.llTelefonos);
            if (llTelefonos != null) {
                llTelefonos.setVisibility(View.GONE);
            }

        }

    }

    /**
     * Eventos cuando se pulse la imagen o el texto de "Rescate".
     *
     * @param view
     *            la vista.
     */
    private void onClickRescate(final View view) {
        // Google Analytics...
        Utils.notificaGoogleAnalytics(Constants.SCREEN_INICIO, Constants.EVENT_ACTION_LLAMADA_RESCATE,
                Constants.EVENT_LABEL_NA);
        createRescueAlertCall(view, CLCApp.getTlfRescate(), getString(R.string.telefono_rescate));
    }

    /**
     * Eventos cuando se pulse la imagen o el texto de "Central".
     *
     * @param view
     *            la vista.
     */
    private void onClickCentral(final View view) {
        // Google Analytics...
        Utils.notificaGoogleAnalytics(Constants.SCREEN_INICIO, Constants.EVENT_ACTION_LLAMADA_CENTRAL,
                Constants.EVENT_LABEL_NA);
        createAlertCall(view, R.string.llamada_central, CLCApp.getTlfCentral(), getString(R.string.telefono_central));
    }

    /**
     * Eventos cuando se pulse la imagen o el texto de "Reservas".
     *
     * @param view
     *            la vista.
     */
    private void onClickReservas(final View view) {
        // Google Analytics...
        Utils.notificaGoogleAnalytics(Constants.SCREEN_INICIO, Constants.EVENT_ACTION_LLAMADA_RESERVAS,
                Constants.EVENT_LABEL_NA);
        createAlertCall(view, R.string.llamada_reservas, CLCApp.getTlfReservas(), getString(R.string.telefono_reservas));
    }

    /**
     * Método para crear el alert que permite realizar la llamada teléfonica.
     *
     * @param view
     *            la vista.
     * @param tlf
     *            el teléfono al que llamar.
     * @param formattedTlf
     *            el teléfono al que llamar, formateado para presentarlo en el alert.
     */
    private void createRescueAlertCall(final View view, final String tlf, final String formattedTlf) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setMessage(getString(R.string.llamada_rescate) + "\n" + formattedTlf).setTitle(R.string.llamada);
        builder.setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int id) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(R.string.llamar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int id) {
                onClickRescueAlertCall(tlf);
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();

    }

    /**
     * Llamada de emergencia.
     *
     * @param tlf
     *            teléfono al que llamar.
     */
    private void onClickRescueAlertCall(final String tlf) {

        final IWsdl2CodeEvents events = new GISEmergenciaWsdl2CodeEvents();

        final AppCLC appCLC = new AppCLC(events, VariantConstants.WSDL_URL_RESCUE_CALL, AppCLC.TIMEOUT);
        try {
            String persCorrel = "";
            String token = "";
            // Si el usuario está logado, incluimos los campos "persCorrel" y "token"
            if (CLCApp.getInicioSesion() != null) {
                persCorrel = CLCApp.getInicioSesion().getPersCorrel();
                token = CLCApp.getInicioSesion().getToken();
            }
            final String tlfUsuario = PreferencesStore.getCellNumber();
            // OJO: la hora debe ir en formato HH:mm
            appCLC.gisEmergenciaAsync(persCorrel, token, String.valueOf(CLCApp.getMiLatitud()),
                    String.valueOf(CLCApp.getMiLongitud()), tlfUsuario, Utils.getCurrentDate(),
                    Utils.getCurrentTime(Constants.FORMATO_HORA_LLAMADA_EMERGENCIA));

        } catch (final Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

        final Intent callIntent = new Intent(Intent.ACTION_CALL);

        // OJO, hay que poner "tel:"
        callIntent.setData(Uri.parse("tel:" + tlf));
        startActivity(callIntent);

    }

    /**
     * Método para crear el alert que permite realizar la llamada teléfonica.
     *
     * @param view
     *            la vista.
     * @param message
     *            el mensaje a mostrar.
     * @param tlf
     *            el teléfono al que llamar.
     * @param formattedTlf
     *            el teléfono al que llamar, formateado para presentarlo en el alert.
     */
    private void createAlertCall(final View view, final int message, final String tlf, final String formattedTlf) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setMessage(getString(message) + "\n" + formattedTlf).setTitle(R.string.llamada);
        builder.setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int id) {
                dialog.cancel();
            }
        });
        builder.setPositiveButton(R.string.llamar, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int id) {
                final Intent callIntent = new Intent(Intent.ACTION_CALL);
                // OJO, hay que poner "tel:"
                callIntent.setData(Uri.parse("tel:" + tlf));
                startActivity(callIntent);
            }
        });
        final AlertDialog dialog = builder.create();
        dialog.show();

    }

    /**
     * Configura los botones para acceder a las redes sociales de la clínica.
     */
    private void setUpSocialButtons() {

        final ImageView ivFacebook = (ImageView) getActivity().findViewById(R.id.ivFacebook);
        ivFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                openFacebook();
            }
        });
        final ImageView ivTwitter = (ImageView) getActivity().findViewById(R.id.ivTwitter);
        ivTwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                openTwitter();
            }
        });
        final ImageView ivYouTube = (ImageView) getActivity().findViewById(R.id.ivYouTube);
        ivYouTube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                openYoutube();
            }
        });

    }

    /**
     * Abre la app de Facebook si está instalada, o el navegador en caso contrario.
     */
    private void openFacebook() {

        try {
            CLCApp.getAppContext().getPackageManager().getPackageInfo("com.facebook.katana", 0);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.FACEBOOK_ID)));
        } catch (final Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.FACEBOOK_USER)));
        }

        Utils.notificaGoogleAnalytics(Constants.SCREEN_INICIO, Constants.EVENT_ACTION_FACEBOOK,
                Constants.EVENT_LABEL_NA);

    }

    /**
     * Abre la app de Twitter si está instalada, o el navegador en caso contrario.
     */
    private void openTwitter() {

        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.TWITTER_URI_1)));
        } catch (final Exception e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.TWITTER_URI_2)));
        }

        Utils.notificaGoogleAnalytics(Constants.SCREEN_INICIO, Constants.EVENT_ACTION_TWITTER, Constants.EVENT_LABEL_NA);

    }

    /**
     * Abre la app Youtube.
     */
    private void openYoutube() {

        // Abrimos YouTube mostrando el vídeo promocional de la app
        // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.YOUTUBE_URI_1)));

        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + Constants.YOUTUBE_URI_1_VID));
            startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.YOUTUBE_URI_1));
            startActivity(intent);
        }

        Utils.notificaGoogleAnalytics(Constants.SCREEN_INICIO, Constants.EVENT_ACTION_YOUTUBE, Constants.EVENT_LABEL_NA);

    }

    /**
     * Dibuja la botonera de círculos del carrusel. Se llama cada vez que hay un onResume().
     *
     * @param selected
     *            índice del círculo activo.
     */
    private void drawCircles(final int selected) {

        // Si sólo hay un banner, no dibujamos círculos
        if (mPagerAdapter.getCount() > 1) {

            // Comprobar que las URLs no son nulas
            if (!checkURLBanners()) {
                return;
            }

            final LinearLayout layout = (LinearLayout) getActivity().findViewById(R.id.botoneraCirculos);

            // Por si acaso, eliminamos los posibles hijos que pudiera haber
            if (layout.getChildCount() > 0) {
                layout.removeAllViews();
            }

            // Preparamos los parámetros que tendrá el layout (width, height y márgenes)
            final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            final int margin = getResources().getDimensionPixelSize(R.dimen.btn_circulos_margin);
            params.setMargins(margin, margin, margin, margin);

            // Añadimos tantos círculos como páginas tengamos en el carrusel
            final List<Banner> listaBanners = mPagerAdapter.getListaBanners();

            for (int i = 0; i < mPagerAdapter.getCount(); i++) {

                final Banner banner = listaBanners.get(i);

                // Sólo si la URL del banner es no vacía ni nula, añadimos el círculo
                if (banner.checkURLBanner()) {

                    // En este caso no podemos crear los objetos fuera del bucle
                    final ImageView imageView = new ImageView(CLCApp.getAppContext());
                    imageView.setId(i);

                    // El círculo "seleccionado" será blanco
                    if (i == selected) {
                        imageView.setImageResource(R.drawable.circulo_blanco);
                    } else {
                        imageView.setImageResource(R.drawable.circulo_celeste);
                    }

                    // Hay que asignar los parámetros a la imagen
                    imageView.setLayoutParams(params);

                    // Y la añadimos al layout
                    layout.addView(imageView);

                } else {
                    Log.i(TAG, "Hay un banner, pero no tiene una URL válida");
                }
            }

        }

    }

    /**
     * Devuelve true si la lista de banners contiene al menos una URL no nula.
     * 
     * @return
     */
    private boolean checkURLBanners() {
        boolean ret = false;
        final List<Banner> lista = mPagerAdapter.getListaBanners();
        for (final Banner aux : lista) {
            // Si este "banner" no es válido, pasamos al siguiente
            if (!aux.checkURLBanner()) {
                break;
            }
            // Hemos encontrado un "banner" válido
            ret = true;
        }
        return ret;
    }

    /**
     * Método para refrescar los círculos según vaya cambiando la imagen seleccionada en el carrusel.
     *
     * @param selected
     *            indicador del círculo "activo".
     */
    private void refreshCircles(final int selected) {

        final LinearLayout layout = (LinearLayout) getActivity().findViewById(R.id.botoneraCirculos);

        final int count = layout.getChildCount();

        // Recorremos los hijos del layout... que serán los n círculos (máximo de 4)
        for (int i = 0; i < count; i++) {
            final View child = layout.getChildAt(i);
            if (child instanceof ImageView) {
                final int childId = child.getId();
                // Si está seleccionado, círculo blanco
                if (childId != selected) {
                    ((ImageView) child).setImageResource(R.drawable.circulo_celeste);
                } else {
                    ((ImageView) child).setImageResource(R.drawable.circulo_blanco);
                }
            }
        }

    }

    /**
     * Clase que gestiona los eventos del servicio web de llamada de emergencia.
     *
     * @author David Díaz
     * @version 1.0, 23/06/2014
     */
    private class GISEmergenciaWsdl2CodeEvents implements IWsdl2CodeEvents {

        /**
         * Para trazas.
         */
        private static final String TAG = "GISEmergenciaWsdl2CodeEvents";

        /**
         * Método a ejecutar cuando se inicia la petición.
         */
        public void wsdl2CodeStartedRequest() {

            Log.i(TAG, "wsdl2CodeStartedRequest");

        }

        /**
         * Método para tratar la finalización de la llamada al servicio web de inicio de sesión.
         *
         * @param methodName
         *            nombre del método.
         * @param data
         *            datos.
         */
        public void wsdl2CodeFinished(final String methodName, final Object data) {

            if (VariantConstants.DEBUG) {
                if (data instanceof GISEmergencia) {
                    Log.i(TAG, "mensajeNegocio: " + ((GISEmergencia) data).getMensajeNegocio());
                }
            }

        }

        /**
         * @param ex
         *            excepción producida.
         */
        public void wsdl2CodeFinishedWithException(final Exception ex) {

            Log.e(TAG, Log.getStackTraceString(ex));

        }

        /**
         *
         */
        public void wsdl2CodeEndedRequest() {

            Log.i(TAG, "wsdl2CodeEndedRequest");

        }
    }

}
