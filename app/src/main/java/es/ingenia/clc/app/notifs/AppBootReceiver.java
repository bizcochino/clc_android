package es.ingenia.clc.app.notifs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.UtilsCitas;
import es.ingenia.clc.app.VariantConstants;
import es.ingenia.clc.app.ksoap.model.Cita;

/**
 * Clase para detectar el reinicio del dispositivo y reprogramar las alarmas para las notificaciones.
 *
 * @author David Díaz
 * @version 1.0, 16/09/14
 */
public class AppBootReceiver extends BroadcastReceiver {

    /**
     * Para trazas.
     */
    private static final String TAG = "AppBootReceiver";

    @Override
    public void onReceive(final Context context, final Intent intent) {

        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            Log.i(TAG, "Ahora podemos poner las alarmas incluso después de haber reiniciado el dispositivo");
            fijarAlarmasParaCitas();
        } else {
            Log.e(TAG, "¡Hay un problema!");
        }
    }

    /**
     * Fija las alarmas para hacer las notificaciones de las citas. Es llamado desde el onReceive.
     */
    private static void fijarAlarmasParaCitas() {

        final Map<String, Cita> mapa = UtilsCitas.leerMapaCitasPorNotificar();

        if (!mapa.isEmpty()) {

            final Date fechaHoy = new Date();

            final SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
            final SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm");

            // Retrieve alarm manager from the system
            final AlarmManager alarmManager = (AlarmManager) CLCApp.getAppContext().getSystemService(
                    CLCApp.getAppContext().ALARM_SERVICE);

            final Iterator it = mapa.entrySet().iterator();
            while (it.hasNext()) {
                try {
                    final Map.Entry<String, Cita> pairs = (Map.Entry) it.next();

                    final Cita cita = pairs.getValue();
                    final Date fechaCita = new SimpleDateFormat("dd-MM-yyyy HH:mm", CLCApp.getLocale()).parse(cita
                            .getFechaHora());

                    // Si la fecha de la cita es posterior a la fecha actual, no hay que programar nada
                    if (fechaHoy.compareTo(fechaCita) < 0) {

                        StringBuilder msg = new StringBuilder(CLCApp.getAppContext().getString(R.string.recuerde_que));
                        msg.append(sdfFecha.format(new Date()))
                                .append(CLCApp.getAppContext().getString(R.string.a_las))
                                .append(sdfHora.format(new Date()))
                                .append(CLCApp.getAppContext().getString(R.string.en)).append(cita.getAreaMedica());

                        // Preparamos la fecha del aviso (dos días antes de la fecha de la cita)
                        final Calendar calendar = UtilsCitas.calcularFechaAviso(fechaCita);

                        // TODO Hay que tener en cuenta el problema de las doce de la mañana

                        // Para pruebas, cambiamos el texto y el preaviso de las notificaciones, en un tiempo entre
                        // 2 y 6 minutos
                        if (VariantConstants.DEBUG) {
                            final Random rand = new Random();
                            int MAX = 6;
                            int MIN = 2;
                            int randomNum = rand.nextInt((MAX - MIN) + 1) + MIN;
                            long horaActual = System.currentTimeMillis();
                            calendar.setTimeInMillis(horaActual + randomNum * 60 * 1000);
                            msg = new StringBuilder();
                            msg.append("AppBoot ").append(cita.getCamId()).append(". ").append(randomNum)
                                    .append(" mins después ").append(sdfHora.format(horaActual));
                        }

                        int id = (int) System.currentTimeMillis();

                        // A la clase que se encargará de crear la notificación le pasamos el texto y el id de la
                        // misma.
                        final Intent intent = new Intent(CLCApp.getAppContext(), NotificationTimeAlarm.class);

                        // Si el id de la notificación ya está asociado a una existente, el resultado será que se
                        // modificará
                        intent.putExtra(Constants.PERS_CORREL, cita.getPersCorrel());
                        intent.putExtra(Constants.ID_NOTIFICACION, cita.getIdNotificacion());
                        intent.putExtra(Constants.TEXTO_NOTIFICACION, msg.toString());

                        // Preparamos el Intent
                        final PendingIntent pendingIntent = PendingIntent.getBroadcast(CLCApp.getAppContext(), id,
                                intent, PendingIntent.FLAG_UPDATE_CURRENT);

                        // Register the alert in the system. You have the option to define if the device has to wake
                        // up on the alert or not
                        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                        Log.i(TAG, "Fijada alarma para cita " + cita.getCamId());

                        if (VariantConstants.DEBUG) {
                            UtilsCitas.listarMapaCitasPorNotificar("AppBoot1");
                        }

                    } else {
                        Log.i(TAG, "La cita tiene fecha anterior a la fecha actual, no se procesará ninguna alarma");
                    }

                } catch (ParseException pe) {
                    Log.e(TAG, Log.getStackTraceString(pe));
                } catch (Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }

        } else {
            Log.i(TAG, "No hay citas por notificar");
            // Toast.makeText(CLCApp.getAppContext(), "No hay ninguna cita por notificar", Toast.LENGTH_LONG).show();
        }

    }

}
