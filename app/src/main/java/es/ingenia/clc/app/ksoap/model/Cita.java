package es.ingenia.clc.app.ksoap.model;

import java.util.Hashtable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

/**
 * Clase para modelar la información de las citas obtenidas desde el servicio web de CLC.
 * 
 * @author David Díaz
 * @version 1.0, 14/07/2014
 */
public final class Cita implements KvmSerializable, Comparable<Cita> {

    /**
     * Para trazas.
     */
    // private static final String TAG = "Cita";

    // <cita cam_id="8921023" pers_correl="1208085">
    // <nombre_medico>Marcos Skarmeta Muranda</nombre_medico>
    // <fecha_hora>10-12-2013 16:30</fecha_hora>
    // <edificio>EDIFICIO 4 ROJO PISO 3</edificio>
    // <estacionamiento>Estacionamiento ubicado en el edificio 4 rojo</estacionamiento>
    // <url_mapa>
    // http://www.clc.cl/MOVIL/UBICACION-Y-EDIFICIOS/Central.aspx
    // </url_mapa>
    // <centro_atencion>CLINICA LAS CONDES</centro_atencion>
    // <area_medica>Pediatria</area_medica>
    // <area_medica_especifica>[TODOS]</area_medica_especifica>
    // </cita>

    /**
     * cam_id.
     */
    private String camId;

    /**
     * persCorrel.
     */
    private String persCorrel;

    /**
     * Nombre del médico.
     */
    private String nombreMedico;

    /**
     * Fecha y hora.
     */
    private String fechaHora;

    /**
     * Edificio.
     */
    private String edificio;

    /**
     * Estacionamiento.
     */
    private String estacionamiento;

    /**
     * URL del mapa.
     */
    private String urlMapa;

    /**
     * Centro de atención.
     */
    private String centroAtencion;

    /**
     * Área médica.
     */
    private String areaMedica;

    /**
     * Área médica específica.
     */
    private String areaMedicaEspecifica;

    /**
     * Estado (este campo no viene en el XML).
     */
    private String estado;

    /**
     * ID de la notificación asociada.
     */
    private int    idNotificacion;

    /**
     *
     */
    public Cita() {

    }

    /**
     * @param soapObject
     *            objeto.
     */
    public Cita(final SoapObject soapObject) {

        if (soapObject == null) {
            return;
        }

        // <cita cam_id="8921023" pers_correl="1208085">
        // <nombre_medico>Marcos Skarmeta Muranda</nombre_medico>
        // <fecha_hora>10-12-2013 16:30</fecha_hora>
        // <edificio>EDIFICIO 4 ROJO PISO 3</edificio>
        // <estacionamiento>Estacionamiento ubicado en el edificio 4 rojo</estacionamiento>
        // <url_mapa>
        // http://www.clc.cl/MOVIL/UBICACION-Y-EDIFICIOS/Central.aspx
        // </url_mapa>
        // <centro_atencion>CLINICA LAS CONDES</centro_atencion>
        // <area_medica>Pediatria</area_medica>
        // <area_medica_especifica>[TODOS]</area_medica_especifica>
        // </cita>

        camId = soapObject.getAttribute("cam_id").toString();
        persCorrel = soapObject.getAttribute("pers_correl").toString();

        loadNombreMedico(soapObject);

        loadFechaHora(soapObject);

        loadEdificio(soapObject);

        loadEstacionamiento(soapObject);

        loadUrlMapa(soapObject);

        loadCentroAtencion(soapObject);

        loadAreaMedica(soapObject);

        loadAreaMedicaEspecifica(soapObject);

    }

    /**
     * @param soapObject
     */
    private void loadNombreMedico(final SoapObject soapObject) {
        if (soapObject.hasProperty("nombre_medico")) {
            final Object obj = soapObject.getProperty("nombre_medico");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                SoapPrimitive j = (SoapPrimitive) obj;
                nombreMedico = j.toString();
            } else if (obj instanceof String) {
                nombreMedico = (String) obj;
            }
        }
    }

    /**
     * @param soapObject
     */
    private void loadFechaHora(final SoapObject soapObject) {
        if (soapObject.hasProperty("fecha_hora")) {
            final Object obj = soapObject.getProperty("fecha_hora");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                SoapPrimitive j = (SoapPrimitive) obj;
                fechaHora = j.toString();
            } else if (obj instanceof String) {
                fechaHora = (String) obj;
            }
        }
    }

    /**
     * @param soapObject
     */
    private void loadEdificio(final SoapObject soapObject) {
        if (soapObject.hasProperty("edificio")) {
            final Object obj = soapObject.getProperty("edificio");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj;
                edificio = j.toString();
            } else if (obj instanceof String) {
                edificio = (String) obj;
            }
        }
    }

    /**
     * @param soapObject
     */
    private void loadEstacionamiento(final SoapObject soapObject) {
        if (soapObject.hasProperty("estacionamiento")) {
            final Object obj = soapObject.getProperty("estacionamiento");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj;
                estacionamiento = j.toString();
            } else if (obj instanceof String) {
                estacionamiento = (String) obj;
            }
        }
    }

    /**
     * @param soapObject
     */
    private void loadUrlMapa(final SoapObject soapObject) {
        if (soapObject.hasProperty("url_mapa")) {
            final Object obj = soapObject.getProperty("url_mapa");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj;
                urlMapa = j.toString();
            } else if (obj instanceof String) {
                urlMapa = (String) obj;
            }
        }
    }

    /**
     * @param soapObject
     */
    private void loadCentroAtencion(final SoapObject soapObject) {
        if (soapObject.hasProperty("centro_atencion")) {
            final Object obj = soapObject.getProperty("centro_atencion");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj;
                centroAtencion = j.toString();
            } else if (obj instanceof String) {
                centroAtencion = (String) obj;
            }
        }
    }

    /**
     * @param soapObject
     */
    private void loadAreaMedica(final SoapObject soapObject) {
        if (soapObject.hasProperty("area_medica")) {
            final Object obj = soapObject.getProperty("area_medica");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj;
                areaMedica = j.toString();
            } else if (obj instanceof String) {
                areaMedica = (String) obj;
            }
        }
    }

    /**
     * @param soapObject
     */
    private void loadAreaMedicaEspecifica(final SoapObject soapObject) {
        if (soapObject.hasProperty("area_medica_especifica")) {
            final Object obj = soapObject.getProperty("area_medica_especifica");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj;
                areaMedicaEspecifica = j.toString();
            } else if (obj instanceof String) {
                areaMedicaEspecifica = (String) obj;
            }
        }
    }

    // <cita cam_id="8921023" pers_correl="1208085">
    // <nombre_medico>Marcos Skarmeta Muranda</nombre_medico>
    // <fecha_hora>10-12-2013 16:30</fecha_hora>
    // <edificio>EDIFICIO 4 ROJO PISO 3</edificio>
    // <estacionamiento>Estacionamiento ubicado en el edificio 4 rojo</estacionamiento>
    // <url_mapa>
    // http://www.clc.cl/MOVIL/UBICACION-Y-EDIFICIOS/Central.aspx
    // </url_mapa>
    // <centro_atencion>CLINICA LAS CONDES</centro_atencion>
    // <area_medica>Pediatria</area_medica>
    // <area_medica_especifica>[TODOS]</area_medica_especifica>
    // </cita>

    @Override
    public Object getProperty(final int index) {

        switch (index) {
            case 0:
                return camId;
            case 1:
                return persCorrel;
            case 2:
                return nombreMedico;
            case 3:
                return fechaHora;
            case 4:
                return edificio;
            case 5:
                return estacionamiento;
            case 6:
                return urlMapa;
            case 7:
                return centroAtencion;
            case 8:
                return areaMedica;
            case 9:
                return areaMedicaEspecifica;
            default:
                return null;
        }

    }

    @Override
    public int getPropertyCount() {
        return 10;
    }

    @Override
    public void getPropertyInfo(int index, @SuppressWarnings("rawtypes") Hashtable arg1, final PropertyInfo info) {

        switch (index) {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "cam_id";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "pers_correl";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "nombre_medico";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "fecha_hora";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "edificio";
                break;
            case 5:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "estacionamiento";
                break;
            case 6:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "url_mapa";
                break;
            case 7:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "centro_atencion";
                break;
            case 8:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "area_medica";
                break;
            case 9:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "area_medica_especifica";
                break;
            default:
                break;
        }
    }

    @Override
    public void setProperty(int index, final Object value) {

        switch (index) {
            case 0:
                camId = value.toString();
                break;
            case 1:
                persCorrel = value.toString();
                break;
            case 2:
                nombreMedico = value.toString();
                break;
            case 3:
                fechaHora = value.toString();
                break;
            case 4:
                edificio = value.toString();
                break;
            case 5:
                estacionamiento = value.toString();
                break;
            case 6:
                urlMapa = value.toString();
                break;
            case 7:
                centroAtencion = value.toString();
                break;
            case 8:
                areaMedica = value.toString();
                break;
            case 9:
                areaMedicaEspecifica = value.toString();
                break;
            default:
                break;

        }
    }

    /**
     * Devuelve el camId.
     * 
     * @return
     */
    public String getCamId() {
        return camId;
    }

    /**
     * Devuelve el persCorrel.
     *
     * @return
     */
    public String getPersCorrel() {
        return persCorrel;
    }

    /**
     * @return
     */
    public String getFechaHora() {
        return fechaHora;
    }

    /**
     * Fija el valor del estado de la cita.
     *
     * @param value
     *            valor.
     */
    public void setEstado(final String value) {
        this.estado = value;
    }

    /**
     * Devuelve el estado.
     * 
     * @return
     */
    public String getEstado() {
        return this.estado;
    }

    /**
     * Devuelve el área médica.
     *
     * @return
     */
    public String getAreaMedica() {
        return this.areaMedica;
    }

    /**
     * Devuelve la URL del mapa.
     *
     * @return
     */
    public String getUrlMapa() {
        return this.urlMapa;
    }

    /**
     * Fija el valor de la identificación de la notificación.
     *
     * @param id
     *            identificador de la notificación asociada a la cita.
     */
    public void setIdNotificacion(final int id) {
        this.idNotificacion = id;
    }

    /**
     * Devuelve el valor de la identificación de la notificación.
     *
     * @return
     */
    public int getIdNotificacion() {
        return this.idNotificacion;
    }

    /**
     * Fija el valor del campo nombreMedico.
     * <p/>
     * Este método sólo se usa para depuración.
     *
     * @param value
     *            valor.
     */
    public void setNombreMedico(final String value) {
        this.nombreMedico = value;
    }

    /**
     * Devuelve el valor del campo nombreMedico.
     * <p/>
     * Este método sólo se usa para depuración.
     */
    public String getNombreMedico() {
        return this.nombreMedico;
    }

    @Override
    public int compareTo(final Cita item) {

        return camId.compareTo(item.getCamId());

    }

    @Override
    public boolean equals(final Object obj) {

        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Cita)) {
            return false;
        }

        final Cita cita = (Cita) obj;

        // Clase de utilidad del paquete org.apache.commons.lang3
        return new EqualsBuilder().append(camId, cita.camId).append(persCorrel, cita.persCorrel)
                .append(nombreMedico, cita.nombreMedico).append(fechaHora, cita.fechaHora)
                .append(edificio, cita.edificio).append(estacionamiento, cita.estacionamiento)
                .append(urlMapa, cita.urlMapa).append(centroAtencion, cita.centroAtencion)
                .append(areaMedica, cita.areaMedica).append(areaMedicaEspecifica, cita.areaMedicaEspecifica).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).append(camId).append(persCorrel).append(nombreMedico).append(fechaHora)
                .append(edificio).append(estacionamiento).append(urlMapa).append(centroAtencion).append(areaMedica)
                .append(areaMedicaEspecifica).toHashCode();
    }

    @Override
    public String toString() {

        final StringBuffer str = new StringBuffer("Datos de la cita: [");

        str.append("idNotif: ").append(idNotificacion);
        str.append(", camId: ").append(camId);
        str.append(", persCorrel: ").append(persCorrel);
        str.append(", nombreMédico: ").append(nombreMedico);
        str.append(", estado: ").append(estado);
        str.append(", fechaHora: ").append(fechaHora);
        str.append(", áreaMédica: ").append(areaMedicaEspecifica).append("]");

        return str.toString();

    }

}
