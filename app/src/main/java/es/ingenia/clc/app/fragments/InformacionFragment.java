package es.ingenia.clc.app.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.fragments.interfaces.InterfaceMainMenuFragment;
import es.ingenia.clc.app.view.OswaldTextView;

/**
 * Clase para el fragmento "Información" de la app CLC.
 *
 * @author David Díaz
 * @version 1.0, 23/06/2014
 */
public class InformacionFragment extends CustomFragment implements InterfaceMainMenuFragment {

    // private static final String TAG = "InformacionFragment";

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_informacion, container, false);

    }

    @Override
    public void onActivityCreated(final Bundle savedInstance) {

        super.onActivityCreated(savedInstance);

        // Fijamos el texto de la barra superior
        final OswaldTextView tvTitle = (OswaldTextView) getView().findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.informacion);

        // Programamos el botón atrás sólo si es teléfono. En tabletas lo ocultamos
        final ImageView ivLogoCLC = (ImageView) getView().findViewById(R.id.ivLogoCLC);
        if (CLCApp.isHasPhone()) {
            ivLogoCLC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // getFragmentManager().popBackStackImmediate();
                    getMActivity().onBackPressed();

                }
            });
        } else {
            ivLogoCLC.setVisibility(View.GONE);
        }

        // Google Analytics...
        Utils.notificaGoogleAnalytics(Constants.SCREEN_INFORMACION,
            Constants.SCREEN_INFORMACION, Constants.EVENT_LABEL_NA);
    }

}
