package es.ingenia.clc.app;

/**
 * Constantes para la gestión de los fragmentos y las pestañas del menú principal.
 *
 * @author David Díaz
 * @version 1.0, 19/09/14.
 */
public final class TabConstants {

    /**
     * Constructor privado.
     */
    private TabConstants() {

    }

    /**
     * 
     */
    public static final int MENU_PRINCIPAL          = 0;

    // Fragmentos de nivel 0
    /**
     * 
     */
    public static final int FRAG_1_OPCIONES         = 1;

    /**
     * 
     */
    public static final int FRAG_2_COMO_LLEGAR      = 2;

    /**
     * 
     */
    public static final int FRAG_3_INFO             = 3;

    /**
     * 
     */
    public static final int FRAG_3_HOME             = 4;

    /**
     * 
     */
    public static final int FRAG_4_WEB              = 5;

    /**
     * 
     */
    public static final int FRAG_5_LOGIN            = 6;

    // Fragmentos de nivel 1
    /**
     * 
     */
    public static final int FRAG_1_1_CONFIGURACION  = 11;

    /**
     * 
     */
    public static final int FRAG_5_1_PROXIMAS_CITAS = 51;

    /**
     * 
     */
    public static final int FRAG_5_2_GRUPO_FAMILIAR = 52;

    /**
     * 
     */
    public static final int FRAG_GENERAL_NIVEL_2    = 10;

}
