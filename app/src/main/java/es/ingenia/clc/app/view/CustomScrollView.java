package es.ingenia.clc.app.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * Extensión de la clase ScrollView, necesaria para el carrusel de la página principal.
 *
 * @author David Díaz
 * @version 1.0, 08/07/2014
 */
public class CustomScrollView extends ScrollView {

    /**
     * Para trazas.
     */
    // private static final String TAG = "CustomScrollView";

    // true if we can scroll (not locked)
    // false if we cannot scroll (locked)
    /**
     * Indicador de si es "scrollable".
     */
    private boolean mScrollable = false;

    /**
     * Constructor.
     *
     * @param context
     * @param attrs
     * @param defStyle
     */
    public CustomScrollView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Constructor.
     *
     * @param context
     * @param attrs
     */
    public CustomScrollView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Constructor.
     *
     * @param context
     */
    public CustomScrollView(final Context context) {
        super(context);
    }

    /**
     * Constructor.
     *
     * @param enabled
     */
    public void setScrollingEnabledfinal(boolean enabled) {
        mScrollable = enabled;
    }

    /**
     * @return
     */
    public boolean isScrollable() {
        return mScrollable;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                // if we can scroll pass the event to the superclass
                if (mScrollable) {
                    return super.onTouchEvent(ev);
                }
                // only continue to handle the touch event if scrolling enabled
                return mScrollable; // mScrollable is always false at this point
            default:
                return super.onTouchEvent(ev);
        }

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        // Don't do anything with intercepted touch events if
        // we are not scrollable
        return mScrollable && super.onInterceptTouchEvent(ev);

    }

}
