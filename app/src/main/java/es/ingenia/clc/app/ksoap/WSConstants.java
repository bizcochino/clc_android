package es.ingenia.clc.app.ksoap;

/**
 * Clase para guardar las constantes relacionadas con los distintos servicios web de CLC.
 * <p/>
 * Clase no instanciable.
 * 
 * @author David Díaz
 * @version 1.0, 18/09/2014
 */
public final class WSConstants {

    /**
     * Constructor privado.
     */
    private WSConstants() {
    }

    /**
     * NAMESPACE.
     */
    public static final String NAMESPACE                   = "http://tempuri.org/";

    /**
     * Banners.
     */
    public static final String RECUPERA_BANNERS            = "RecuperaBanners";

    /**
     * URL Banners.
     */
    public static final String URL_RECUPERA_BANNERS        = "http://tempuri.org/RecuperaBanners";

    /**
     * 
     */
    public static final String INICIO_SESION               = "InicioSesion";

    /**
     * URL Inicio Sesión.
     */
    public static final String URL_INICIO_SESION           = "http://tempuri.org/InicioSesion";

    /**
     *
     */
    public static final String RECUPERA_PROXIMAS_CITAS     = "RecuperaProximasCitas";

    /**
     * URL Próximas Citas.
     */
    public static final String URL_RECUPERA_PROXIMAS_CITAS = "http://tempuri.org/RecuperaProximasCitas";

    /**
     *
     */
    public static final String RECUPERA_GRUPO_FAMILIAR     = "RecuperaGrupoFamiliar";

    /**
     * URL Recupera Grupo Familiar.
     */
    public static final String URL_RECUPERA_GRUPO_FAMILIAR = "http://tempuri.org/RecuperaGrupoFamiliar";

    /**
     *
     */
    public static final String GIS_EMERGENCIA              = "GISEmergencia";

    /**
     * URL Llamada Emergencia.
     */
    public static final String URL_GIS_EMERGENCIA          = "http://tempuri.org/GISEmergencia";

    /**
     * Tipos de prestaciones.
     */
    public static final String GIS_TIPOS_PRESTACIONES      = "TiposPrestaciones";

    /**
     * URL Tipos Prestaciones.
     */
    public static final String URL_TIPOS_PRESTACIONES      = "http://tempuri.org/TiposPrestaciones";

    /**
     * Buscar tipos de prestaciones.
     */
    public static final String GIS_BUSCAR_PRESTACIONES     = "BuscarPrestaciones";

    /**
     * URL Buscar tipos de prestaciones.
     */
    public static final String URL_BUSCAR_PRESTACIONES     = "http://tempuri.org/BuscarPrestaciones";

    /**
     * Recupera clave.
     */
    public static final String GIS_RECUPERA_CLAVE          = "RecuperaClave";

    /**
     * URL Recupera clave.
     */
    public static final String URL_RECUPERA_CLAVE          = "http://tempuri.org/RecuperaClave";
}
