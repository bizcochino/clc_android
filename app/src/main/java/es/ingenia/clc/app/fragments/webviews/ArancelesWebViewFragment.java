package es.ingenia.clc.app.fragments.webviews;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.Utils;

/**
 * Clase para el fragmento que muestra un "WebView" la app CLC.
 *
 * @author David Díaz
 * @version 1.0, 23/06/14
 */
public class ArancelesWebViewFragment extends WebViewFragment {

    /**
     * Para trazas.
     */
    // private static final String TAG = "ArancelesWebViewFragment";

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_aranceles_webview, container, false);

    }

    @Override
    public void setTitle() {
        final TextView tvTitle = (TextView) getView().findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.aranceles);
    }

    @Override
    public void invocarURL() {

        Utils.notificaGoogleAnalytics(Constants.SCREEN_ARANCELES,
            Constants.SCREEN_ARANCELES, Constants.EVENT_LABEL_NA);

        mWebview.loadUrl(Constants.URL_ARANCELES);

    }

}
