package es.ingenia.clc.app.banners;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.PreferencesStore;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.VariantConstants;
import es.ingenia.clc.app.ksoap.AppCLC;
import es.ingenia.clc.app.ksoap.IWsdl2CodeEvents;
import es.ingenia.clc.app.ksoap.model.Banner;

/**
 * Adaptador para el carrusel.
 *
 * @author David Díaz
 * @version 1.0, 23/06/2014
 */
// private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
// FragmentStatePagerAdapter da problemas al volver a este fragmento desde, por ejemplo, Configuración.
// Más info en:
// http://stackoverflow.com/questions/18747975/difference-between-fragmentpageradapter-and-fragmentstatepageradapter
public final class ScreenSlidePagerAdapter extends FragmentPagerAdapter {

    /**
     * Para trazas.
     */
    private static final String            TAG                       = "ScreenSlidePagerAdapter";

    /**
     * Para la carga de las imágenes de los banners.
     */
    private static ImageLoader             imageLoader;                                              // = new
                                                                                                      // ImageLoader();

    /**
     * Configuración de los banners, en función del tamaño y la densidad de la pantalla.
     */
    private static int                     bannerConfig              = Utils.checkScreenAndDensity();

    /**
     * Lista de banners
     */
    private static List<Banner>            listaBanners              = new ArrayList<Banner>();

    /**
     * Lista de banners
     */
    private static List<Banner>            auxListaBanners           = new ArrayList<Banner>();

    /**
     * 
     */
    private static ScreenSlidePagerAdapter slidePagerAdapterInstance = null;

    /**
     * Devuelve la instancia de la clase ScreenSlidePagerAdapter
     * 
     * @param fm
     *            FragmentManager.
     * @return
     */
    public static ScreenSlidePagerAdapter getInstance(final FragmentManager fm) {

        if (slidePagerAdapterInstance == null) {
            slidePagerAdapterInstance = new ScreenSlidePagerAdapter(fm);
        }
        return slidePagerAdapterInstance;

    }

    /**
     * Constructor privado. Sólo se accede a él desde el método getInstance(fm).
     *
     * @param fm
     *            FragmentManager.
     */
    private ScreenSlidePagerAdapter(final FragmentManager fm) {

        super(fm);

        // Creamos la instancia del ImageLoader
        imageLoader = new ImageLoader();

        // Consultamos el servicio web que nos devolverá las URL de los banners
        /*
         * Gestor de banners.
         */
        GestorDescargaBanners gestor = new GestorDescargaBanners();
        gestor.cargarBanners();

    }

    /**
     * Refresca la lista de banners del adaptador, y ejecuta un "notifyDataSetChanged" para que se produzca el refresco.
     * <p/>
     * Es llamado desde el onResume() de la clase ScreenSlidePageFragment.
     */
    private void updateListaBanners() {
        listaBanners.clear();
        listaBanners = new ArrayList<Banner>(auxListaBanners);
        this.notifyDataSetChanged();
    }

    /**
     * Refresca la lista de banners.
     * <p/>
     * Es llamado desde el onResume() de la clase HomeFragment.
     */
    public void refreshBannersList() {

        if (auxListaBanners != null && !auxListaBanners.isEmpty()) {
            listaBanners = new ArrayList<Banner>(auxListaBanners);
        }

    }

    /**
     * @param lista
     *            nueva lista de banners.
     */
    private void setAuxListaBanners(final List<Banner> lista) {
        for (final Banner aux : lista) {
            // Sólo añadimos el banner a la lista si tiene una URL válida (no vacía ni nula)
            if (aux.checkURLBanner()) {
                auxListaBanners.add(aux);
            }
        }
        // auxListaBanners = lista;
    }

    /**
     *
     */
    public List<Banner> getListaBanners() {
        // Log.i(TAG, "getListaBanners - listaBanners.size(): " + listaBanners.size());
        return listaBanners;
    }

    /**
     * @return
     */
    private static ScreenSlidePagerAdapter getInstance() {

        return slidePagerAdapterInstance;

    }

    @Override
    public Fragment getItem(int position) {

        // imageLoader.displayImage(data[position], imageView);

        // Log.i(TAG, "getItem " + position);

        return ScreenSlidePageFragment.create(position);

    }

    @Override
    public int getCount() {

        return ((listaBanners == null || listaBanners.isEmpty()) ? 1 : listaBanners.size());

    }

    /**
     * @return
     */
    private static int getNumNewBanners() {

        return ((auxListaBanners == null || auxListaBanners.isEmpty()) ? 1 : auxListaBanners.size());

    }

    /**
     * Clase que se encarga de lanzar la tarea asíncrona para consultar el servicio web de banners.
     * 
     * @author David Díaz
     * @version 1.0, 23/09/2014
     */
    public class GestorDescargaBanners {

        /**
         * Para trazas.
         */
        private static final String TAG = "GestorDescargaBanners";

        /**
         * Llama al servicio web para recuperar el listado de banners. Es invocado desde el constructor de
         * ScreenSlidePagerAdapter.
         */
        protected void cargarBanners() {

            // Obtenemos los datos almacenados en las preferencias
            PreferencesStore.restorePreferences();
            final long ultimaSincBanners = PreferencesStore.getFechaUltimaSincBanners();
            Log.i(TAG, "Tiempo desde la última actualización: " + (System.currentTimeMillis() - ultimaSincBanners));

            if (System.currentTimeMillis() - ultimaSincBanners > (VariantConstants.BANNERS_SINC_TIMEOUT)) {
                final IWsdl2CodeEvents events = new RecuperaBannersWsdl2CodeEvents();
                final AppCLC appCLC = new AppCLC(events, VariantConstants.WSDL_URL_BANNERS, AppCLC.TIMEOUT);
                try {
                    // TODO sólo para pruebas
                    String persCorrel = "1208025";
                    // Si el usuario está logado, incluimos el campo "persCorrel"
                    if (CLCApp.getInicioSesion() != null) {
                        persCorrel = CLCApp.getInicioSesion().getPersCorrel();
                    }
                    final String idioma = Constants.IDIOMA;
                    appCLC.recuperaBannersAsync(this, persCorrel, idioma);

                } catch (final Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            } else {
                Log.i(TAG, "No hay que llamar nuevamente al servicio de banners");
            }

        }

        /**
         * Este método es invocado desde el onPostExecute de la clase AppCLC.BannerService.
         *
         * @param lista
         *            lista de banners.
         */
        public void setAuxListaBanners(final List<Banner> lista) {

            if (ScreenSlidePagerAdapter.getInstance() != null) {
                Log.i(TAG, "mPager - actualizamos lista de banners");
                ScreenSlidePagerAdapter.getInstance().setAuxListaBanners(lista);
            }

        }

        /**
         * Este método es invocado desde el onPostExecute de la clase AppCLC.BannerService.
         *
         * @param lista
         *            lista de banners.
         */
        public void descargarImagenesBanners(final List<Banner> lista) {

            imageLoader.downloadImages(lista, bannerConfig);

            /*
             * imageLoader.displayImage(ScreenSlidePagerAdapter.getListaBanners().get(mPageNumber)
             * .getBannerConfiguration(bannerConfig), imageView);
             */
        }

        /**
         * Clase que gestiona los eventos de la recuperación de los banners.
         *
         * @author David Díaz
         * @version 1.0, 03/10/2014
         */
        private class RecuperaBannersWsdl2CodeEvents implements IWsdl2CodeEvents {

            /**
             * Para trazas.
             */
            private static final String TAG = "RecuperarBannersWsdl2CodeEvents";

            /**
             *
             */
            public void wsdl2CodeStartedRequest() {

                Log.i(TAG, "wsdl2CodeStartedRequest");

            }

            /**
             * Método para tratar la finalización de la llamada al servicio web de banners.
             *
             * @param methodName
             *            nombre del método.
             * @param data
             *            datos.
             */
            public void wsdl2CodeFinished(final String methodName, final Object data) {

                Log.i(TAG, "wsdl2CodeFinished, method: " + methodName);

                // Actualizamos la fecha de la sincronización de los banners
                PreferencesStore.setFechaUltimaSincBanners();

            }

            /**
             * La llamada ha finalizado con excepción.
             *
             * @param ex
             *            excepción producida.
             */
            public void wsdl2CodeFinishedWithException(final Exception ex) {

                Log.i(TAG, "wsdl2CodeFinishedWithException");
                Log.e(TAG, Log.getStackTraceString(ex));

            }

            /**
             *
             */
            public void wsdl2CodeEndedRequest() {

                Log.i(TAG, "wsdl2CodeEndedRequest");

            }
        }
    }

    /**
     * Clase para gestionar los fragmentos del carrusel.
     *
     * @author David Díaz
     * @version 1.0, 23/09/2014
     */
    public static class ScreenSlidePageFragment extends Fragment {

        /**
         * Para trazas.
         */
        private static final String TAG      = "ScreenSlidePageFragment";

        /**
         * The argument key for the page number this fragment represents.
         */
        private static final String ARG_PAGE = "page";

        /**
         * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
         */
        private int                 mPageNumber;

        /**
         * Construye un nuevo fragmento para el número de página indicado.
         *
         * @param pageNumber
         *            número de página.
         * @return ScreenSlidePageFragment
         */
        public static ScreenSlidePageFragment create(final int pageNumber) {

            final ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();

            // Hay que pasar los argumentos a través de un Bundle
            final Bundle args = new Bundle();
            args.putInt(ARG_PAGE, pageNumber);
            fragment.setArguments(args);

            return fragment;

        }

        @Override
        public final void onCreate(final Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);
            // Log.e(TAG, "onCreate de ScreenSlidePageFragment");

            if (getArguments() != null) {
                // Log.i(TAG, "arg: " + getArguments().getInt(ARG_PAGE));
                mPageNumber = getArguments().getInt(ARG_PAGE);
            } else {
                Log.e(TAG, "getArguments es null");
            }

        }

        @Override
        public final View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                final Bundle savedInstanceState) {

            return inflater.inflate(R.layout.fragment_screen_slide_page, container, false);

        }

        @Override
        public void onResume() {

            super.onResume();

            // Aquí es donde hay que cargar la imagen del banner que corresponda

            if (ScreenSlidePagerAdapter.getNumNewBanners() > 1) {
                slidePagerAdapterInstance.updateListaBanners();

                final ImageView imageView = (ImageView) getView().findViewById(R.id.ivBanner);
                if (imageView != null) {
                    // Log.i(TAG, "mPageNumber: " + mPageNumber);
                    imageLoader.displayImage(listaBanners.get(mPageNumber).getBannerConfiguration(bannerConfig),
                            imageView);
                }
            }

        }

        @Override
        public void onDetach() {

            super.onDetach();

            // Cuando volvamos, hay que empezar desde cero
            slidePagerAdapterInstance = null;

        }
    }

}
