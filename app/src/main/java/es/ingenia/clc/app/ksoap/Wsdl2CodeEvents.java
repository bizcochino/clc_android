package es.ingenia.clc.app.ksoap;

import android.util.Log;

/**
 * Created by David Díaz on 14/07/14.
 */
public class Wsdl2CodeEvents implements IWsdl2CodeEvents {

    /** Para trazas. */
    private static final String TAG = "Wsdl2CodeEventds";

    /**
     *
     */
    public void wsdl2CodeStartedRequest() {

        Log.i(TAG, "wsdl2CodeStartedRequest");

    }

    /**
     * @param methodName
     *            nombre del método.
     * @param data
     *            datos.
     */
    public void wsdl2CodeFinished(final String methodName, final Object data) {

        Log.i(TAG, "wsdl2CodeFinished, method: " + methodName);

    }

    /**
     * @param ex excepción producida.
     */
    public void wsdl2CodeFinishedWithException(final Exception ex) {

        Log.i(TAG, "wsdl2CodeFinishedWithException");
        Log.e(TAG, Log.getStackTraceString(ex));

    }

    /**
     *
     */
    public void wsdl2CodeEndedRequest() {

        Log.i(TAG, "wsdl2CodeEndedRequest");

    }
}
