package es.ingenia.clc.app.ksoap.model;

import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

/**
 * Clase para modelar la información de los tipos de prestaciones.
 *
 * @author David Díaz
 * @version 1.0, 02/10/2014
 */
public class BuscarPrestaciones implements KvmSerializable {

    // TODO: Este clase está sin implementar del todo. Sólo se hizo para probar que los servicios funcionaban
    // correctamente.

    /**
     * Para trazas.
     */
    // private static final String TAG = "BuscarPrestaciones";

    /**
     * ResultProceso.
     */
    private String resultProceso;

    /**
     * MsgProceso.
     */
    private String msgProceso;

    /**
     * Constructor.
     */
    public BuscarPrestaciones() {

    }

    /**
     * Constructor.
     *
     * @param soapObject
     *            objeto soap.
     */
    public BuscarPrestaciones(SoapObject soapObject) {

        if (soapObject == null) {
        }

    }

    @Override
    public Object getProperty(int arg0) {

        switch (arg0) {
            case 0:
                return resultProceso;
            case 1:
                return msgProceso;
            default:
                return null;
        }
    }

    @Override
    public void setProperty(int index, Object value) {

        switch (index) {
            case 0:
                resultProceso = value.toString();
                break;
            case 1:
                msgProceso = value.toString();
                break;
            default:
                break;
        }
    }

    @Override
    public void getPropertyInfo(final int index, @SuppressWarnings("rawtypes") Hashtable arg1, final PropertyInfo info) {

        switch (index) {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "resultProceso";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "msgProceso";
                break;
            default:
                break;
        }

    }

    @Override
    public int getPropertyCount() {
        return 2;
    }

}
