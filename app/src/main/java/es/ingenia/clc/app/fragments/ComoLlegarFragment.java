package es.ingenia.clc.app.fragments;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.fragments.interfaces.InterfaceMainMenuFragment;
import es.ingenia.clc.app.view.OswaldTextView;

/**
 * Clase para el fragmento "Cómo llegar" de la app CLC.
 *
 * @author David Díaz
 * @version 1.0, 23/06/2014
 */
public class ComoLlegarFragment extends CustomFragment implements InterfaceMainMenuFragment {

    /**
     * Para trazas.
     */
    //private static final String TAG = "ComoLlegarFragment";

    /**
     * Imagen del hospital 1
     */
    private static Bitmap       chicureo;

    /**
     * Imagen del hospital 2
     */
    private static Bitmap       estoril;

    @Override
    public void onCreate(final Bundle savedInstance) {

        super.onCreate(null);

        estoril = decodeSampledBitmapFromResource(getResources(), R.drawable.hospital, CLCApp.getScreenWidth(),
                CLCApp.getScreenWidth());

        chicureo = decodeSampledBitmapFromResource(getResources(), R.drawable.chicureo, CLCApp.getScreenWidth(),
                CLCApp.getScreenWidth());

    }

    @Override
    public final View onCreateView(final LayoutInflater inflater, final ViewGroup container,
            final Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_como_llegar, container, false);

    }

    @Override
    public final void onActivityCreated(final Bundle savedInstance) {

        super.onActivityCreated(savedInstance);

        // Fijamos el título
        final OswaldTextView tvTitle = (OswaldTextView) getView().findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.como_llegar);

        final ImageView ivEstoril = (ImageView) getView().findViewById(R.id.ivEstoril);
        ivEstoril.setImageBitmap(estoril);

        final ImageView ivUbicacionEstoril = (ImageView) getView().findViewById(R.id.ivUbicacionEstoril);
        ivUbicacionEstoril.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                // Google Analytics...
                Utils.notificaGoogleAnalytics(Constants.SCREEN_COMO_LLEGAR,
                    Constants.EVENT_ACTION_COMO_LLEGAR_A, Constants.EVENT_LABEL_ESTORIL);

                startLocationActivity(Constants.LAT_HSPTL_ESTORIL, Constants.LON_HSPTL_ESTORIL);

            }
        });

        final ImageView ivChicureo = (ImageView) getView().findViewById(R.id.ivChicureo);
        ivChicureo.setImageBitmap(chicureo);

        final ImageView ivUbicacionChicureo = (ImageView) getView().findViewById(R.id.ivUbicacionChicureo);
        ivUbicacionChicureo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                // Google Analytics...
                Utils.notificaGoogleAnalytics(Constants.SCREEN_COMO_LLEGAR,
                    Constants.EVENT_ACTION_COMO_LLEGAR_A, Constants.EVENT_LABEL_CHICUREO);

                startLocationActivity(Constants.LAT_HSPTL_CHICUREO, Constants.LON_HSPTL_CHICUREO);

            }
        });

        // Google Analytics... dejamos constancia de la vista de la página
        Utils.notificaGoogleAnalytics(Constants.SCREEN_COMO_LLEGAR,
            Constants.SCREEN_COMO_LLEGAR, Constants.EVENT_LABEL_NA);

    }

    /**
     * Método alternativo para el reescalado de imágenes (no se usa).
     *
     * @param res
     * @param resId
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    private Bitmap decodeSampledBitmapFromResourceMemOpt(final Resources res, final int resId, int reqWidth,
            int reqHeight) {

        InputStream is = null;

        byte[] byteArr = new byte[0];
        final byte[] buffer = new byte[1024];
        int len;
        int count = 0;

        try {
            is = res.openRawResource(resId);
            while ((len = is.read(buffer)) > -1) {
                if (len != 0) {
                    if (count + len > byteArr.length) {
                        byte[] newbuf = new byte[(count + len) * 2];
                        System.arraycopy(byteArr, 0, newbuf, 0, count);
                        byteArr = newbuf;
                    }

                    System.arraycopy(buffer, 0, byteArr, count, len);
                    count += len;
                }
            }

            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(byteArr, 0, count, options);

            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            return BitmapFactory.decodeByteArray(byteArr, 0, count, options);

        } catch (Exception e) {
            return null;
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException ioe) {
                }
            }

        }
    }

    /**
     * Método de utilidad para reescalar imágenes (propuesta de Google).
     *
     * @param res
     * @param resId
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    private Bitmap decodeSampledBitmapFromResource(final Resources res, final int resId, final int reqWidth,
            final int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        return BitmapFactory.decodeResource(res, resId, options);

    }

    /**
     * Método auxiliar para el reescalado de las imágenes.
     * 
     * @param options
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    private int calculateInSampleSize(final BitmapFactory.Options options, final int reqWidth, final int reqHeight) {

        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;

    }

    /**
     * Inicia una nueva actividad como el "Cómo llegar" desde la posición actual del usuario hasta el hospital indicado.
     *
     * @param lat
     *            latitud
     * @param lon
     *            longitud
     */
    private void startLocationActivity(final double lat, final double lon) {

        // Tenemos localización, seguimos adelante
        final StringBuffer url = new StringBuffer("http://maps.google.com/maps?saddr=");
        url.append(CLCApp.getMiLatitud()).append(',').append(CLCApp.getMiLongitud());
        url.append("&daddr=").append(lat).append(',').append(lon);
        url.append("&dirflg=w");

        try {
            final Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(url.toString()));
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            startActivity(intent);
        } catch (final Exception ex) {
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url.toString()));
            startActivity(intent);
        }

    }

}
