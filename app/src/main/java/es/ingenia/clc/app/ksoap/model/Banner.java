package es.ingenia.clc.app.ksoap.model;

import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import es.ingenia.clc.app.Constants;

/**
 * Clase para modelar la información de los banners obtenidos desde el servicio web de CLC.
 *
 * @author David Díaz
 * @version 1.0, 15/07/2014
 */
public class Banner implements KvmSerializable {

    /**
     * Para trazas.
     */
    // private static final String TAG = "Banner";

    // <banner hora="10:20" fecha="06/12/2013" id="1">
    // <img id="ipadretina">http://www.clc.cl/appmovil/banner/image1.png</img>
    // <img id="androidPhoneXhdpi">http://www.clc.cl/appmovil/banner/image1.png</img>
    // <img id="iphoneretina">http://www.clc.cl/appmovil/banner/image1.png</img>
    // <img id="androidTabletMdpi">http://www.clc.cl/appmovil/banner/image1.png</img>
    // </banner>

    /**
     * hora.
     */
    private String hora;

    /**
     * fecha.
     */
    private String fecha;

    /**
     * id.
     */
    private String id;

    /**
     * androidPhoneXhdpi.
     */
    private String androidPhoneHdpi;

    /**
     * androidPhoneXhdpi.
     */
    private String androidPhoneXhdpi;

    /**
     * androidTabletMdpi.
     */
    private String androidTabletMdpi;

    /**
     * androidTabletMdpi.
     */
    private String androidTabletHdpi;

    /**
     * Constructor.
     */
    public Banner() {

    }

    /**
     * Constructor.
     *
     * @param soapObject objeto.
     */
    public Banner(final SoapObject soapObject) {

        if (soapObject == null) {
            return;
        }

        // <banner hora="10:20" fecha="06/12/2013" id="1">
        // <img id="ipadretina">http://www.clc.cl/appmovil/banner/image1.png</img>
        // <img id="androidPhoneXhdpi">http://www.clc.cl/appmovil/banner/image1.png</img>
        // <img id="iphoneretina">http://www.clc.cl/appmovil/banner/image1.png</img>
        // <img id="androidTabletMdpi">http://www.clc.cl/appmovil/banner/image1.png</img>
        // </banner>

        fecha = soapObject.getAttribute("fecha").toString();
        hora = soapObject.getAttribute("hora").toString();
        id = soapObject.getAttribute("id").toString();

        if (soapObject.hasProperty("img")) {
            final int size = soapObject.getPropertyCount();
            for (int i = 0; i < size; i++) {
                final Object obj = soapObject.getProperty(i);
                if (obj != null && obj.getClass().equals(SoapPrimitive.class)) {
                    final SoapPrimitive aux = (SoapPrimitive) obj;
                    if (aux.hasAttribute("id")) {
                        loadTipoById(aux);
                    }

                }
            }

        }

    }

    /**
     * @param aux
     */
    private void loadTipoById(final SoapPrimitive aux) {

        final String tipo = (String) aux.getAttribute("id");
        if (tipo.equals("androidPhoneHdpi")) {
            androidPhoneHdpi = (String) aux.getValue();
        } else if (tipo.equals("androidPhoneXhdpi")) {
            androidPhoneXhdpi = (String) aux.getValue();
        } else if (tipo.equals("androidTabletMdpi")) {
            androidTabletMdpi = (String) aux.getValue();
        } else if (tipo.equals("androidTabletHdpi")) {
            androidTabletHdpi = (String) aux.getValue();
        }
    }

    // <banner hora="10:20" fecha="06/12/2013" id="1">
    // <img id="ipadretina">http://www.clc.cl/appmovil/banner/image1.png</img>
    // <img id="androidPhoneXhdpi">http://www.clc.cl/appmovil/banner/image1.png</img>
    // <img id="iphoneretina">http://www.clc.cl/appmovil/banner/image1.png</img>
    // <img id="androidTabletMdpi">http://www.clc.cl/appmovil/banner/image1.png</img>
    // </banner>

    @Override
    public Object getProperty(final int index) {

        switch (index) {
            case 0:
                return fecha;
            case 1:
                return hora;
            case 2:
                return id;
            case 3:
                return androidPhoneHdpi;
            case 4:
                return androidPhoneXhdpi;
            case 5:
                return androidTabletMdpi;
            case 6:
                return androidTabletHdpi;
            default:
                return null;
        }

    }

    @Override
    public int getPropertyCount() {
        return 5;
    }

    @Override
    public void getPropertyInfo(final int index, @SuppressWarnings("rawtypes") Hashtable arg1, final PropertyInfo info) {

        switch (index) {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "fecha";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "hora";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "id";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "androidPhoneHdpi";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "androidPhoneXhdpi";
                break;
            case 5:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "androidTabletMdpi";
                break;
            case 6:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "androidTabletHdpi";
                break;
            default:
                break;
        }
    }

    @Override
    public void setProperty(final int index, final Object value) {

        switch (index) {
            case 0:
                fecha = value.toString();
                break;
            case 1:
                hora = value.toString();
                break;
            case 2:
                id = value.toString();
                break;
            case 3:
                androidPhoneHdpi = value.toString();
                break;
            case 4:
                androidPhoneXhdpi = value.toString();
                break;
            case 5:
                androidTabletMdpi = value.toString();
                break;
            case 6:
                androidTabletHdpi = value.toString();
                break;
            default:
                break;

        }
    }

    /**
     * Devuelve el valor del campo androidPhoneHdpi.
     *
     * @return
     */
    public String getAndroidPhoneHdpi() {
        return androidPhoneHdpi;
    }

    /**
     * Devuelve el valor del campo androidPhoneXhdpi.
     *
     * @return
     */
    public String getAndroidPhoneXhdpi() {
        return androidPhoneXhdpi;
    }

    /**
     * Devuelve el valor del campo androidTabletMdpi.
     *
     * @return
     */
    public String getAndroidTabletMdpi() {
        return androidTabletMdpi;
    }

    /**
     * Devuelve el valor del campo androidTabletHdpi.
     *
     * @return
     */
    public String getAndroidTabletHdpi() {
        return androidTabletHdpi;
    }

    /**
     * Devuelve el campo que indica la URL del banner adecuado a una determinada densidad y tamaño de pantalla.
     *
     * @param conf configuración de pantalla y densidad.
     * @return
     */
    public String getBannerConfiguration(int conf) {

        switch (conf) {
            case Constants.PHONE_HDPI:
                return androidPhoneHdpi;
            case Constants.PHONE_XHDPI:
                return androidPhoneXhdpi;
            case Constants.TABLET_MDPI:
                return androidTabletMdpi;
            case Constants.TABLET_HDPI:
                return androidTabletHdpi;
            default:
                return androidPhoneHdpi;
        }
    }

    /**
     * Devuelve true si este banner tiene al menos una URL válida (no vacía ni nula).
     *
     * @return
     */
    public boolean checkURLBanner() {

        return isURLValid(androidPhoneHdpi) || isURLValid(androidPhoneXhdpi) || isURLValid(androidTabletHdpi) || isURLValid(androidTabletMdpi);

    }

    /**
     * Devuelve falso si la url asociada a una configuración es nula o vacía.
     *
     * @param url
     * @return
     */
    private boolean isURLValid(final String url) {

        if ((url == null) || "".equals(url)) {
            return false;
        }
        return true;

    }

}
