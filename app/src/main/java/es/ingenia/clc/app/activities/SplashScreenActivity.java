package es.ingenia.clc.app.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.Utils;

/**
 * Pantalla de inicio de la app.
 *
 * @author David Díaz
 * @version 1.0, 23/06/2014
 */
public final class SplashScreenActivity extends BaseActivity {

    /**
     * Para trazas.
     */
    // private static final String TAG = "SplashScreenActivity";

    /**
     * Tiempo en milisegundos.
     */
    private static final int SPLASH_TIME_OUT = 1250;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Lo primero es controlar si tenemos teléfono o no... (por defecto, sí)
        if (!CLCApp.getAppContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
            CLCApp.setHasPhone(false);
        }

        CLCApp.setScreenWidth(Utils.detectScreenWidth());

        setContentView(R.layout.activity_splash);

        CLCApp.setPortrait(getResources().getBoolean(R.bool.portrait_only));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

}
