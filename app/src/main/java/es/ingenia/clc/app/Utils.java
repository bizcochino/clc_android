package es.ingenia.clc.app;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Utilidades varias. Clase no instanciable.
 *
 * @author David Díaz
 * @version 1.0, 10/07/14.
 */
public final class Utils {

    /**
     * Para trazas.
     */
    private static final String TAG = "Utils";

    /**
     * Constructor privado.
     */
    private Utils() {

    }

    /**
     * Devuelve true si una cadena es numérica.
     *
     * @param str
     *            cadena a evaluar
     * @return boolean.
     */
    public static boolean isNumber(final String str) {

        return str.matches("-?\\d+(\\.\\d+)?");

    }

    /**
     * Devuelve true si la cadena representa a un carácter que es número o letra.
     *
     * @param str
     *            cadena a evaluar.
     * @return boolean.
     */
    public static boolean isNumberOrLetter(final String str) {

        return str.matches("[a-zA-Z0-9]");

    }

    /**
     * Devuelve true si la cadena representa a un carácter que es número o letra.
     *
     * @param str
     *            cadena a evaluar.
     * @return boolean.
     */
    public static boolean isValidRUTCharacter(final String str) {

        return str.matches("[a-zA-Z0-9]*[-][a-zA-Z0-9]");

    }

    /**
     * Muestra un "dialog" con un texto determinado.
     *
     * @param theContext
     *            el contexto.
     * @param title
     *            título de la ventana.
     * @param messageText
     *            mensaje a mostrar.
     */
    public static void showOkDialogWithText(final Context theContext, final String title, final String messageText) {

        if (theContext != null) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(theContext);
            builder.setTitle(title);
            builder.setMessage(messageText);
            builder.setCancelable(true);
            builder.setPositiveButton(CLCApp.getAppContext().getString(R.string.aceptar), null);
            final AlertDialog dialog = builder.create();
            dialog.show();
        } else {
            Log.e(TAG, "Imposible mostrar diálogo: ¡el contexto es nulo!");
        }

    }

    /**
     * Devuelve la fecha actual en formato dd/MM/yyyy.
     *
     * @return cadena de texto.
     */
    public static String getCurrentDate() {

        final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        final Date date = new Date();

        return dateFormat.format(date);

    }

    /**
     * Devuelve la hora actual en un determinado formato.
     *
     * @return cadena de texto.
     */
    public static String getCurrentTime(final String formato) {

        final DateFormat dateFormat = new SimpleDateFormat(formato);
        final Date date = new Date();

        return dateFormat.format(date);

    }

    /**
     * Envía una notificación a Google Analytics.
     * 
     * @param screen
     *            pantalla desde la que se llama.
     * @param action
     *            acción realizada.
     * @param label
     *            label.
     */
    public static void notificaGoogleAnalytics(final String screen, final String action, final String label) {

        try {
            final Tracker t = CLCApp.getTracker(CLCApp.TrackerName.APP_TRACKER);
            t.setScreenName(screen);
            t.enableAdvertisingIdCollection(false);
            // .setValue(value)
            t.send(new HitBuilders.EventBuilder().setCategory(Constants.EVENT_CATEGORY_SERVICES).setAction(action)
                    .setLabel(label).build());

        } catch (final Exception ex) {
            Log.e(TAG, Log.getStackTraceString(ex));
        }
    }

    /**
     * Para trazas.
     *
     * @param str
     *            cadena a mostrar.
     */
    public static void trazaTiempo(final String str) {

        Log.i(TAG, System.currentTimeMillis() + " --> [" + str + "]");

    }

    /**
     * Escribe un fichero de texto.
     *
     * @param nombreFichero
     *            nombre del fichero a crear.
     * @param contenido
     *            contenido a escribir en el fichero.
     */
    public static boolean escribirFichero(final String nombreFichero, final String contenido) {

        boolean result = true;

        // Si no hay nada que escribir, nos ahorramos el viaje
        if (!"".equals(contenido)) {

            OutputStreamWriter osw = null;
            result = false;

            try {
                osw = new OutputStreamWriter(CLCApp.getAppContext().openFileOutput(nombreFichero, Context.MODE_PRIVATE));
                osw.write(contenido);
                osw.flush();
                result = true;
            } catch (final Exception e) {
                throw new RuntimeException(e);
            } finally {
                if (osw != null) {
                    try {
                        osw.close();
                    } catch (final IOException e) {
                        Log.e(TAG, Log.getStackTraceString(e));
                    }
                }

            }
        }

        return result;

    }

    /**
     * Detecta la densidad de la pantalla y la almacena en una variable del contexto.
     */
    public static void checkDisplayDensity() {

        int density = CLCApp.getAppContext().getResources().getDisplayMetrics().densityDpi;

        switch (density) {
            case DisplayMetrics.DENSITY_LOW:
                CLCApp.setDisplayDensity(DisplayMetrics.DENSITY_LOW);
                // Toast.makeText(CLCApp.getAppContext(), "LDPI", Toast.LENGTH_LONG).show();
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                CLCApp.setDisplayDensity(DisplayMetrics.DENSITY_MEDIUM);
                // Toast.makeText(CLCApp.getAppContext(), "MDPI", Toast.LENGTH_LONG).show();
                break;
            case DisplayMetrics.DENSITY_HIGH:
                CLCApp.setDisplayDensity(DisplayMetrics.DENSITY_HIGH);
                // Toast.makeText(CLCApp.getAppContext(), "HDPI", Toast.LENGTH_LONG).show();
                break;
            case DisplayMetrics.DENSITY_XHIGH:
            default:
                CLCApp.setDisplayDensity(DisplayMetrics.DENSITY_XHIGH);
                // Toast.makeText(CLCApp.getAppContext(), "XHDPI", Toast.LENGTH_LONG).show();
                break;
        }
    }

    /**
     * Detecta el tamaño de la pantalla del dispositivo y lo almacena en una variable del contexto.
     */
    public static void checkScreenSize() {

        int screenSize = CLCApp.getAppContext().getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK;

        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_SMALL:
                CLCApp.setScreenSize(Configuration.SCREENLAYOUT_SIZE_SMALL);
                // Toast.makeText(CLCApp.getAppContext(), "Small screen", Toast.LENGTH_LONG).show();
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                CLCApp.setScreenSize(Configuration.SCREENLAYOUT_SIZE_NORMAL);
                // Toast.makeText(CLCApp.getAppContext(), "Normal screen", Toast.LENGTH_LONG).show();
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                CLCApp.setScreenSize(Configuration.SCREENLAYOUT_SIZE_LARGE);
                // Toast.makeText(CLCApp.getAppContext(), "Large screen", Toast.LENGTH_LONG).show();
                break;
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                CLCApp.setScreenSize(Configuration.SCREENLAYOUT_SIZE_XLARGE);
                // Toast.makeText(CLCApp.getAppContext(), "XLarge screen", Toast.LENGTH_LONG).show();
                break;
            default:
                CLCApp.setScreenSize(Configuration.SCREENLAYOUT_SIZE_UNDEFINED);
                // Toast.makeText(CLCApp.getAppContext(), "Screen size is neither large, normal or small",
                // Toast.LENGTH_LONG).show();
        }
    }

    /**
     * @return
     */
    public static int detectScreenWidth() {

        final WindowManager wm = (WindowManager) CLCApp.getAppContext().getSystemService(Context.WINDOW_SERVICE);
        final Display display = wm.getDefaultDisplay();
        final Point size = new Point();

        int width;

        // Hay que diferenciar con las apis más antiguas...
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
            width = (size.x) / 2;
        } else {
            width = display.getWidth() / 2; // deprecated
        }

        return width;

    }

    /**
     * Establece un tipo de configuración de pantalla en función de su tamaño y densidad.
     */
    public static int checkScreenAndDensity() {

        int bitmask = CLCApp.getDisplayDensity() | CLCApp.getScreenSize();
        Log.i(TAG, "density + size: " + bitmask);

        switch (bitmask) {
            case (Configuration.SCREENLAYOUT_SIZE_SMALL | DisplayMetrics.DENSITY_LOW):
            case (Configuration.SCREENLAYOUT_SIZE_NORMAL | DisplayMetrics.DENSITY_LOW):
            case (Configuration.SCREENLAYOUT_SIZE_SMALL | DisplayMetrics.DENSITY_MEDIUM):
            case (Configuration.SCREENLAYOUT_SIZE_NORMAL | DisplayMetrics.DENSITY_MEDIUM):
            case (Configuration.SCREENLAYOUT_SIZE_SMALL | DisplayMetrics.DENSITY_HIGH):
            case (Configuration.SCREENLAYOUT_SIZE_NORMAL | DisplayMetrics.DENSITY_HIGH):
                Log.i(TAG, "Configuración phone hdpi");
                return Constants.PHONE_HDPI;

            case (Configuration.SCREENLAYOUT_SIZE_NORMAL | DisplayMetrics.DENSITY_XHIGH):
            case (Configuration.SCREENLAYOUT_SIZE_SMALL | DisplayMetrics.DENSITY_XHIGH):
                Log.i(TAG, "Configuración phone xhdpi");
                return Constants.PHONE_XHDPI;

            case (Configuration.SCREENLAYOUT_SIZE_LARGE | DisplayMetrics.DENSITY_LOW):
            case (Configuration.SCREENLAYOUT_SIZE_XLARGE | DisplayMetrics.DENSITY_LOW):
            case (Configuration.SCREENLAYOUT_SIZE_LARGE | DisplayMetrics.DENSITY_MEDIUM):
            case (Configuration.SCREENLAYOUT_SIZE_XLARGE | DisplayMetrics.DENSITY_MEDIUM):
                Log.i(TAG, "Configuración tablet mdpi");
                return Constants.TABLET_MDPI;

            case (Configuration.SCREENLAYOUT_SIZE_LARGE | DisplayMetrics.DENSITY_HIGH):
            case (Configuration.SCREENLAYOUT_SIZE_XLARGE | DisplayMetrics.DENSITY_HIGH):
            case (Configuration.SCREENLAYOUT_SIZE_LARGE | DisplayMetrics.DENSITY_XHIGH):
            case (Configuration.SCREENLAYOUT_SIZE_XLARGE | DisplayMetrics.DENSITY_XHIGH):
                Log.i(TAG, "Configuración tablet hdpi");
                return Constants.TABLET_HDPI;

            default:
                Log.i(TAG, "default");
                return Constants.PHONE_HDPI;
        }

    }

    /**
     * Comprueba si el dispositivo tiene conexión a internet.
     *
     * @return
     */
    public static boolean isOnline() {

        final ConnectivityManager cm = (ConnectivityManager) CLCApp.getAppContext().getSystemService(
                Context.CONNECTIVITY_SERVICE);
        final NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnectedOrConnecting();

    }

}
