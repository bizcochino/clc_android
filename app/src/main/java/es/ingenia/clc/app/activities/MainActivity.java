package es.ingenia.clc.app.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.GestorCitas;
import es.ingenia.clc.app.PreferencesStore;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.TabConstants;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.UtilsCitas;
import es.ingenia.clc.app.VariantConstants;
import es.ingenia.clc.app.fragments.ComoLlegarFragment;
import es.ingenia.clc.app.fragments.CustomFragment;
import es.ingenia.clc.app.fragments.HomeFragment;
import es.ingenia.clc.app.fragments.InformacionFragment;
import es.ingenia.clc.app.fragments.OpcionesFragment;
import es.ingenia.clc.app.fragments.UsuarioLoginFragment;
import es.ingenia.clc.app.fragments.interfaces.InterfaceCustomFragment;
import es.ingenia.clc.app.fragments.webviews.ReservaWebViewFragment;
import es.ingenia.clc.app.ksoap.AppCLC;
import es.ingenia.clc.app.ksoap.AppCLCArancel;
import es.ingenia.clc.app.ksoap.IWsdl2CodeEvents;
import es.ingenia.clc.app.ksoap.model.ProximasCitas;
import es.ingenia.clc.app.ksoap.model.TiposPrestaciones;
import es.ingenia.clc.app.notifs.AppBootReceiver;
import es.ingenia.clc.app.view.ToolbarImageView;

/**
 * Actividad principal de la aplicación de CLC.
 *
 * @author David Díaz
 * @version 1.0, 23/06/2014
 */
public class MainActivity extends BaseActivity implements LocationListener,
        GooglePlayServicesClient.ConnectionCallbacks, GooglePlayServicesClient.OnConnectionFailedListener {

    /**
     * Para trazas.
     */
    private static final String               TAG                           = "MainActivity";

    /**
     * Timeout.
     */
    private static final int                  CONNECT_FAILURE_RESOL_REQUEST = 9000;

    /**
     * Intervalo de actualización.
     */
    private static final int                  UPDATE_INTERVAL               = 5;

    /**
     * Intervalo de actualización.
     */
    private static final int                  FASTEST_INTERVAL              = 1;

    /**
     * Flag que muestra si está activa la localización.
     */
    private static boolean                    locationEnabled               = false;

    /**
     * Localización.
     */
    private LocationClient                    mLocationClient;

    /**
     * Localización (para el caso de que getLastLocation sea null...
     */
    private LocationRequest                   mLocationRequest;

    /**
     * Fragmento para las opciones.
     */
    private OpcionesFragment                  opcFragment;

    /**
     * Fragmento para cómo llegar.
     */
    private InformacionFragment               infoFragment;

    /**
     * Fragmento para cómo llegar.
     */
    private ComoLlegarFragment                comoLlegarFragment;

    /**
     * Fragmento para cómo llegar.
     */
    private HomeFragment                      homeFragment;

    /**
     * Fragmento para el navegador web (consultas).
     */
    private ReservaWebViewFragment            reservaWebViewFragment;

    /**
     * Fragmento para la pantalla de login.
     */
    private UsuarioLoginFragment              loginFragment;

    /**
     * Un mapa de Stacks, donde usaremos el nombre del fragmento como clave.
     * <p/>
     * Por motivos de eficiencia, no definimos la variable como un interface:
     * <p/>
     * On devices without a JIT, it is true that invoking methods via a variable with an exact type rather than an
     * interface is slightly more efficient. (http://developer.android.com/training/articles/perf-tips.html)
     */
    private HashMap<Integer, Stack<Fragment>> mStacks;

    /**
     * Icono "opciones" del menú inferior.
     */
    private ToolbarImageView                  ivOpciones;

    /**
     * Icono "cómo llegar" del menú inferior.
     */
    private ToolbarImageView                  ivComoLlegar;

    /**
     * Visible sólo en el caso de las tabletas.
     */
    private ToolbarImageView                  ivInfo;

    /**
     * Icono "home" del menú inferior.
     */
    private ToolbarImageView                  ivHome;

    /**
     * Icono "pedir cita" del menú inferior.
     */
    private ToolbarImageView                  ivPedirCita;

    /**
     * Icono "login" del menú inferior.
     */
    private ToolbarImageView                  ivLogin;

    /**
     * Nombre del "tab" actual.
     */
    private int                               mCurrentTab;

    /**
     * 
     */
    private final ArrayList<Fragment>         fragmentsList                 = new ArrayList<Fragment>();

    /**
     * 
     */
    private final ArrayList<ToolbarImageView> listaIconosMenu               = new ArrayList<ToolbarImageView>();

    @Override
    public void onCreate(final Bundle savedInstanceState) {

        // Ignoramos el savedInstanceState...
        super.onCreate(null);

        // Ocultamos la barra de estado
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_main);

        // Preparamos el HashMap para gestionar las pilas de fragmentos
        prepareFragmentStacks();

        // Registramos la clase que estará a la escucha cuando el teléfono se reinicie
        registrarAppBootReceiver();

        // Localización
        prepareLocation();

        addTabBarFragmentsOne();

        // La botonera inferior varía según la orientación
        setUpToolbarCommonButtons();

        if (!CLCApp.isPortrait()) {
            setUpToolbarButtonsLandscape();
        }

        // Cargamos la lista con las imágenes de los iconos que habrá en la barra inferior
        cargarListaIconosMenu();

    }

    @Override
    protected void onStart() {

        super.onStart();

        mLocationClient.connect();

        addTabBarFragmentsTwo();

    }

    @Override
    public void onResume() {

        super.onResume();

        resaltarOpcionActiva();

        checkNuevasCitas();

        // checkTiposPrestaciones();

        // buscarPrestaciones();

    }

    @Override
    protected void onStop() {

        // Desconectamos el cliente
        mLocationClient.disconnect();

        super.onStop();

    }

    //
    // @Override
    // protected void onDestroy() {
    //
    // super.onDestroy();
    //
    // Log.i(TAG, "onDestroy");
    //
    // }

    /**
     * Utilizaremos una lista auxiliar para poder gestionar más fácilmente qué icono del menú está activo en cada
     * momento.
     */
    private void cargarListaIconosMenu() {

        listaIconosMenu.add(ivOpciones);
        listaIconosMenu.add(ivComoLlegar);
        // En tabletas, añadimos el botón info
        if (!CLCApp.isPortrait()) {
            listaIconosMenu.add(ivInfo);
        }
        listaIconosMenu.add(ivHome);
        listaIconosMenu.add(ivPedirCita);
        listaIconosMenu.add(ivLogin);
    }

    /**
     * Añade al mapa de fragmentos los fragmentos del menú principal, opciones y login. Se llama desde el onCreate.
     */
    private void prepareFragmentStacks() {

        /*
         * Stacks de navegación para cada opción de menú que puede tener navegación hacia fragmentos anteriores (y para
         * la opción seleccionada del menú principal).
         */
        mStacks = new HashMap<Integer, Stack<Fragment>>();
        mStacks.put(Integer.valueOf(TabConstants.MENU_PRINCIPAL), new Stack<Fragment>());
        mStacks.put(TabConstants.FRAG_1_OPCIONES, new Stack<Fragment>());
        mStacks.put(TabConstants.FRAG_5_LOGIN, new Stack<Fragment>());

    }

    /**
     *
     */
    private void prepareLocation() {

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                && !manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            locationEnabled = false;
            Toast.makeText(this, "Active los servicios de localización para tener un mejor resultado", Toast.LENGTH_SHORT).show();
        } else {
            locationEnabled = true;
        }

        // Aquí creamos el LocationClient
        mLocationClient = new LocationClient(this, this, this);
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

    }

    /**
     * Registramos para que la aplicación detecte cuándo se ha apagado el dispositivo y ha vuelto a iniciarse.
     */
    private void registrarAppBootReceiver() {

        final ComponentName receiver = new ComponentName(CLCApp.getAppContext(), AppBootReceiver.class);
        final PackageManager pm = CLCApp.getAppContext().getPackageManager();

        pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

    }

    /**
     * Comprueba si hay que buscar nuevas citas.
     */
    private void checkNuevasCitas() {

        // Obtenemos los datos almacenados en las preferencias
        PreferencesStore.restorePreferences();
        final long ultimaSincCitas = PreferencesStore.getFechaUltimaSincCitas();
        Log.i(TAG, "Tiempo desde la última actualización de las citas: "
                + (System.currentTimeMillis() - ultimaSincCitas));

        // Si hay sesión y ha pasado el tiempo de espera, solicitamos las citas
        if (CLCApp.getInicioSesion() != null) {

            if (System.currentTimeMillis() - ultimaSincCitas > (VariantConstants.CITAS_SINC_TIMEOUT)) {

                final ComprobarNuevasCitasWsdl2CodeEvents eventsCitas = new ComprobarNuevasCitasWsdl2CodeEvents(this);
                final AppCLC appCLC = new AppCLC(eventsCitas, VariantConstants.WSDL_URL_PROXIMAS_CITAS, AppCLC.TIMEOUT);
                try {
                    appCLC.recuperaProximasCitasAsync(CLCApp.getInicioSesion().getPersCorrel(), CLCApp
                            .getInicioSesion().getToken(), "");
                } catch (final Exception e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }

            } else {
                Log.i(TAG, "No hay que llamar nuevamente al servicio de citas (ejem)");
            }

        } else {
            Log.i(TAG, "No hay sesión, así que no se llama al servicio de citas");
        }

        UtilsCitas.borrarNotificacionesObsoletas();

    }

    /**
     * MOCK.
     */
    private void checkTiposPrestaciones() {

        final ComprobarTiposPrestacionesWsdl2CodeEvents eventsAranceles = new ComprobarTiposPrestacionesWsdl2CodeEvents(
                this);
        final AppCLCArancel appCLC = new AppCLCArancel(eventsAranceles, VariantConstants.WSDL_URL_TIPOS_PRESTACIONES,
                AppCLC.TIMEOUT);
        try {
            appCLC.tiposPrestacionesAsync();
        } catch (final Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

    }

    /**
     * MOCK.
     */
    private void buscarPrestaciones() {

        final BuscarPrestacionesWsdl2CodeEvents eventsAranceles = new BuscarPrestacionesWsdl2CodeEvents(this);
        final AppCLCArancel appCLC = new AppCLCArancel(eventsAranceles, VariantConstants.WSDL_URL_TIPOS_PRESTACIONES,
                AppCLC.TIMEOUT);
        try {
            appCLC.buscarPrestacionesAsync("190", "ANGIO");
        } catch (final Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

    }

    @Override
    public void onConnected(final Bundle dataBundle) {

        /*
         * De la documentación oficial: "Called by Location Services when the request to connect the client finishes
         * successfully. At this point, you can request the current location or start periodic updates"
         */
        final Location mCurrentLocation = mLocationClient.getLastLocation();
        if (mCurrentLocation == null) {
            if (locationEnabled) {
                mLocationClient.requestLocationUpdates(mLocationRequest, this);
            } else {
                Toast.makeText(this, "No está activada la localización", Toast.LENGTH_SHORT).show();
            }
        }
        CLCApp.setCurrentLocation(mCurrentLocation);
        Log.v(TAG, "google, onConnected!");
        CLCApp.setGoogleServicesOK(true);

    }

    @Override
    public void onDisconnected() {

        /*
         * Called by Location Services if the connection to the location client drops because of an error.
         */

        // Este método es obligatorio implementarlo debido a la interfaz ConnecionCallbacks, pero de momento no hacemos
        // nada

    }

    @Override
    public void onLocationChanged(final Location location) {

        Log.v(TAG, "onLocationChanged!");

        mLocationClient.removeLocationUpdates(this);

        // La posición del usuario, para calcular cuál es la parada más cercana
        CLCApp.setCurrentLocation(location);

    }

    /**
     * Añade los primeros fragmentos. Es llamado desde el onCreate.
     */
    private void addTabBarFragmentsOne() {

        // Log.i(TAG, "addTabBarFragmentsOne");

        // DDG 29-10-2014
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        homeFragment = new HomeFragment();
        opcFragment = new OpcionesFragment();
        comoLlegarFragment = new ComoLlegarFragment();

        ft.add(R.id.fragment_container, opcFragment);
        ft.add(R.id.fragment_container, comoLlegarFragment);
        ft.add(R.id.fragment_container, homeFragment);

        // Insertamos en la lista en el orden que tengan las opciones en el menú
        fragmentsList.add(opcFragment);
        fragmentsList.add(comoLlegarFragment);
        // Si se trata de una tableta, inicializamos el infoFragment
        if (!CLCApp.isPortrait()) {
            infoFragment = new InformacionFragment();
            ft.add(R.id.fragment_container, infoFragment);
            ft.hide(infoFragment);
        }
        fragmentsList.add(infoFragment);
        fragmentsList.add(homeFragment);

        ft.hide(opcFragment);
        ft.hide(comoLlegarFragment);

        ft.show(homeFragment);

        CLCApp.setCurrentFragment(homeFragment);
        CLCApp.setCurrentFragmentId(TabConstants.FRAG_3_HOME);

        // Y hacemos commit!
        ft.commit();

    }

    /**
     *
     */
    private void addTabBarFragmentsTwo() {

        if (reservaWebViewFragment == null) {
            // Log.i(TAG, "addTabs2: " + System.currentTimeMillis());

            reservaWebViewFragment = new ReservaWebViewFragment();
            final Bundle args1 = new Bundle();
            args1.putInt(Constants.CALLER, Constants.PEDIR_CITA);
            args1.putString(Constants.URL, Constants.URL_RESERVA_CITA);
            reservaWebViewFragment.setArguments(args1);

            loginFragment = new UsuarioLoginFragment();

            final FragmentManager fragmentManager = getSupportFragmentManager();
            final FragmentTransaction ft = fragmentManager.beginTransaction();

            ft.add(R.id.fragment_container, reservaWebViewFragment);
            ft.add(R.id.fragment_container, loginFragment);

            fragmentsList.add(reservaWebViewFragment);
            fragmentsList.add(loginFragment);

            ft.hide(reservaWebViewFragment);
            ft.hide(loginFragment);

            // Y hacemos commit!
            ft.commit();
        }

    }

    /**
     * Para ser llamado en el onResume y fijar en la "toolbar" el icono apropiado a la opción activa.
     */
    private void resaltarOpcionActiva() {

        switch (mCurrentTab) {
            case TabConstants.FRAG_1_OPCIONES:
                setActiveIcon(ivOpciones);
                break;
            case TabConstants.FRAG_2_COMO_LLEGAR:
                setActiveIcon(ivComoLlegar);
                break;
            case TabConstants.FRAG_3_INFO:
                setActiveIcon(ivInfo);
                break;
            case TabConstants.FRAG_4_WEB:
                setActiveIcon(ivPedirCita);
                break;
            case TabConstants.FRAG_5_LOGIN:
                setActiveIcon(ivLogin);
                break;
            case TabConstants.FRAG_3_HOME:
            default:
                setActiveIcon(ivHome);

        }

    }

    /**
     * To add fragment to a tab. tag -> Tab identifier fragment -> Fragment to show, in tab identified by tag
     * shouldAnimate -> should animate transaction. false when we switch tabs, or adding first fragment to a tab true
     * when when we are pushing more fragment into navigation stack. shouldAdd -> Should add to fragment navigation
     * stack (mStacks.get(tag)). false when we are switching tabs (except for the first time) true in all other cases.
     *
     * @param tag
     *            identificador del tab activo
     * @param fragment
     *            fragmento a mostrar
     * @param shouldAnimate
     *            ¿hay que hacer animación?
     * @param shouldAdd
     *            ¿hay que añadir el fragmento a la pila correspondiente?
     * @param fragId
     *            id del fragmento a mostrar
     */
    public void pushFragments(final int tag, final Fragment fragment, final boolean shouldAnimate,
            final boolean shouldAdd, final int fragId) {

        // Iniciamos la transacción...
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // getSupportFragmentManager().executePendingTransactions();

        // ¿Hay que hacer una animación de entrada/salida?
        if (shouldAnimate) {
            ft.setCustomAnimations(R.anim.right_in, R.anim.left_out);
        }

        // ¿Hay que añadir el fragmento a la pila de framentos del tab en cuestión?
        if (shouldAdd) {
            mStacks.get(tag).push(fragment);
            ft.add(R.id.fragment_container, fragment);
            ft.addToBackStack(null);
        }

        // Enseñamos el nuevo, ocultamos el antiguo y hacemos commit!
        ft.show(fragment);
        ft.hide((Fragment) CLCApp.getCurrentFragment());
        ft.commit();

        // Actualizamos variables de control
        CLCApp.setCurrentFragment((InterfaceCustomFragment) fragment);
        CLCApp.setCurrentFragmentId(fragId);

    }

    /**
     * Añade un fragmento a la pila de un determinado tab.
     * 
     * @param tag
     *            tag del fragmento a añadir.
     * @param fragment
     *            fragmento a añadir.
     */
    public void addFragmentToStack(final int tag, final Fragment fragment) {

        // Hay que añadir el fragmento a la pila de framentos del tab en cuestión
        mStacks.get(tag).push(fragment);

    }

    /**
     * Elimina un fragmento de una determinada pila y muestra el anterior.
     */
    private void popFragments() {

        // Obtenemos el penúltimo fragmento de la pila del tab actual, que será el que habrá que mostrar
        final Fragment fragment = mStacks.get(mCurrentTab).elementAt(mStacks.get(mCurrentTab).size() - 2);

        // Iniciamos la transacción...
        final FragmentManager manager = getSupportFragmentManager();
        final FragmentTransaction ft = manager.beginTransaction();

        // Animación...
        ft.setCustomAnimations(R.anim.left_in, R.anim.right_out);
        ft.remove(mStacks.get(mCurrentTab).lastElement());
        ft.show(fragment);

        CLCApp.setCurrentFragment((InterfaceCustomFragment) fragment);
        // Aquí no fijamos el id del fragmento actual, sino desde el propio fragmento

        // Sacamos de la pila el fragmento actual
        mStacks.get(mCurrentTab).pop();

        ft.commit();
    }

    /**
     * @param fragId
     *            id del fragmento que pasa a ser el actual.
     */
    public final void controlBackStack(final int fragId) {

        // Obtenemos el penúltimo fragmento de la pila del tab actual, que será el que habrá que mostrar
        final Fragment fragment = mStacks.get(mCurrentTab).elementAt(mStacks.get(mCurrentTab).size() - 2);

        // Log.i(TAG, "mStacks, pop " + mStacks.get(mCurrentTab).lastElement().getClass().getName());
        // Iniciamos la transacción...
        final FragmentManager manager = getSupportFragmentManager();
        final FragmentTransaction ft = manager.beginTransaction();

        // Animación...
        ft.setCustomAnimations(R.anim.left_in, R.anim.right_out);
        ft.remove(mStacks.get(mCurrentTab).lastElement());
        ft.show(fragment);

        ft.addToBackStack(null);

        CLCApp.setCurrentFragment((InterfaceCustomFragment) fragment);
        CLCApp.setCurrentFragmentId(fragId);

        // Sacamos de la pila el fragmento actual
        mStacks.get(mCurrentTab).pop();

        ft.commit();

    }

    @Override
    public final void onBackPressed() {

        if ((mStacks.get(mCurrentTab) == null) || (mStacks.get(mCurrentTab).isEmpty())) {

            Log.d(TAG, "finish");

            // El tab actual no tiene elementos en su pila, así que salimos de la app
            finish();

        } else {

            // Expresión simplificada de acuerdo a CheckStyle
            if (!((InterfaceCustomFragment) mStacks.get(mCurrentTab).lastElement()).onBackPressed()) {

                /*
                 * top fragment in current tab doesn't handles back press, we can do our thing, which is if current tab
                 * has only one fragment in stack, ie first fragment is showing for this tab. finish the activity else
                 * pop to previous fragment in stack for the same tab
                 */
                if (mStacks.get(mCurrentTab).size() == 1) {
                    Log.i(TAG, "super.onBackPressed -> finish");
                    finish();
                } else {
                    Log.i(TAG, "popFragments");
                    popFragments();
                }
            } else {
                // do nothing... fragment already handled back button press.
                Log.i(TAG, "do nothing...");
            }
        }
    }

    /**
     * Fija la imagen "on" del icono activo y fija a "off" el resto.
     * 
     * @param icon
     *            icono activo.
     */
    private void setActiveIcon(final ToolbarImageView icon) {

        for (ToolbarImageView item : listaIconosMenu) {
            item.deseleccionar();
        }
        icon.seleccionar();

    }

    // //////

    /**
     * Configura los "listeners" asociados a los botones del mapa.
     */
    private void setUpToolbarCommonButtons() {

        ivOpciones = (ToolbarImageView) findViewById(R.id.ivOpciones);
        ivOpciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                setOpcionesTabActions();
            }
        });

        ivComoLlegar = (ToolbarImageView) findViewById(R.id.ivComoLlegar);
        ivComoLlegar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                setComoLlegarTabActions();
            }
        });

        ivHome = (ToolbarImageView) findViewById(R.id.ivHome);
        ivHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                setHomeTabActions();
            }
        });

        ivPedirCita = (ToolbarImageView) findViewById(R.id.ivPedirCita);
        ivPedirCita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                setReservasTabActions();
            }
        });

        ivLogin = (ToolbarImageView) findViewById(R.id.ivUsuario);
        ivLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                setLoginTabActions();
            }
        });

    }

    /**
     * <p>
     * Configuramos los listener asociados a los botones del mapa.
     * </p>
     * <p>
     * En este caso, son seis los iconos a mostrar.
     * </p>
     */
    private void setUpToolbarButtonsLandscape() {

        ivInfo = (ToolbarImageView) findViewById(R.id.ivInfo);
        ivInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                setInfoTabActions();
            }
        });

    }

    /**
     * Configura las acciones para el tab "Opciones".
     */
    private void setOpcionesTabActions() {

        mCurrentTab = TabConstants.FRAG_1_OPCIONES;
        setActiveIcon(ivOpciones);

        if (mStacks.get(TabConstants.FRAG_1_OPCIONES).isEmpty()) {
            addFragmentToStack(TabConstants.FRAG_1_OPCIONES, opcFragment);
        }

        if ((CLCApp.getCurrentFragmentId() < TabConstants.FRAG_GENERAL_NIVEL_2)
                && (mStacks.get(TabConstants.FRAG_1_OPCIONES).size() == 1)) {
            // Venimos de una opción base del menú principal: la ocultamos y mostramos la nueva
            showPlainMenuOption(opcFragment, TabConstants.FRAG_1_OPCIONES);

        } else {
            pushFragments(TabConstants.FRAG_1_OPCIONES, mStacks.get(TabConstants.FRAG_1_OPCIONES).lastElement(), false,
                    false, TabConstants.FRAG_GENERAL_NIVEL_2);
        }

    }

    /**
     * Configura las acciones para el tab "Cómo llegar".
     */
    private void setComoLlegarTabActions() {

        try {
            mCurrentTab = TabConstants.FRAG_2_COMO_LLEGAR;
            setActiveIcon(ivComoLlegar);

            if (CLCApp.getCurrentFragmentId() < TabConstants.FRAG_GENERAL_NIVEL_2) {
                // Venimos de una opción base del menú principal: la ocultamos y mostramos la nueva
                showPlainMenuOption(comoLlegarFragment, TabConstants.FRAG_2_COMO_LLEGAR);
            } else {
                cambiazo(comoLlegarFragment, TabConstants.FRAG_2_COMO_LLEGAR);
            }
        } catch (Exception ex) {
            Log.e(TAG, Log.getStackTraceString(ex));
        }

    }

    /**
     * Configura las acciones para el tab "Info".
     */
    public final void setInfoTabActions() {

        try {
            mCurrentTab = TabConstants.FRAG_3_INFO;
            setActiveIcon(ivInfo);

            if (CLCApp.getCurrentFragmentId() < TabConstants.FRAG_GENERAL_NIVEL_2) {
                showPlainMenuOption(infoFragment, TabConstants.FRAG_3_INFO);
            } else {
                cambiazo(infoFragment, TabConstants.FRAG_3_INFO);
            }

        } catch (Exception ex) {
            Log.e(TAG, Log.getStackTraceString(ex));
        }

    }

    /**
     * Configura las acciones para el tab "Home".
     */
    private void setHomeTabActions() {

        try {
            mCurrentTab = TabConstants.FRAG_3_HOME;
            setActiveIcon(ivHome);

            if (CLCApp.getCurrentFragmentId() < TabConstants.FRAG_GENERAL_NIVEL_2) {
                showPlainMenuOption(homeFragment, TabConstants.FRAG_3_HOME);
            } else {
                cambiazo(homeFragment, TabConstants.FRAG_3_HOME);
            }

        } catch (Exception ex) {
            Log.e(TAG, Log.getStackTraceString(ex));
        }
    }

    /**
     * Configura las acciones para el tab "Reserva hora".
     */
    public final void setReservasTabActions() {

        try {
            mCurrentTab = TabConstants.FRAG_4_WEB;
            setActiveIcon(ivPedirCita);

            if (CLCApp.getCurrentFragmentId() < TabConstants.FRAG_GENERAL_NIVEL_2) {
                showPlainMenuOption(reservaWebViewFragment, TabConstants.FRAG_4_WEB);
            } else {
                cambiazo(reservaWebViewFragment, TabConstants.FRAG_4_WEB);
            }
            reservaWebViewFragment.checkAndLoad();

            CLCApp.setCurrentFragmentId(TabConstants.FRAG_4_WEB);

        } catch (Exception ex) {
            Log.e(TAG, Log.getStackTraceString(ex));
        }

    }

    /**
     * Configura las acciones para el tab "Login".
     */
    private void setLoginTabActions() {

        try {
            mCurrentTab = TabConstants.FRAG_5_LOGIN;
            setActiveIcon(ivLogin);

            if (mStacks.get(TabConstants.FRAG_5_LOGIN).isEmpty()) {
                addFragmentToStack(TabConstants.FRAG_5_LOGIN, loginFragment);
            }

            if (CLCApp.getCurrentFragmentId() < TabConstants.FRAG_GENERAL_NIVEL_2) {

                if (mStacks.get(TabConstants.FRAG_5_LOGIN).size() == 1) {
                    showPlainMenuOption(loginFragment, TabConstants.FRAG_5_LOGIN);
                } else {
                    pushFragments(TabConstants.FRAG_5_LOGIN, mStacks.get(TabConstants.FRAG_5_LOGIN).lastElement(),
                            false, false, TabConstants.FRAG_GENERAL_NIVEL_2);
                }

            } else {
                pushFragments(TabConstants.FRAG_5_LOGIN, mStacks.get(TabConstants.FRAG_5_LOGIN).lastElement(), false,
                        false, TabConstants.FRAG_GENERAL_NIVEL_2);
            }

        } catch (Exception ex) {
            Log.e(TAG, Log.getStackTraceString(ex));
        }

    }

    /**
     * Oculta el framento actual y muestra el que se le pasa como parámetro.
     *
     * @param fragment
     *            fragmento
     * @param fragId
     *            id del fragmento a mostrar
     */
    private void cambiazo(final CustomFragment fragment, final int fragId) {

        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.hide((Fragment) CLCApp.getCurrentFragment());
        ft.show(fragment);
        ft.commit();
        CLCApp.setCurrentFragment(fragment);
        CLCApp.setCurrentFragmentId(fragId);

    }

    /**
     * Método que oculta una opción del menú principal y muestra otra.
     *
     * @param fragment
     *            fragmento a mostrar
     * @param fragId
     *            id del fragmento a mostrar
     */
    private void showPlainMenuOption(final Fragment fragment, final int fragId) {

        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        ft.hide(fragmentsList.get(CLCApp.getCurrentFragmentId() - 1));
        ft.show(fragment);
        ft.commit();
        CLCApp.setCurrentFragment((InterfaceCustomFragment) fragment);
        CLCApp.setCurrentFragmentId(fragId);

    }

    @Override
    public final void onConnectionFailed(final ConnectionResult connectionResult) {

        // Called by Location Services if the attempt to Location Services fails.
        Log.i(TAG, "connectionResult: " + connectionResult.getErrorCode());

        /*
         * Google Play services can resolve some errors it detects. If the error has a resolution, try sending an Intent
         * to start a Google Play services activity that can resolve error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECT_FAILURE_RESOL_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original PendingIntent
                 */
            } catch (final IntentSender.SendIntentException e) {
                Log.e(TAG, Log.getStackTraceString(e));
            }
        } else {
            // Avisamos al usuario si no hay solución
            showGooglePlayErrorDialog(connectionResult.getErrorCode());
            Log.i(TAG, "google, onConnectionFailed!");
            CLCApp.setGoogleServicesOK(false);
        }

    }

    /**
     * Muestra una ventana de error avisando al usuario que debe instalar los servicios de Google Play.
     * 
     * @param resultCode
     *            código de error.
     */
    private void showGooglePlayErrorDialog(final int resultCode) {

        // Mensaje de aviso usando los servicios de Google Play, indicando al usuario que debe instalarlos
        try {
            final Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                    CONNECT_FAILURE_RESOL_REQUEST);
            // OJO: este errorDialog sólo será nulo si el resultCode es ConnectionResult.SUCCESS,
            // así que para las pruebas hay que forzar el cambio manualmente
            if (errorDialog != null) {
                errorDialog.show();
            }
        } catch (final NoClassDefFoundError ex) {
            Utils.showOkDialogWithText(this, getString(R.string.aviso), getString(R.string.google_play_services));
        }

    }

    /**
     * Clase que gestiona los eventos del servicio web de recuperación de las próximas citas.
     *
     * @author David Díaz
     * @version 1.0, 16/07/14
     */
    private class ComprobarNuevasCitasWsdl2CodeEvents implements IWsdl2CodeEvents {

        /**
         * Para trazas.
         */
        private static final String    TAG = "ComprobarNuevasCitasWsdl2CodeEvents";

        /**
         * Clase para modelar la respuesta del servicio web de próximas citas.
         */
        private ProximasCitas          citas;

        /**
         * Actividad llamante.
         */
        private final FragmentActivity mActivity;

        /**
         * Constructor.
         * 
         * @param activity
         *            actividad
         */
        ComprobarNuevasCitasWsdl2CodeEvents(FragmentActivity activity) {
            this.mActivity = activity;
        }

        /**
         *
         */
        public void wsdl2CodeStartedRequest() {

            Log.i(TAG, "wsdl2CodeStartedRequest");

        }

        /**
         * El servicio web ha finalizado correctamente.
         *
         * @param methodName
         *            nombre del método.
         * @param data
         *            datos.
         */
        public void wsdl2CodeFinished(final String methodName, final Object data) {

            this.citas = (ProximasCitas) data;

            if (citas.getListaCitas() != null) {
                PreferencesStore.setFechaUltimaSincCitas();
            }

            final GestorCitas gestor = new GestorCitas(mActivity, CLCApp.getInicioSesion().getPersCorrel());
            gestor.gestionarCitas(citas);

        }

        /**
         * Se ha producido una excepción en la invocación al servicio web.
         *
         * @param ex
         *            excepción producida.
         */
        public void wsdl2CodeFinishedWithException(final Exception ex) {

            Log.e(TAG, Log.getStackTraceString(ex));

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Utils.showOkDialogWithText(mActivity, getString(R.string.login),
                    // getString(R.string.error_general));
                    CLCApp.setInicioSesion(null);
                }
            });

        }

        /**
         *
         */
        public void wsdl2CodeEndedRequest() {

            Log.i(TAG, "wsdl2CodeEndedRequest");

        }

    }

    /**
     * Clase que gestiona los eventos del servicio web de recuperación de las próximas citas.
     *
     * @author David Díaz
     * @version 1.0, 16/07/14
     */
    private class ComprobarTiposPrestacionesWsdl2CodeEvents implements IWsdl2CodeEvents {

        /**
         * Para trazas.
         */
        private static final String TAG = "ComprobarTiposPrestacionesWsdl2CodeEvents";

        /**
         * Clase para modelar la respuesta del servicio web de próximas citas.
         */
        private TiposPrestaciones   prestaciones;

        /**
         * Actividad llamante.
         */
        private FragmentActivity    mActivity;

        /**
         * Constructor.
         *
         * @param activity
         *            actividad
         */
        ComprobarTiposPrestacionesWsdl2CodeEvents(FragmentActivity activity) {
            try {
                this.mActivity = activity;
            } catch (Exception ex) {
                Log.e(TAG, Log.getStackTraceString(ex));
            }
        }

        /**
         *
         */
        public void wsdl2CodeStartedRequest() {

            Log.i(TAG, "wsdl2CodeStartedRequest");
            // Mostramos la barra de progreso.
            // ProgressBar pbLogin = (ProgressBar) mActivity.findViewById(R.id.pbLogin);

        }

        /**
         * El servicio web ha finalizado correctamente.
         *
         * @param methodName
         *            nombre del método.
         * @param data
         *            datos.
         */
        public void wsdl2CodeFinished(final String methodName, final Object data) {

        }

        /**
         * Se ha producido una excepción en la invocación al servicio web.
         *
         * @param ex
         *            excepción producida.
         */
        public void wsdl2CodeFinishedWithException(final Exception ex) {

            Log.i(TAG, "wsdl2CodeFinishedWithException");
            Log.e(TAG, Log.getStackTraceString(ex));

        }

        /**
         *
         */
        public void wsdl2CodeEndedRequest() {

            Log.i(TAG, "wsdl2CodeEndedRequest");

        }

    }

    /**
     * Clase que gestiona los eventos del servicio web de recuperación de las próximas citas.
     *
     * @author David Díaz
     * @version 1.0, 16/07/14
     */
    private class BuscarPrestacionesWsdl2CodeEvents implements IWsdl2CodeEvents {

        /**
         * Para trazas.
         */
        private static final String TAG = "BuscarPrestacionesWsdl2CodeEvents";

        /**
         * Clase para modelar la respuesta del servicio web de próximas citas.
         */
        private TiposPrestaciones   prestaciones;

        /**
         * Actividad llamante.
         */
        private FragmentActivity    mActivity;

        /**
         * Constructor.
         *
         * @param activity
         *            actividad
         */
        BuscarPrestacionesWsdl2CodeEvents(FragmentActivity activity) {
            try {
                this.mActivity = activity;
            } catch (Exception ex) {
                Log.e(TAG, Log.getStackTraceString(ex));
            }
        }

        /**
         *
         */
        public void wsdl2CodeStartedRequest() {

            Log.i(TAG, "wsdl2CodeStartedRequest");
            // Mostramos la barra de progreso.
            // ProgressBar pbLogin = (ProgressBar) mActivity.findViewById(R.id.pbLogin);

        }

        /**
         * El servicio web ha finalizado correctamente.
         *
         * @param methodName
         *            nombre del método.
         * @param data
         *            datos.
         */
        public void wsdl2CodeFinished(final String methodName, final Object data) {

            Log.i(TAG, "wsdl2CodeFinished, method: " + methodName);

        }

        /**
         * Se ha producido una excepción en la invocación al servicio web.
         *
         * @param ex
         *            excepción producida.
         */
        public void wsdl2CodeFinishedWithException(final Exception ex) {

            Log.i(TAG, "wsdl2CodeFinishedWithException");
            Log.e(TAG, Log.getStackTraceString(ex));

        }

        /**
         *
         */
        public void wsdl2CodeEndedRequest() {

            Log.i(TAG, "wsdl2CodeEndedRequest");

        }

    }
}
