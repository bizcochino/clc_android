package es.ingenia.clc.app.ksoap.model;

import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

/**
 * Clase para modelar la información de inicio de sesión en CLC.
 *
 * @author David Díaz
 * @version 1.0, 15/07/2014
 */
public class InicioSesion implements KvmSerializable {

    /** Para trazas. */
    // private static final String TAG = "InicioSesion";

    /**
     * Indica si ha habido éxito en la conexión.
     */
    private boolean success;

    /**
     * Nombre.
     */
    private String  name;

    /**
     * pers_correl.
     */
    private String  persCorrel;

    /**
     * token.
     */
    private String  token;

    /**
     * mensaje_negocio.
     */
    private String  mensajeNegocio;

    // <?xml version="1.0" encoding="utf-8"?>
    // <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
    // xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    // <soap:Body>
    // <InicioSesionResponse xmlns="http://tempuri.org/">
    // <InicioSesionResult>
    // <login success="true" xmlns="">
    // <pers_correl>1208025</pers_correl>
    // <token>1314d988d50f36797aeca3c57bfb57f2</token>
    // <mensaje_negocio>AUTENTICADO - CON PRIVILEGIOS</mensaje_negocio>
    // </login>
    // </InicioSesionResult>
    // </InicioSesionResponse>
    // </soap:Body>
    // </soap:Envelope>

    /**
     * Constructor sin parámetros.
     */
    public InicioSesion() {
    }

    /**
     * Constructor.
     *
     * @param soapObject
     *            objeto soap.
     */
    public InicioSesion(final SoapObject soapObject) {

        if (soapObject == null) {
            return;
        }

        if (soapObject.hasProperty("login")) {
            final SoapObject obj = (SoapObject) soapObject.getProperty("login");
            success = Boolean.valueOf((String) obj.getAttribute("success"));

            if (success) {
                name = "Nombre inventado";

                loadPersCorrel(obj);

                loadToken(obj);

                loadMensajeNegocio(obj);

            } else {
                token = "tokenMalo";
            }
        }

    }

    /**
     * @param obj
     */
    private void loadPersCorrel(final SoapObject obj) {
        if (obj.hasProperty("pers_correl")) {
            final Object obj1 = obj.getProperty("pers_correl");
            if (obj1 != null && obj1.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj1;
                persCorrel = j.toString();
            } else if (obj1 != null && obj1.getClass().equals(SoapObject.class)) {
                persCorrel = (String) obj1;
            } else if (obj1 instanceof String) {
                persCorrel = (String) obj1;
            }
        }
    }

    /**
     * @param obj
     */
    private void loadToken(final SoapObject obj) {
        if (obj.hasProperty("token")) {
            final Object obj1 = obj.getProperty("token");
            if (obj1 != null && obj1.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj1;
                token = j.toString();
            } else if (obj1 instanceof String) {
                token = (String) obj1;
            }
        } else {
            token = "tokenMalo";
        }
    }

    /**
     * @param obj
     */
    private void loadMensajeNegocio(final SoapObject obj) {
        if (obj.hasProperty("mensaje_negocio")) {
            final Object obj1 = obj.getProperty("mensaje_negocio");
            if (obj1 != null && obj1.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj1;
                mensajeNegocio = j.toString();
            } else if (obj1 instanceof String) {
                mensajeNegocio = (String) obj1;
            }
        }
    }

    @Override
    public Object getProperty(int arg0) {

        switch (arg0) {
            case 0:
                return success;
            case 1:
                return name;
            case 2:
                return persCorrel;
            case 3:
                return token;
            case 4:
                return mensajeNegocio;
            default:
                return null;
        }

    }

    @Override
    public int getPropertyCount() {
        return 5;
    }

    @Override
    public void getPropertyInfo(final int index, @SuppressWarnings("rawtypes") Hashtable arg1, final PropertyInfo info) {

        switch (index) {
            case 0:
                info.type = PropertyInfo.BOOLEAN_CLASS;
                info.name = "success";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "name";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "pers_correl";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "token";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "mensaje_negocio";
                break;
            default:
                break;
        }
    }

    @Override
    public void setProperty(final int index, final Object value) {

        switch (index) {
            case 0:
                success = Boolean.valueOf(value.toString());
                break;
            case 1:
                name = value.toString();
                break;
            case 2:
                persCorrel = value.toString();
                break;
            case 3:
                token = value.toString();
                break;
            case 4:
                mensajeNegocio = value.toString();
                break;
            default:
                break;
        }

    }

    // Nuevos métodos.

    /**
     * Devuelve el campo "success".
     * 
     * @return
     */
    public boolean isSuccess() {

        return success;
    }

    /**
     * Devuelve el campo persCorrel.
     *
     * @return
     */
    public String getPersCorrel() {

        return persCorrel;

    }

    /**
     * Devuelve el token.
     *
     * @return
     */
    public String getToken() {

        return token;

    }
}
