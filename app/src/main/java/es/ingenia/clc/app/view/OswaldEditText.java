package es.ingenia.clc.app.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Extensión de la clase EditText, que incluye una tipografía personalizada.
 *
 * @author David Díaz
 * @version 1.0, 15/07/2014
 */
public final class OswaldEditText extends EditText {

    /**
     * Para trazas.
     */
    // private static final String TAG = "OswaldTextView";

    /**
     * Contexto de la aplicación
     */
    private final Context       context;

    /**
     *
     */
    private static final String FONT_NAME = "fonts/Oswald-Regular.otf";

    /**
     * Constructor.
     * 
     * @param theContext
     *            contexto de la aplicación.
     * @param attrs
     *            atributos.
     */
    public OswaldEditText(final Context theContext, final AttributeSet attrs) {
        super(theContext, attrs);
        this.context = theContext;
        init();
    }

    /**
     *
     */
    private void init() {
        Typeface font = Typeface.createFromAsset(context.getAssets(), FONT_NAME);
        setTypeface(font);
    }

}
