package es.ingenia.clc.app.fragments;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.TabConstants;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.UtilsCitas;
import es.ingenia.clc.app.VariantConstants;
import es.ingenia.clc.app.activities.UbicacionCitaActivity;
import es.ingenia.clc.app.ksoap.AppCLC;
import es.ingenia.clc.app.ksoap.IWsdl2CodeEvents;
import es.ingenia.clc.app.ksoap.model.Cita;
import es.ingenia.clc.app.ksoap.model.Familiar;
import es.ingenia.clc.app.ksoap.model.GrupoFamiliar;
import es.ingenia.clc.app.view.OswaldTextView;

/**
 * Clase para el fragmento que muestra el listado de próximas citas de la app CLC.
 *
 * @author David Díaz
 * @version 1.0, 25/06/2014
 */
public class ProximasCitasFragment extends CustomListFragment {

    /**
     * Para trazas.
     */
    private static final String TAG     = "ProximasCitasFragment";

    /**
     * Constante para seleccionar al usuario titular.
     */
    private static final String TITULAR = "Titular";

    /**
     *
     */
    private CitasArrayAdapter   ladapter;

    /**
     * 
     */
    private List<Cita>          listaCitas;

    @Override
    public void onCreate(Bundle savedInstance) {

        super.onCreate(savedInstance);

        // Añadimos el listener para detectar que se ha pulsado "atrás"
        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(getListener());

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_proximas_citas, container, false);

    }

    @Override
    public void onActivityCreated(final Bundle savedInstance) {

        super.onActivityCreated(savedInstance);

        // Fijamos el texto de la barra superior (OJO, getView, no getActivity)
        final OswaldTextView tvTitle = (OswaldTextView) getView().findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.proximas_citas);

        // Para evitar que se muestre momentáneamente el nombre del familiar anterior
        final OswaldTextView tvFamiliar = (OswaldTextView) getView().findViewById(R.id.tvFamiliar);
        tvFamiliar.setText("");

        // Programamos el botón atrás
        final ImageView ivLogoCLC = (ImageView) getView().findViewById(R.id.ivLogoCLC);
        if (ivLogoCLC != null) {
            ivLogoCLC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // // Log.i(TAG, "pulsamos botón atrás");
                    getMActivity().controlBackStack(TabConstants.FRAG_5_LOGIN);
                }
            });

        }

        // UtilsCitas.leerCitasAnteriores(CLCApp.getInicioSesion().getPersCorrel());

        // Google Analytics...
        Utils.notificaGoogleAnalytics(Constants.SCREEN_PROXIMAS_CITAS, Constants.SCREEN_PROXIMAS_CITAS,
                Constants.EVENT_LABEL_NA);

    }

    @Override
    public void onResume() {

        super.onResume();

        final RecuperarGrupoFamiliarCitasWsdl2CodeEvents events = new RecuperarGrupoFamiliarCitasWsdl2CodeEvents();
        final AppCLC appCLC = new AppCLC(events, VariantConstants.WSDL_URL_GRUPO_FAMILIAR, AppCLC.TIMEOUT);
        try {

            appCLC.recuperaGrupoFamiliarAsync(CLCApp.getInicioSesion().getPersCorrel(), CLCApp.getInicioSesion()
                    .getToken());
        } catch (final Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }

        listaCitas = prepararListaCitas();

    }

    /**
     * Aquí capturamos el cambio que se produce en la pila, y volvemos a llamar a onResume().
     *
     * @return
     */
    private FragmentManager.OnBackStackChangedListener getListener() {

        return new FragmentManager.OnBackStackChangedListener() {

            public void onBackStackChanged() {

                if (CLCApp.getCurrentFragmentId() == TabConstants.FRAG_5_1_PROXIMAS_CITAS) {

                    if (CLCApp.getFamiliarSeleccionado() != null && getView() != null) {
                        final OswaldTextView tvFamiliar = (OswaldTextView) getView().findViewById(R.id.tvFamiliar);
                        mostrarNombreFamiliar(tvFamiliar);
                        if (ladapter != null) {
                            refrescarLista();
                        } else {
                            inicializarLista();
                        }
                    }
                }
            }

            /**
             * 
             */
            private void inicializarLista() {
                final ListView listView = (ListView) getView().findViewById(android.R.id.list);

                ladapter = new CitasArrayAdapter(CLCApp.getAppContext(), filtrarListaCitas(listaCitas), CLCApp
                        .getFamiliarSeleccionado().getPersCorrel());
                listView.setAdapter(ladapter);
            }

            /**
             * 
             */
            private void refrescarLista() {
                ladapter.refill(filtrarListaCitas(listaCitas), CLCApp.getFamiliarSeleccionado().getPersCorrel());
            }
        };

    }

    /**
     * Genera una lista de citas en la que sólo están las del familiar seleccionado.
     *
     * @return
     */
    private List<Cita> prepararListaCitas() {

        final List<Cita> listaCitasNuevas = UtilsCitas.leerCitasAnteriores(CLCApp.getInicioSesion().getPersCorrel());
        // La lista de citas no visualizadas irá menguando conforme el usuario vaya viéndolas, por lo que es necesario
        // hacer un "merge" con la lista de citas nuevas, que sí será constante.
        // No basta con mostrar las nuevas, por la lista de no visualizadas incluye aquellas citas que se han borrado, y
        // que se deben mostrar al usuario bajo el estado "cancelada".
        final List<Cita> listaCitasNoVisualizadas = UtilsCitas.leerCitasNoVisualizadas(CLCApp.getInicioSesion()
                .getPersCorrel());

        final Set<Cita> lista = new LinkedHashSet<Cita>(listaCitasNoVisualizadas);

        for (final Cita aux : listaCitasNuevas) {
            lista.add(aux);
        }

        return new ArrayList<Cita>(lista);
    }

    /**
     * Genera una lista de citas en la que sólo están las del familiar seleccionado.
     * 
     * @param origen
     * @return
     */
    private List<Cita> filtrarListaCitas(final List<Cita> origen) {

        final List<Cita> listaFiltrada = new ArrayList<Cita>(origen);

        // Nos quedamos únicamente con aquellas citas asociadas al persCorrel actual.
        final Iterator<Cita> it = listaFiltrada.iterator();
        while (it.hasNext()) {
            final Cita cita = it.next();
            if (!cita.getPersCorrel().equals(CLCApp.getFamiliarSeleccionado().getPersCorrel())) {
                it.remove();
            }
        }

        return listaFiltrada;

    }

    /**
     * Muestra el listado de citas que corresponden al familiar seleccionado.
     * <p/>
     * Es llamado desde el método wsdl2CodeFinished de ProximasCitas.
     */
    void mostrarCitasPariente(final List<Familiar> lista) {

        final View view = getView();

        if (view != null) {

            final OswaldTextView tvFamiliar = (OswaldTextView) view.findViewById(R.id.tvFamiliar);
            // Elegimos al familiar activo
            for (final Familiar item : lista) {
                if (item.getParentesco() != null && item.getParentesco().equals(TITULAR)) {
                    // Log.i(TAG, "DEBUG - El familiar seleccionado será: " + item.getNombre());
                    CLCApp.setFamiliarSeleccionado(item);
                    break;
                }
            }
            mostrarNombreFamiliar(tvFamiliar);

            // Log.d(TAG, "familiar mostrado en campo: " + tvFamiliar.getText());
            if (tvFamiliar != null) {
                tvFamiliar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Log.i(TAG, "click en familiar!");
                        getMActivity().pushFragments(TabConstants.FRAG_5_LOGIN, new GrupoFamiliarFragment(), true,
                                true, TabConstants.FRAG_5_2_GRUPO_FAMILIAR);

                    }
                });
            }

            final ListView listView = (ListView) getView().findViewById(android.R.id.list);
            ladapter = new CitasArrayAdapter(CLCApp.getAppContext(), filtrarListaCitas(listaCitas), CLCApp
                    .getFamiliarSeleccionado().getPersCorrel());
            listView.setAdapter(ladapter);

        } else {
            Log.e(TAG, "View es null!!!");
        }

    }

    /**
     * Muestra en el campo de texto el nombre del familiar seleccionado.
     *
     * @param tvFamiliar
     *            campo de texto.
     */
    private void mostrarNombreFamiliar(final OswaldTextView tvFamiliar) {

        final Familiar fam = CLCApp.getFamiliarSeleccionado();

        if ((tvFamiliar != null) && (fam != null)) {
            // String nombre = fam.getNombre();
            // Log.i(TAG, "nombre familiar: " + nombre);
            tvFamiliar.setText(fam.getNombre());
        }

    }

    /**
     * Clase interna para el adaptador de la lista de citas.
     *
     * @author David Díaz
     * @version 1.0, 27/06/2014
     */
    private class CitasArrayAdapter extends ArrayAdapter<Cita> {

        /**
         * lista de citas.
         */
        private final List<Cita> listaCitas;

        /**
         * Lista de citas no visualizadas por el usuario.
         */
        private final List<Cita> listaCitasNoVisualizadas;

        /**
         * Pers Correl.
         */
        private final String     persCorrel;

        /**
         * Constructor.
         *
         * @param theContext
         *            contexto de la aplicación.
         * @param items
         *            listado de elementos a mostrar.
         * @param thePersCorrel
         *            de la persona seleccionada.
         */
        CitasArrayAdapter(final Context theContext, final List<Cita> items, final String thePersCorrel) {

            super(theContext, R.layout.cita_item, items);

            this.persCorrel = thePersCorrel;

            this.listaCitas = items;

            this.listaCitasNoVisualizadas = UtilsCitas.leerCitasNoVisualizadas(persCorrel);

        }

        /**
         * Para el relleno...
         *
         * @param aux
         *            lista con la que hacer el "rellenado".
         * @param thePersCorrel
         *            persCorrel.
         */
        public void refill(final List<Cita> aux, final String thePersCorrel) {

            listaCitas.clear();
            for (final Cita item : aux) {
                if (item.getPersCorrel().equals(thePersCorrel)) {
                    listaCitas.add(item);
                }
            }
            notifyDataSetChanged();

        }

        @Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {

            View view = convertView;

            if (view == null) {
                final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.cita_item, null);
                final ViewHolder viewHolder = new ViewHolder();
                viewHolder.aparcamiento = (ImageView) view.findViewById(R.id.ivAparcamiento);
                viewHolder.fechaHora = (OswaldTextView) view.findViewById(R.id.tvFechaHora);
                viewHolder.tipoConsulta = (OswaldTextView) view.findViewById(R.id.tvTipoConsulta);
                viewHolder.nombreMedico = (OswaldTextView) view.findViewById(R.id.tvNombreMedico);
                viewHolder.ubicacion = (OswaldTextView) view.findViewById(R.id.tvUbicacion);
                viewHolder.estacionamiento = (OswaldTextView) view.findViewById(R.id.tvEstacionamiento);
                viewHolder.estado = (OswaldTextView) view.findViewById(R.id.tvEstado);
                view.setTag(viewHolder);
            }

            final Cita cita = listaCitas.get(position);

            if (cita != null) {

                // Rellenamos los datos
                final ViewHolder holder = (ViewHolder) view.getTag();

                // if (holder == null) {
                // Log.e(TAG, "holder es nulo!");
                // }
                // if (holder.aparcamiento == null) {
                // Log.e(TAG, "holder.aparcamiento es nulo!");
                // }

                holder.fechaHora.setText((String) cita.getProperty(3));
                holder.tipoConsulta.setText((String) cita.getProperty(8));
                holder.nombreMedico.setText((String) cita.getProperty(2));
                holder.ubicacion.setText((String) cita.getProperty(4));
                holder.estacionamiento.setText((String) cita.getProperty(5));
                holder.estado.setText(selectEstado(cita.getEstado()));
                Log.i(TAG, "Estado de la cita " + cita.getCamId() + ": #" + cita.getEstado() + "#");
                if ((cita.getEstado() != null) && (cita.getEstado().equals(Constants.CITA_BORRADA))) {
                    Log.i(TAG, "La cita " + cita.getCamId() + " tiene como estado \"cancelada\".");
                }
                holder.aparcamiento.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View view) {
                        final Intent intent = new Intent(getActivity(), UbicacionCitaActivity.class);
                        intent.putExtra(Constants.URL_MAPA, cita.getUrlMapa());
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.bottom_up, 0);
                    }
                });

                boolean hayCambio = false;
                // Si alguna de las citas que estamos mostrando estaba en la lista de citas sin visualizar, la
                // eliminamos
                for (final Iterator<Cita> it = listaCitasNoVisualizadas.iterator(); it.hasNext();) {
                    final Cita aux = it.next();
                    if (cita.equals(aux)) {
                        it.remove();
                        hayCambio = true;
                    }
                }
                // Si ha habido un cambio en la lista de citas sin visualizar, actualizamos el fichero correspondiente
                if (hayCambio) {
                    if (!UtilsCitas.guardarFicheroCitasNoVisualizadas(listaCitasNoVisualizadas, persCorrel)) {
                        Log.e(TAG, "¡Error!");
                    }
                    // Log.d(TAG, "número de citas por visualizar: " + listaCitasNoVisualizadas.size());
                    UtilsCitas.setCirculoRojo(getActivity(), listaCitasNoVisualizadas.size());
                }

            }

            return view;

        }

        /**
         * Devuelve el literal de texto que corresponda al estado de la cita.
         *
         * @param estado
         *            estado (N/M/B)
         * @return
         */
        private String selectEstado(final String estado) {

            String txt = "";
            if (estado != null) {
                if (estado.equals(Constants.CITA_NUEVA)) {
                    txt = getString(R.string.nueva);
                } else if (estado.equals(Constants.CITA_MODIFICADA)) {
                    txt = getString(R.string.modificada);
                } else if (estado.equals(Constants.CITA_BORRADA)) {
                    txt = getString(R.string.borrada);
                }
            }

            return txt;
        }

        /**
         * Clase estática interna para aplicar el patrón "View Holder".
         *
         * @author David Díaz García
         * @version 1.0, 23/09/2014
         */
        class ViewHolder {

            /**
             * Imagen de la opción de menú.
             */
            private ImageView      aparcamiento;

            /**
             * Fecha/hora.
             */
            private OswaldTextView fechaHora;

            /**
             * Fecha/hora.
             */
            private OswaldTextView tipoConsulta;

            /**
             * Fecha/hora.
             */
            private OswaldTextView nombreMedico;

            /**
             * Fecha/hora.
             */
            private OswaldTextView ubicacion;

            /**
             * Fecha/hora.
             */
            private OswaldTextView estacionamiento;

            /**
             * Fecha/hora.
             */
            private OswaldTextView estado;

        }

    }

    /**
     * Clase interna que gestiona los eventos de la recuperación del grupo familiar.
     *
     * @author David Díaz
     * @version 1.0, 25/06/2014
     */
    private class RecuperarGrupoFamiliarCitasWsdl2CodeEvents implements IWsdl2CodeEvents {

        /**
         * Para trazas.
         */
        private static final String TAG = "RecuperarGrupoFamiliarCitasWsdl2CodeEvents";

        /**
         *
         */
        public void wsdl2CodeStartedRequest() {

            Log.i(TAG, "wsdl2CodeStartedRequest");

        }

        /**
         * La llamada al servicio web ha finalizado correctamente.
         *
         * @param methodName
         *            nombre del método.
         * @param data
         *            datos.
         */
        public void wsdl2CodeFinished(final String methodName, final Object data) {

            final boolean autenticado = ((GrupoFamiliar) data).isAutenticado();

            if (autenticado) {

                final List<Familiar> lista = ((GrupoFamiliar) data).getListaFamiliares();
                Log.i(TAG, "Tiene " + lista.size() + " grupos familiares");

                if (lista != null) {

                    mostrarCitasPariente(lista);
                }

            } else {
                // La sesión ha caducado
                Log.i(TAG, "La sesión ha caducado");

                CLCApp.setInicioSesion(null);

                // Informamos al usuario
                Utils.showOkDialogWithText(getActivity(), getString(R.string.sesion_caducada),
                        getString(R.string.sesion_caducada_info));

            }

        }

        /**
         * Se ha producido una excepción durante la llamada al servicio web.
         *
         * @param ex
         *            excepción producida.
         */
        public void wsdl2CodeFinishedWithException(final Exception ex) {

            Log.i(TAG, "wsdl2CodeFinishedWithException");
            Log.e(TAG, Log.getStackTraceString(ex));

        }

        /**
         *
         */
        public void wsdl2CodeEndedRequest() {

            Log.i(TAG, "wsdl2CodeEndedRequest");

        }

    }

}
