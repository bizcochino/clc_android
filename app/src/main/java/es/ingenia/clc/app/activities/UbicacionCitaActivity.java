package es.ingenia.clc.app.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.view.OswaldTextView;

/**
 * Pantalla con la ubicación de una cita.
 *
 * @author David Díaz
 * @version 1.0, 23/06/2014
 */
public class UbicacionCitaActivity extends BaseActivity {

    /**
     * Para trazas.
     */
    private static final String TAG = "UbicacionCitaActivity";

    /**
     * WebView.
     */
    private WebView             webview;

    /**
     * Barra de progreso.
     */
    private ProgressBar         mProgress;

    @Override
    public void onCreate(final Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // context = (CLCApp) getApplication();

        // Ocultamos la barra de estado
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_ubicacion_cita);

        // Fijamos el texto de la barra superior
        final OswaldTextView tvTitle = (OswaldTextView) findViewById(R.id.tvTitle);
        tvTitle.setText("");

        // Programamos el botón atrás
        final ImageView ivLogoCLC = (ImageView) findViewById(R.id.ivLogoCLC);
        if (ivLogoCLC != null) {
            ivLogoCLC.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    Log.i(TAG, "pulsado botón atrás");
                    finish();
                    overridePendingTransition(R.anim.bottom_up, 0);
                }
            });
        }

        // Configuramos el WebView...
        webview = (WebView) findViewById(R.id.webView);
        webview.setBackgroundResource(R.drawable.fondo_webview);
        webview.getSettings().setJavaScriptEnabled(true);

        // Hay que reescribir un par de cositas... (si no, se abriría el navegador por defecto).
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(final WebView view, final int progress) {
                setProgress(progress * 1000);
            }
        });
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(final WebView view, final String theURL) {
                // Ocultamos la barra de progreso cuando se haya terminado la carga
                if (mProgress.isShown()) {
                    mProgress.setVisibility(View.GONE);
                }
            }
        });

    }

    @Override
    public void onResume() {

        super.onResume();

        // Barra de progreso...
        mProgress = (ProgressBar) findViewById(R.id.pbWebview);
        mProgress.setVisibility(View.VISIBLE);

        final String url = getIntent().getStringExtra(Constants.URL_MAPA);
        webview.loadUrl(url);

        // Google Analytics...
        Utils.notificaGoogleAnalytics(Constants.SCREEN_PROXIMAS_CITAS,
            Constants.EVENT_ACTION_PARKING, Constants.EVENT_LABEL_NA);

    }

}
