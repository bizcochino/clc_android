package es.ingenia.clc.app.ksoap;

//------------------------------------------------------------------------------
// <wsdl2code-generated>
//    This code was generated by http://www.wsdl2code.com version  2.5
//
// Date Of Creation: 7/14/2014 10:19:26 AM
//    Please dont change this code, regeneration will override your changes
//</wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code  Version
//

/**
 *
 */
public interface IWsdl2CodeEvents {

    /**
     *
     */
    void wsdl2CodeStartedRequest();

    /**
     * @param methodName
     *            nombre del método.
     * @param Data
     *            datos devueltos.
     */
    void wsdl2CodeFinished(final String methodName, final Object Data);

    /**
     * @param ex
     *            excepción producida.
     */
    void wsdl2CodeFinishedWithException(final Exception ex);

    /**
     * 
     */
    void wsdl2CodeEndedRequest();
}
