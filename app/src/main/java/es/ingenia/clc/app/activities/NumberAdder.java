package es.ingenia.clc.app.activities;

/**
 * Created by David on 03/10/14.
 */
public final class NumberAdder {

    /**
     * Constructor privado.
     */
    private NumberAdder() {

    }

    /**
     * @param a
     * @param b
     * @return
     */
    public static int addNumbers(final int a, final int b) {

        return a + b;

    }

}
