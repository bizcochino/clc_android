package es.ingenia.clc.app.notifs;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.UtilsCitas;
import es.ingenia.clc.app.VariantConstants;
import es.ingenia.clc.app.activities.MainActivity;

/**
 * Esta clase se encarga de generar la notificación cuando se recibe la alarma correspondiente.
 * 
 * @author David Díaz
 * @version 1.0, 16/09/2014
 */
public class NotificationTimeAlarm extends BroadcastReceiver {

    /**
     * Para trazas.
     */
    private static final String TAG = "NotificationTimeAlarm";

    @Override
    public void onReceive(final Context context, final Intent paramIntent) {

        Log.i(TAG, "¡Alarma recibida!");

        final Bundle extras = paramIntent.getExtras();
        if (extras == null) {
            return;
        }

        // Este es el id de la notificación asociado a la cita. Si ya existía, el efecto es que se modificará, por lo
        // que este mismo método nos sirve para crear nuevas notificaciones y alterar las ya existentes.
        final int idNotif = extras.getInt(Constants.ID_NOTIFICACION);

        // Texto de la notificación a enviar
        final String msg = extras.getString(Constants.TEXTO_NOTIFICACION);

        final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                .setContentTitle(context.getString(R.string.clinica_las_condes)).setContentText(msg)
                .setSmallIcon(R.drawable.ic_notif);

        final NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle(mBuilder);
        bigTextStyle.setBigContentTitle(context.getString(R.string.clinica_las_condes));
        bigTextStyle.bigText(msg);

        mBuilder.setStyle(bigTextStyle);

        // Asignamos el intent que se abrirá al pulsar sobre la notificación
        // OJO: No confundirlo con este propio Intent, que es el encargado de recibir la alarma
        final Intent intent = new Intent(context.getApplicationContext(), MainActivity.class);
        intent.putExtra(Constants.ID_NOTIFICACION, idNotif);

        final PendingIntent pIntent = PendingIntent.getActivity(context.getApplicationContext(), 0, intent, 0);
        mBuilder.setContentIntent(pIntent);

        final Notification noti = mBuilder.build();
        noti.priority = Notification.PRIORITY_DEFAULT;
        noti.defaults |= Notification.DEFAULT_ALL;
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        final NotificationManager notificationManager = (NotificationManager) context
                .getSystemService(context.NOTIFICATION_SERVICE);

        notificationManager.notify(idNotif, noti);

        Log.i(TAG, "Listando notificaciones pendientes...");
        if (VariantConstants.DEBUG) {
            UtilsCitas.listarMapaCitasPorNotificar("Notif1");
        }
        Log.i(TAG, "Borramos de la lista de pendientes la notificación: #" + msg + "#");
        // Hay que buscar la notificación en el fichero y borrarla
        UtilsCitas.borrarNotificacionPendiente(idNotif);

        Log.i(TAG, "Listamos de nuevo las notificaciones pendientes...");
        if (VariantConstants.DEBUG) {
            UtilsCitas.listarMapaCitasPorNotificar("Notif2");
        }
    }
}
