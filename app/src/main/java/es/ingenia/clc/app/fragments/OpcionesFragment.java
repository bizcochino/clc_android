package es.ingenia.clc.app.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.TabConstants;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.fragments.interfaces.InterfaceMainMenuFragment;
import es.ingenia.clc.app.fragments.webviews.ArancelesWebViewFragment;
import es.ingenia.clc.app.fragments.webviews.RevistaWebViewFragment;
import es.ingenia.clc.app.view.OswaldTextView;

/**
 * Clase para el fragmento "Opciones" de la app CLC.
 *
 * @author David Díaz
 * @version 1.0, 23/06/2014
 */
public class OpcionesFragment extends CustomFragment implements InterfaceMainMenuFragment {

    /**
     * Para trazas.
     */
    // private static final String TAG = "OpcionesFragment";

    /**
     *
     */
    private final InformacionFragment   infoFragment = new InformacionFragment();

    /**
     *
     */
    private final ConfiguracionFragment confFragment = new ConfiguracionFragment();

    /*
     * Culturilla: el ciclo de vida de los fragmentos es [onAttach - onCreate - onCreateView - onActivityCreated -
     * onStart - onResume - [ACTIVO] - onPause - onStop - onDestroyView - onDestroy - onDettach]
     */

    @Override
    public void onAttach(final Activity activity) {

        super.onAttach(activity);

        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(getListener());

    }

    @Override
    public void onCreate(final Bundle savedInstance) {

        super.onCreate(savedInstance);

        getActivity().getSupportFragmentManager().addOnBackStackChangedListener(getListener());

    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_opciones, container, false);

    }

    @Override
    public void onActivityCreated(final Bundle savedInstance) {

        super.onActivityCreated(savedInstance);

        // Fijamos el texto de la barra superior
        final OswaldTextView tvTitle = (OswaldTextView) getView().findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.opciones);

        // Configuración de los botones de llamada
        setUpGridButtons();

        // Google Analytics... dejamos constancia de la vista de la página
        Utils.notificaGoogleAnalytics(Constants.SCREEN_OPCIONES, Constants.SCREEN_OPCIONES, Constants.EVENT_LABEL_NA);

    }

    /*
     * @Override public void onStart() { super.onStart(); }
     * @Override public void onResume() { super.onResume(); }
     * @Override public void onPause() { super.onPause(); }
     * @Override public void onStop() { super.onStop(); }
     */

    /**
     * Aquí capturamos el cambio que se produce en la pila, y volvemos a llamar a onResume().
     * 
     * @return
     */
    private FragmentManager.OnBackStackChangedListener getListener() {

        return new FragmentManager.OnBackStackChangedListener() {

            public void onBackStackChanged() {

            }
        };

    }

    /**
     * Configura las opciones del menú. Es llamado desde el onActivityCreated.
     */
    private void setUpGridButtons() {

        final GridView gridView = (GridView) getView().findViewById(R.id.main_grid_view);

        gridView.setAdapter(new ItemsMenuOpcionesAdapter(CLCApp.getAppContext()));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
                onItemClickGridView(position);
            }
        });

    }

    /**
     * @param position
     */
    private void onItemClickGridView(final int position) {
        switch (position) {
            case 0:
                // Si es tableta, desactivamos el icono "opciones" para activar "información"
                if (!CLCApp.isHasPhone()) {

                    getMActivity().setInfoTabActions();

                } else {

                    getMActivity().pushFragments(TabConstants.FRAG_1_OPCIONES, infoFragment, true, true,
                            TabConstants.FRAG_GENERAL_NIVEL_2);

                }
                // Google Analytics...
                Utils.notificaGoogleAnalytics(Constants.SCREEN_OPCIONES, Constants.EVENT_ACTION_INFO,
                        Constants.EVENT_LABEL_NA);

                break;

            case 1:
                // Click en aranceles
                final ArancelesWebViewFragment arancelesFragment = new ArancelesWebViewFragment();

                // final Bundle args1 = new Bundle();
                // args1.putInt(Constants.CALLER, Constants.ARANCELES);
                // args1.putString(Constants.URL, Constants.URL_ARANCELES);
                // arancelesFragment.setArguments(args1);

                getMActivity().pushFragments(TabConstants.FRAG_1_OPCIONES, arancelesFragment, true, true,
                        TabConstants.FRAG_GENERAL_NIVEL_2);

                // Google Analytics...
                Utils.notificaGoogleAnalytics(Constants.SCREEN_OPCIONES, Constants.EVENT_ACTION_ARANCELES,
                        Constants.EVENT_LABEL_NA);

                break;

            case 2:
                // Click en revista (Vivir mejor)
                final RevistaWebViewFragment revistaFragment = new RevistaWebViewFragment();

                // final Bundle args2 = new Bundle();
                // args2.putInt(Constants.CALLER, Constants.REVISTA);
                // args2.putString(Constants.URL, Constants.URL_REVISTA);
                // revistaFragment.setArguments(args2);

                getMActivity().pushFragments(TabConstants.FRAG_1_OPCIONES, revistaFragment, true, true,
                        TabConstants.FRAG_GENERAL_NIVEL_2);

                // Google Analytics...
                Utils.notificaGoogleAnalytics(Constants.SCREEN_OPCIONES, Constants.EVENT_ACTION_REVISTA,
                        Constants.EVENT_LABEL_NA);

                break;

            case 3:
                // Click en configuración (si es teléfono) / cita previa (si es tableta)
                if (CLCApp.isHasPhone()) {
                    // Configuración (en los teléfonos)

                    getMActivity().pushFragments(TabConstants.FRAG_1_OPCIONES, confFragment, true, true,
                            TabConstants.FRAG_1_1_CONFIGURACION);

                    // Google Analytics...
                    Utils.notificaGoogleAnalytics(Constants.SCREEN_OPCIONES, Constants.EVENT_ACTION_CONFIG,
                            Constants.EVENT_LABEL_NA);

                } else {
                    // Reserva de hora (en los tablets)
                    getMActivity().setReservasTabActions();

                    // Google Analytics...
                    Utils.notificaGoogleAnalytics(Constants.SCREEN_OPCIONES, Constants.EVENT_ACTION_RESERVA_HORA,
                            Constants.EVENT_LABEL_NA);

                }
                break;

            default:
                break;
        }
    }

    /**
     * Adaptador para los elementos del menú de opciones.
     * 
     * @author David Díaz García
     * @version 1.0, 23/06/2014
     */
    private static class ItemsMenuOpcionesAdapter extends BaseAdapter {

        /**
         * Para trazas.
         */
        private static final String  TAG                                = "ItemsMenuOpcionesAdapter";

        /**
         * Listado de elementos del menú principal.
         */
        private final List<MenuItem> items                              = new ArrayList<MenuItem>();

        /**
         * Inflater.
         */
        private final LayoutInflater inflater;

        /**
         * Recursos gráficos del menú de opciones (incluyen cambio al pulsarse).
         */
        private static final int[]   ICONOS_MENU_PRINCIPAL_CON_TELEFONO = { R.drawable.selector_opc_info,
                                                                                R.drawable.selector_opc_aranceles,
                                                                                R.drawable.selector_opc_revista,
                                                                                R.drawable.selector_opc_configuracion };

        /**
         * Recursos gráficos del menú de opciones cuando el dispositivo no tiene teléfono.
         */
        private static final int[]   ICONOS_MENU_PRINCIPAL_SIN_TELEFONO = { R.drawable.selector_opc_info,
                                                                                R.drawable.selector_opc_aranceles,
                                                                                R.drawable.selector_opc_revista,
                                                                                R.drawable.selector_opc_reserva_hora };

        /**
         * Constructor. Es llamado desde el método setUpGridButtons.
         * 
         * @param theContext
         *            contexto de la aplicación
         */
        public ItemsMenuOpcionesAdapter(final Context theContext) {

            inflater = LayoutInflater.from(theContext);
            final String[] itemsMenu;

            // Los iconos serán diferentes si el dispositivo no tiene teléfono
            final int[] iconos;
            if (CLCApp.isHasPhone()) {
                Log.d(TAG, "iconos CON teléfono");
                iconos = ICONOS_MENU_PRINCIPAL_CON_TELEFONO;
                itemsMenu = CLCApp.getAppContext().getResources().getStringArray(R.array.opciones_menu_con_telefono);
            } else {
                Log.d(TAG, "iconos SIN teléfono");
                iconos = ICONOS_MENU_PRINCIPAL_SIN_TELEFONO;
                itemsMenu = CLCApp.getAppContext().getResources().getStringArray(R.array.opciones_menu_sin_telefono);
            }

            // Añadimos los elementos del menú
            for (int i = 0; i < iconos.length; i++) {
                items.add(new MenuItem(itemsMenu[i], iconos[i]));
            }

        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(final int index) {
            return items.get(index);
        }

        @Override
        public long getItemId(final int index) {
            return items.get(index).getDrawableId();
        }

        @Override
        public View getView(final int index, final View convertView, final ViewGroup viewGroup) {

            View view = convertView;

            // Reusamos las vistas
            if (view == null) {
                view = inflater.inflate(R.layout.button_item_opciones, viewGroup, false);
                final ViewHolder viewHolder = new ViewHolder();
                viewHolder.picture = (ImageView) view.findViewById(R.id.ivPicture);
                viewHolder.name = (OswaldTextView) view.findViewById(R.id.tvOpcion);
                view.setTag(viewHolder);
            }

            // Rellenamos los datos
            final ViewHolder holder = (ViewHolder) view.getTag();
            final MenuItem item = (MenuItem) getItem(index);
            holder.picture.setImageResource(item.getDrawableId());
            holder.name.setText(item.getName());

            return view;

        }

        /**
         * Clase estática interna para aplicar el patrón "View Holder".
         *
         * @author David Díaz García
         * @version 1.0, 23/09/2014
         */
        static class ViewHolder {

            /**
             * Imagen de la opción de menú.
             */
            private ImageView      picture;

            /**
             * Texto de la opción del menú.
             */
            private OswaldTextView name;

        }

        /**
         * Clase para configurar un elemento del menú.
         * 
         * @author David Díaz
         * @version 1.0, 23/06/2014
         */
        private class MenuItem {

            /**
             * texto del botón.
             */
            private final String name;

            /**
             * icono.
             */
            private final int    drawableId;

            /**
             * Constructor.
             *
             * @param theName
             *            nombre.
             * @param theDrawableId
             *            identificador de la imagen.
             */
            MenuItem(final String theName, final int theDrawableId) {
                this.name = theName;
                this.drawableId = theDrawableId;
            }

            /**
             * Devuelve el nombre del item de menú.
             *
             * @return the name
             */
            String getName() {
                return name;
            }

            /**
             * Devuelve el id del icono asociado al item.
             *
             * @return the drawableId
             */
            int getDrawableId() {
                return drawableId;
            }

        }

    }

}
