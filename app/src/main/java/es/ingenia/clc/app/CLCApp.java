package es.ingenia.clc.app;

import java.util.HashMap;
import java.util.Locale;

import android.app.Application;
import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

import es.ingenia.clc.app.fragments.interfaces.InterfaceCustomFragment;
import es.ingenia.clc.app.ksoap.model.Familiar;
import es.ingenia.clc.app.ksoap.model.GrupoFamiliar;
import es.ingenia.clc.app.ksoap.model.InicioSesion;

/**
 * Created by David Díaz on 23/06/14.
 */
public final class CLCApp extends Application {

    /**
     * Para trazas.
     */
    private static final String            TAG             = "CLCApp";

    /**
     * Contexto de la aplicación.
     */
    private static Context                 context;

    /**
     * Flag que nos indica si el dispositivo tiene teléfono o no.
     */
    private static boolean                 hasPhone        = true;

    /**
     * Locale de la aplicación. Será el que tenga el móvil por defecto.
     */
    private static Locale                  locale;

    /**
     * Localización del usuario.
     */
    private static Location                currentLocation = null;

    /**
     * Teléfono de reservas.
     */
    private static final String            tlfReservas     = Constants.TEL_RESERVAS;

    /**
     * Teléfono de central.
     */
    private static final String            tlfCentral      = Constants.TEL_CENTRAL;

    /**
     * Teléfono de rescate.
     */
    private static final String            tlfRescate      = Constants.TEL_RESCATE;

    /**
     * Latitud de la posición del usuario.
     */
    private static Double                  miLatitud;

    /**
     * Longitud de la posición del usuario.
     */
    private static Double                  miLongitud;

    /**
     * Comprobación de que los servicios de Google están disponibles.
     */
    private static Boolean                 googleServicesOK;

    /**
     * Inicio de sesión.
     */
    private static InicioSesion            inicioSesion;

    /**
     * Grupo familiar.
     */
    private static GrupoFamiliar           grupoFamiliar;

    /**
     * Familiar seleccionado.
     */
    private static Familiar                familiarSeleccionado;

    /**
     * Aquí guardamos cuál es en cada momento el fragmento activo.
     */
    private static InterfaceCustomFragment currentFragment;

    /**
     * Aquí guardamos cuál es en cada momento el fragmento activo.
     */
    private static int                     currentFragmentId;

    /**
     * Tamaño de pantalla.
     */
    private static int                     screenSize;

    /**
     * Densidad de la pantalla.
     */
    private static int                     displayDensity;

    /**
     * anchura de la pantalla (para redimensionar los banners)
     */
    private static int                     screenWidth;

    /**
     * Fija cuál es el fragmento actual.
     * 
     * @param cf
     *            custom fragment.
     */
    public static void setCurrentFragment(final InterfaceCustomFragment cf) {
        currentFragment = cf;
    }

    /**
     * Fija cuál es el id del fragmento actual.
     *
     * @param id
     *            id.
     */
    public static void setCurrentFragmentId(final int id) {
        currentFragmentId = id;
    }

    /**
     * Devuelve el fragmento actual.
     * 
     * @return fragment.
     */
    public static InterfaceCustomFragment getCurrentFragment() {
        return currentFragment;
    }

    /**
     * Devuelve el fragmento actual.
     *
     * @return fragment.
     */
    public static int getCurrentFragmentId() {
        return currentFragmentId;
    }

    /** Tracker para Google Analytics. */
    public enum TrackerName {
        APP_TRACKER // Tracker used only in this app.
        // GLOBAL_TRACKER, // Tracker used by all the apps from a company. eg: roll-up tracking.
        // ECOMMERCE_TRACKER, // Tracker used by all ecommerce transactions from a company.
    }

    /** Mapa de trackers. Por eficiencia, usamos directamente la implementación en vez del interfaz */
    private static final HashMap<TrackerName, Tracker> mTrackers = new HashMap<TrackerName, Tracker>();

    /**
     * Flag para reconocer la orientación en la que trabajar con el dispositivo.
     */
    private static boolean                             portrait  = true;

    @Override
    public void onCreate() {
        super.onCreate();
        CLCApp.context = getApplicationContext();
        initializeLocale();

        // Para deshabilitar Google Analytics:
        // GoogleAnalytics.getInstance(this).setDryRun(true);

    }

    /**
     * @return
     */
    public static Context getAppContext() {
        return CLCApp.context;
    }

    /** TODO revisar. */
    private static final String PROPERTY_ID = "aaa";

    /**
     * Método que devuelve el Tracker de GA.
     *
     * @param trackerId
     *            identificador del tracker.
     * @return tracker.
     */
    public static synchronized Tracker getTracker(TrackerName trackerId) {

        if (!mTrackers.containsKey(trackerId)) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(context);

            if (VariantConstants.DEBUG) {
                analytics.setLocalDispatchPeriod(1);
            }

            /*
             * Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(PROPERTY_ID) : (trackerId ==
             * TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.app_tracker) :
             * analytics.newTracker(R.xml.ecommerce_tracker);
             */
            Tracker t = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(R.xml.app_tracker) : analytics
                    .newTracker(PROPERTY_ID);
            mTrackers.put(trackerId, t);

        }
        return mTrackers.get(trackerId);

    }

    /**
     * @return devuelve si el dispositivo tiene teléfono.
     */
    public static boolean isHasPhone() {
        return hasPhone;
    }

    /**
     * @param value
     *            boolean.
     */
    public static void setHasPhone(final boolean value) {
        CLCApp.hasPhone = value;
    }

    /**
     * @return the locale
     */
    public static Locale getLocale() {

        if (locale == null) {
            initializeLocale();
        }
        return locale;

    }

    /**
     *
     */
    private static void initializeLocale() {
        locale = Locale.getDefault();
    }

    /**
     * Devuelve el teléfono de reservas.
     * 
     * @return tlfReservas
     */
    public static String getTlfReservas() {
        return tlfReservas;
    }

    /**
     * Devuelve el teléfono de la central.
     * 
     * @return tlfCentral
     */
    public static String getTlfCentral() {
        return tlfCentral;
    }

    /**
     * Devuelve el teléfono de rescate.
     * 
     * @return tlfRescate
     */
    public static String getTlfRescate() {
        return tlfRescate;
    }

    /**
     * Devuelve la latitud.
     *
     * @return miLatitud
     */
    public static Double getMiLatitud() {
        return miLatitud;
    }

    /**
     * Devuelve la longitud.
     *
     * @return miLongitud
     */
    public static Double getMiLongitud() {
        return miLongitud;
    }

    /**
     * Devuelve la localización actual.
     *
     * @return the currentLocation
     */
    public Location getCurrentLocation() {
        return currentLocation;
    }

    /**
     * Llamado desde: 1) el método onConnected de la clase MainActivity. 2) el método onLocationChanged de la clase
     * GestorMapaLocalizador.
     * 
     * @param location
     *            the currentLocation to set
     */
    public static void setCurrentLocation(final Location location) {

        CLCApp.currentLocation = location;
        if (CLCApp.currentLocation != null) {
            // Toast.makeText(this, "miLatitud:" + location.getLatitude(), Toast.LENGTH_SHORT).show();
            CLCApp.miLatitud = location.getLatitude();
            CLCApp.miLongitud = location.getLongitude();
        }

    }

    /**
     * @param value
     *            the googleServicesOK to set
     */
    public static void setGoogleServicesOK(final boolean value) {
        CLCApp.googleServicesOK = value;
    }

    /**
     * Devuelve true si el dispositivo está en modo portrait.
     *
     * @return
     */
    public static boolean isPortrait() {
        return portrait;
    }

    /**
     * @param portrait
     *            booleano.
     */
    public static void setPortrait(final boolean portrait) {
        CLCApp.portrait = portrait;
    }

    /**
     * Devuelve el objeto InicioSesion.
     *
     * @return
     */
    public static InicioSesion getInicioSesion() {
        return inicioSesion;
    }

    /**
     * @param value
     *            inicio de sesión.
     */
    public static void setInicioSesion(final InicioSesion value) {

        inicioSesion = value;

    }

    /**
     * @return
     */
    public static GrupoFamiliar getGrupoFamiliar() {
        return grupoFamiliar;
    }

    /**
     * @param value
     *            grupo familiar.
     */
    public static void setGrupoFamiliar(final GrupoFamiliar value) {

        // List<Familiar> grupo = value.getListaFamiliares();

        grupoFamiliar = value;

    }

    /**
     * Devuelve el familiar seleccionado.
     * 
     * @return
     */
    public static Familiar getFamiliarSeleccionado() {

        if (familiarSeleccionado != null) {
            return familiarSeleccionado;
        } else {
            if (grupoFamiliar != null) {
                Log.d(TAG, "Devolvemos el familiar por defecto");
                return grupoFamiliar.getListaFamiliares().get(0);
            } else {
                // No hay familiar aún
                Log.d(TAG, "No hay familiar aún");
                return null;
            }
        }

    }

    /**
     * Fija cuál es el familiar seleccionado.
     *
     * @param value
     *            valor.
     */
    public static void setFamiliarSeleccionado(final Familiar value) {
        familiarSeleccionado = value;
    }

    /**
     * @return
     */
    public static int getDisplayDensity() {
        return displayDensity;
    }

    /**
     * @param value
     */
    public static void setDisplayDensity(final int value) {
        displayDensity = value;
    }

    /**
     * @return
     */
    public static int getScreenSize() {
        return screenSize;
    }

    /**
     * @param value
     */
    public static void setScreenSize(final int value) {
        screenSize = value;
    }

    /**
     * @return
     */
    public static int getScreenWidth() {
        if (screenWidth == 0) {
            screenWidth = Utils.detectScreenWidth();
        }
        return screenWidth;
    }

    /**
     * @param value
     *            anchura.
     */
    public static void setScreenWidth(final int value) {
        screenWidth = value;
    }

}
