package es.ingenia.clc.app.fragments.webviews;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import es.ingenia.clc.app.CLCApp;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.TabConstants;
import es.ingenia.clc.app.Utils;
import es.ingenia.clc.app.fragments.interfaces.InterfaceMainMenuFragment;

/**
 * Clase para el WebView de reservas.
 *
 * @author David Díaz
 * @version 1.0, 23/06/14
 */
public class ReservaWebViewFragment extends WebViewFragment implements InterfaceMainMenuFragment {

    /**
     * Para trazas.
     */
    private static final String TAG = "ReservaWebViewFragment";

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_reservas_webview, container, false);

    }

    @Override
    public void checkAndLoad() {

        if (CLCApp.getCurrentFragmentId() == TabConstants.FRAG_4_WEB) {

            final ProgressBar progressBar = (ProgressBar) getView().findViewById(R.id.pbWebview);

            // comprobar conexión a internet
            if (Utils.isOnline()) {
                progressBar.setVisibility(View.VISIBLE);
                invocarURL();
            } else {
                Utils.showOkDialogWithText(getActivity(), getString(R.string.aviso_no_internet),
                        getString(R.string.servicio_no_disponible));
                progressBar.setVisibility(View.GONE);
            }

        }

    }

    @Override
    public void invocarURL() {

        // Google Analytics...
        Utils.notificaGoogleAnalytics(Constants.SCREEN_RESERVA,
            Constants.SCREEN_RESERVA, Constants.EVENT_LABEL_NA);

        mWebview.loadUrl(Constants.URL_RESERVA_CITA);

    }

}
