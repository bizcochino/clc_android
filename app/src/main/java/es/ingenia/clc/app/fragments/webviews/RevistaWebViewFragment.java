package es.ingenia.clc.app.fragments.webviews;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import es.ingenia.clc.app.Constants;
import es.ingenia.clc.app.R;
import es.ingenia.clc.app.Utils;

/**
 * Clase para el fragmento que muestra un "WebView" en la app CLC.
 *
 * @author David Díaz
 * @version 1.0, 23/06/14
 */
public class RevistaWebViewFragment extends WebViewFragment {

    /**
     * Para trazas.
     */
    //private static final String TAG = "RevistaWebViewFragment";

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_revista_webview, container, false);

    }

    @Override
    public void setTitle() {

        final TextView tvTitle = (TextView) getView().findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.revista);

    }

    @Override
    public void invocarURL() {

        // Google Analytics...
        Utils.notificaGoogleAnalytics(Constants.SCREEN_REVISTA,
            Constants.SCREEN_REVISTA, Constants.EVENT_LABEL_NA);

        mWebview.loadUrl(Constants.URL_REVISTA);

    }

}
