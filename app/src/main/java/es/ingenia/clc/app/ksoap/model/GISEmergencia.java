package es.ingenia.clc.app.ksoap.model;

import java.util.Hashtable;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

/**
 * Clase para modelar la información una llamada de emergencia.
 *
 * @author David Díaz
 * @version 1.0, 05/08/14
 */
public class GISEmergencia implements KvmSerializable {

    /** Para trazas. */
    //private static final String TAG = "GISEmergencia";

    /**
     * mensaje_negocio.
     */
    private String              mensajeNegocio;

    // <?xml version="1.0" encoding="utf-8"?>
    // <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"
    // xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    // <soap:Body>
    // <GISEmergenciaResponse xmlns="http://tempuri.org/">
    // <GISEmergenciaResult>
    // <mensaje_negocio xmlns="">Ok</mensaje_negocio>
    // </GISEmergenciaResult>
    // </GISEmergenciaResponse>
    // </soap:Body>
    // </soap:Envelope>

    /**
     * Constructor sin parámetros.
     */
    public GISEmergencia() {
    }

    /**
     * Constructor.
     *
     * @param soapObject
     *            objeto soap.
     */
    public GISEmergencia(final SoapObject soapObject) {

        if (soapObject == null) {
            return;
        }

        if (soapObject.hasProperty("mensaje_negocio")) {
            final Object obj1 = soapObject.getProperty("mensaje_negocio");
            if (obj1 != null && obj1.getClass().equals(SoapPrimitive.class)) {
                final SoapPrimitive j = (SoapPrimitive) obj1;
                mensajeNegocio = j.toString();
            } else if (obj1 instanceof String) {
                mensajeNegocio = (String) obj1;
            }
        }

    }

    @Override
    public Object getProperty(final int arg0) {

        switch (arg0) {
            case 0:
                return mensajeNegocio;
            default:
                return null;
        }

    }

    @Override
    public int getPropertyCount() {
        return 1;
    }

    @Override
    public void getPropertyInfo(final int index, @SuppressWarnings("rawtypes") Hashtable arg1, final PropertyInfo info) {

        switch (index) {
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "mensaje_negocio";
                break;
            default:
                break;
        }
    }

    @Override
    public void setProperty(final int index, final Object value) {

        switch (index) {
            case 0:
                mensajeNegocio = value.toString();
                break;
            default:
                break;
        }

    }

    // Nuevos métodos.

    /**
     * Devuelve el token.
     *
     * @return
     */
    public String getMensajeNegocio() {

        return mensajeNegocio;

    }
}
