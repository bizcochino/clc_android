package es.ingenia.clc.app;

/**
 * Clase para guardar las constantes generales de la aplicación.
 * <p/>
 * Clase no instanciable.
 *
 * @author David Díaz
 * @version 1.0, 24/06/14
 */
public final class Constants {

    /**
     * Constructor privado.
     */
    private Constants() {

    }

    /**
     * Teléfono de reservas (por defecto).
     */
    public static final String TEL_RESERVAS                      = "+56226108000";

    /**
     * Teléfono de central (por defecto).
     */
    public static final String TEL_CENTRAL                       = "+56222104000";

    /**
     * Teléfono de rescate (por defecto).
     */
    public static final String TEL_RESCATE                       = "77777777";

    /**
     * Idioma
     */
    public static final String IDIOMA                            = "ES";

    /**
     * Identificador de Facebook.
     */
    public static final String FACEBOOK_ID                       = "fb://profile/142496349122748";

    /**
     * Página de Facebook.
     */
    public static final String FACEBOOK_USER                     = "https://www.facebook.com/clinicalascondes";

    /**
     * Para Twitter.
     */
    public static final String TWITTER_URI_1                     = "twitter://user?id=164810565";

    /**
     * Para Twitter.
     */
    public static final String TWITTER_URI_2                     = "https://twitter.com/#!/cliniclascondes";

    /**
     * Para Youtube (vídeo app).
     */
    public static final String YOUTUBE_URI_1                     = "http://www.youtube.com/watch?v=s9clUSvaU3Q";

    /**
     * Para Youtube (vídeo app).
     */
    public static final String YOUTUBE_URI_1_VID                 = "s9clUSvaU3Q";

    /**
     * Para Youtube (canal CLC).
     */
    public static final String YOUTUBE_URI_2                     = "http://www.youtube.com/user/clctelevision";

    /**
     * Botón desde el que se llama al WebViewFragment. Puede ser: PEDIR_CITA, ARANCELES o REVISTA
     */
    public static final String CALLER                            = "caller";

    /**
     * Para pedir cita...
     */
    public static final int    PEDIR_CITA                        = 1;

    /**
     * Opciones... Aranceles
     */
    public static final int    ARANCELES                         = 2;

    /**
     * Opciones... Revista
     */
    public static final int    REVISTA                           = 3;

    /**
     * URL para .
     */
    public static final String URL                               = "url";

    /**
     * URL para .
     */
    public static final String URL_RESERVA_CITA                  = "http://www.clc.cl/MOVIL/Hora/RESERVA.aspx";

    /**
     * URL para .
     */
    public static final String URL_ARANCELES                     = "http://www.miclc.cl/aplicaciones/arancel/ArancelCLC.aspx";

    /**
     * URL para .
     */
    public static final String URL_REVISTA                       = "http://www.clc.cl/revistavivirmas";

    // Localización Hospital Estoril.
    /**
     * Latitud Estoril.
     */
    public static final double LAT_HSPTL_ESTORIL                 = -33.38552;

    /**
     * Longitud Estoril.
     */
    public static final double LON_HSPTL_ESTORIL                 = -70.53187;

    // Localización Hospital Chicureo.
    /**
     * Latitud Chicureo.
     */
    public static final double LAT_HSPTL_CHICUREO                = -33.28349;

    /**
     * Longitud Chicureo.
     */
    public static final double LON_HSPTL_CHICUREO                = -70.65106;

    /**
     * Formato de hora para las llamadas de emergencia (24hh:mm).
     */
    public static final String FORMATO_HORA_LLAMADA_EMERGENCIA   = "HH:mm";

    // //////////////////////////////////
    // Constantes para Google Analytics
    // //////////////////////////////////

    // Categoría

    /**
     * Categoría servicios.
     */
    public static final String EVENT_CATEGORY_SERVICES           = "Servicios";

    // Pantallas

    /**
     * Pantalla de inicio.
     */
    public static final String SCREEN_INICIO                     = "Pantalla Inicio";

    /**
     * Pantalla de opciones.
     */
    public static final String SCREEN_OPCIONES                   = "Pantalla Opciones";

    /**
     * Pantalla de configuración.
     */
    public static final String SCREEN_CONFIGURACION              = "Pantalla Configuración";

    /**
     * Pantalla de cómo llegar.
     */
    public static final String SCREEN_COMO_LLEGAR                = "Pantalla Cómo llegar";

    /**
     * Pantalla de reserva.
     */
    public static final String SCREEN_RESERVA                    = "Pantalla Reserva";

    /**
     * Pantalla de Mi CLC.
     */
    public static final String SCREEN_MI_CLC                     = "Pantalla MiCLC";

    /**
     * Pantalla de aranceles.
     */
    public static final String SCREEN_ARANCELES                  = "Pantalla Aranceles";

    /**
     * Pantalla de revista.
     */
    public static final String SCREEN_REVISTA                    = "Pantalla Revista";

    /**
     * Pantalla de próximas citas.
     */
    public static final String SCREEN_PROXIMAS_CITAS             = "Pantalla Próximas citas";

    /**
     * Pantalla de selección de grupo familiar.
     */
    public static final String SCREEN_GRUPO_FAMILIAR             = "Pantalla Selección grupo familiar";

    /**
     * Pantalla de información.
     */
    public static final String SCREEN_INFORMACION                = "Pantalla Información CLC";

    // Eventos

    /**
     * Evento de llamada a reserva.
     */
    public static final String EVENT_ACTION_LLAMADA_RESERVAS     = "Llamada a reservas";

    /**
     * Evento de llamada a central.
     */
    public static final String EVENT_ACTION_LLAMADA_CENTRAL      = "Llamada a central";

    /**
     * Evento de llamada a rescate.
     */
    public static final String EVENT_ACTION_LLAMADA_RESCATE      = "Llamada a rescate";

    /**
     * Evento Facebook.
     */
    public static final String EVENT_ACTION_FACEBOOK             = "Facebook";

    /**
     * Evento Twitter.
     */
    public static final String EVENT_ACTION_TWITTER              = "Twitter";

    /**
     * Evento Youtube.
     */
    public static final String EVENT_ACTION_YOUTUBE              = "Youtube";

    /**
     * Evento información.
     */
    public static final String EVENT_ACTION_INFO                 = "Información";

    /**
     * Evento aranceles.
     */
    public static final String EVENT_ACTION_ARANCELES            = "Aranceles";

    /**
     * Evento revista.
     */
    public static final String EVENT_ACTION_REVISTA              = "Revista";

    /**
     * Evento reserva de hora.
     */
    public static final String EVENT_ACTION_RESERVA_HORA         = "Reserva hora";

    /**
     * Evento configuración.
     */
    public static final String EVENT_ACTION_CONFIG               = "Configuración";

    /**
     * Evento activar.
     */
    public static final String EVENT_ACTION_ACTIVAR              = "Activar";

    /**
     * Evento cómo llegar a.
     */
    public static final String EVENT_ACTION_COMO_LLEGAR_A        = "Cómo llegar a";

    /**
     * Evento ingresar.
     */
    public static final String EVENT_ACTION_INGRESAR             = "Ingresar";

    /**
     * Evento cancelar.
     */
    public static final String EVENT_ACTION_CANCELAR             = "Cancelar";

    /**
     * Evento cerrar sesión.
     */
    public static final String EVENT_ACTION_CERRAR_SESION        = "Cerrar sesión";

    /**
     * Evento próximas citas.
     */
    public static final String EVENT_ACTION_PROXIMAS_CITAS       = "Próximas citas";

    /**
     * Evento selección unidad familiar.
     */
    public static final String EVENT_ACTION_SELECCION_UNIDAD_FAM = "Selección Unidad familiar";

    /**
     * Evento parking.
     */
    public static final String EVENT_ACTION_PARKING              = "Parking";

    /**
     * URL del aparcamiento.
     */
    public static final String URL_MAPA                          = "URL_MAPA";

    /**
     * Para usar como "no aplica".
     */
    public static final String EVENT_LABEL_NA                    = "N/A";

    /**
     * Estoril.
     */
    public static final String EVENT_LABEL_ESTORIL               = "Estoril 450";

    /**
     * Chicureo
     */
    public static final String EVENT_LABEL_CHICUREO              = "Av. Chicureo";

    // /////////////////////
    // ///// CITAS /////
    // /////////////////////

    /**
     * Texto notificación.
     */
    public static final String TEXTO_NOTIFICACION                = "TextoNotificacion";

    /**
     * Patrón para el nombre del fichero de citas actuales (se añade el persCorrel al principio).
     */
    public static final String CITAS_ACTUALES_FICHERO            = "_citasActuales.json";

    /**
     * Patrón para el nombre del fichero de citas no visualizadas por el usuario (se añade el persCorrel al principio).
     */
    public static final String CITAS_NO_VISUALIZADAS_FICHERO     = "_citasNoVisualizadas.json";

    /**
     * Patrón para el nombre del fichero de citas pendientes de ser notificadas (se añade el persCorrel al principio).
     */
    public static final String CITAS_POR_NOTIFICAR_FICHERO       = "citasPorNotificar.json";

    /**
     * Constante de estado.
     */
    public static final String CITA_NUEVA                        = "N";

    /**
     * Constante de estado.
     */
    public static final String CITA_MODIFICADA                   = "M";

    /**
     * Constante de estado.
     */
    public static final String CITA_BORRADA                      = "B";

    /**
     * Días de preaviso en las notificaciones.
     */
    public static final int    DIAS_PREAVISO_CITA                = 2;

    /**
     * Pers correl.
     */
    public static final String PERS_CORREL                       = "persCorrel";

    /**
     * Identificador de la notificación.
     */
    public static final String ID_NOTIFICACION                   = "idNotificacion";

    /**
     * Configuración para teléfonos hdpi
     */
    public static final int    PHONE_HDPI                        = 0;

    /**
     * Configuración para teléfonos xhdpi
     */
    public static final int    PHONE_XHDPI                       = 1;

    /**
     * Configuración para tabletas mdpi
     */
    public static final int    TABLET_MDPI                       = 2;

    /**
     * Configuración para tabletas hdpi
     */
    public static final int    TABLET_HDPI                       = 3;

}
