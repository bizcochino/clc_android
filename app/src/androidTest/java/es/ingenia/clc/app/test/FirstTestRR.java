package es.ingenia.clc.app.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;

import com.robotium.solo.Solo;

import es.ingenia.clc.app.R;
import es.ingenia.clc.app.activities.SplashScreenActivity;

/**
 *
 */
public class FirstTestRR extends ActivityInstrumentationTestCase2<SplashScreenActivity> {

    /**
     * Para trazas.
     */
    private static final String TAG         = "FirstTestRR";

    /**
     *
     */
    private Solo                solo;

    /**
     *
     */
    private static final int    LONG_WAIT   = 1500;

    /**
     *
     */
    private static final int    MEDIUM_WAIT = LONG_WAIT / 2;

    /**
     *
     */
    private static final int    SHORT_WAIT  = LONG_WAIT / 4;

    /**
     *
     */
    public FirstTestRR() {
        super(SplashScreenActivity.class);
    }

    /**
     * @throws Exception
     */
    public void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation());
        getActivity();
    }

    public void allTests() {

        // Wait for activity: 'es.ingenia.clc.app.activities.SplashScreenActivity'
        solo.waitForActivity(SplashScreenActivity.class, 2000);
        // Wait for activity: 'es.ingenia.clc.app.activities.MainActivity'
        assertTrue("es.ingenia.clc.app.activities.MainActivity is not found!",
                solo.waitForActivity(es.ingenia.clc.app.activities.MainActivity.class));
        // Sleep for 7533 milliseconds
        solo.sleep(7533);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivOpciones));
        // Sleep for 4672 milliseconds
        solo.sleep(4672);
        // Click on Información
        solo.clickInList(1, 0);
        // Sleep for 5970 milliseconds
        solo.sleep(5970);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivLogoCLC));
        // Sleep for 1852 milliseconds
        solo.sleep(1852);
        // Click on Aranceles
        solo.clickInList(2, 0);
        // Sleep for 11249 milliseconds
        solo.sleep(11249);
        // Press menu back key
        solo.goBack();
        // Sleep for 1612 milliseconds
        solo.sleep(1612);
        // Click on Vivir mejor
        solo.clickInList(3, 0);
        // Sleep for 36986 milliseconds
        solo.sleep(36986);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivComoLlegar));
        // Sleep for 5991 milliseconds
        solo.sleep(5991);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivOpciones));
        // Sleep for 10000 milliseconds
        // solo.sleep(10000);
        // Click on SIGUIENTE
        // solo.clickOnWebElement(By.textContent("SIGUIENTE"));
        // Sleep for 2452 milliseconds
        solo.sleep(2452);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivLogoCLC));
        // Sleep for 2202 milliseconds
        solo.sleep(2202);
        // Click on Configuración
        solo.clickInList(4, 0);
        // Sleep for 2370 milliseconds
        solo.sleep(2370);
        // Click on Empty Text View
        solo.clickOnView(solo.getView(R.id.etTelefono));
        // Sleep for 9541 milliseconds
        solo.sleep(9541);
        // Enter the text: '654653208544147'
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.etTelefono));
        solo.enterText((android.widget.EditText) solo.getView(R.id.etTelefono), "654653208544147");
        // Sleep for 1407 milliseconds
        solo.sleep(1407);
        // Click on Activar
        solo.clickOnView(solo.getView(R.id.tvActivar));
        // Wait for dialog
        solo.waitForDialogToOpen(5000);
        // Sleep for 2585 milliseconds
        solo.sleep(2585);
        // Click on Aceptar
        solo.clickOnView(solo.getView(android.R.id.button1));
        // Sleep for 1500 milliseconds
        solo.sleep(1500);
        // Click on Desactivar
        solo.clickOnView(solo.getView(R.id.tvDesactivar));
        // Wait for dialog
        solo.waitForDialogToOpen(5000);
        // Sleep for 1626 milliseconds
        solo.sleep(1626);
        // Click on Aceptar
        solo.clickOnView(solo.getView(android.R.id.button1));
        // Sleep for 2012 milliseconds
        solo.sleep(2012);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivLogoCLC));
        // Sleep for 2663 milliseconds
        solo.sleep(2663);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivComoLlegar));
        // Sleep for 1946 milliseconds
        solo.sleep(1946);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep for 2514 milliseconds
        solo.sleep(2514);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivReservas));
        // Wait for dialog
        solo.waitForDialogToOpen(5000);
        // Sleep for 1801 milliseconds
        solo.sleep(1801);
        // Click on Cancelar
        solo.clickOnView(solo.getView(android.R.id.button2));
        // Wait for dialog to close
        solo.waitForDialogToClose(5000);
        // Sleep for 2460 milliseconds
        solo.sleep(2460);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivPedirCita));
        // Sleep for 11820 milliseconds
        solo.sleep(11820);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep for 3748 milliseconds
        solo.sleep(3748);

        // Click on ImageView
        // solo.clickOnView(solo.getView(R.id.ivFacebook));
        // //Sleep for 6435 milliseconds
        // solo.sleep(6435);

        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivComoLlegar));
        // Sleep for 1389 milliseconds
        solo.sleep(1389);
        // Click on ImageView
        // solo.clickOnView(solo.getView(R.id.ivUbicacionEstoril));
        // //Sleep for 6111 milliseconds
        // solo.sleep(6111);

        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivUsuario));
        // Sleep for 1597 milliseconds
        solo.sleep(1597);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep for 1012 milliseconds
        solo.sleep(1012);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivUsuario));
        // Sleep for 1368 milliseconds
        solo.sleep(1368);
        // Click on Ingresar
        solo.clickOnView(solo.getView(R.id.tvIngresar));
        // Wait for dialog
        solo.waitForDialogToOpen(5000);
        // Sleep for 1144 milliseconds
        solo.sleep(1144);
        // Click on Aceptar
        solo.clickOnView(solo.getView(android.R.id.button1));
        // Sleep for 844 milliseconds
        solo.sleep(844);
        // Click on Empty Text View
        solo.clickOnView(solo.getView(R.id.etRUT));
        // Sleep for 8631 milliseconds
        solo.sleep(8631);
        // Enter the text: '130688624'
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.etRUT));
        solo.enterText((android.widget.EditText) solo.getView(R.id.etRUT), "130688624");
        // Enter the text: '13068862-4'
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.etRUT));
        solo.enterText((android.widget.EditText) solo.getView(R.id.etRUT), "13068862-4");
        // Sleep for 902 milliseconds
        solo.sleep(902);
        // Click on Empty Text View
        solo.clickOnView(solo.getView(R.id.etClaveRUT));
        // Sleep for 7596 milliseconds
        solo.sleep(7596);
        // Enter the text: 'mar1976'
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.etClaveRUT));
        solo.enterText((android.widget.EditText) solo.getView(R.id.etClaveRUT), "mar1976");
        // Sleep for 869 milliseconds
        solo.sleep(869);
        // Click on Ingresar
        solo.clickOnView(solo.getView(R.id.tvIngresar));
        // Sleep for 68307 milliseconds
        solo.sleep(68307);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep for 1015 milliseconds
        solo.sleep(1015);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivUsuario));
        // Sleep for 118876 milliseconds
        solo.sleep(118876);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivPedirCita));
        // Sleep for 1589 milliseconds
        solo.sleep(1589);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep for 1610 milliseconds
        solo.sleep(1610);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivComoLlegar));
        // Sleep for 1249 milliseconds
        solo.sleep(1249);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivOpciones));
        // Sleep for 1840 milliseconds
        solo.sleep(1840);
        // Click on Configuración
        solo.clickInList(4, 0);
        // Sleep for 3982 milliseconds
        solo.sleep(3982);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep for 850 milliseconds
        solo.sleep(850);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivOpciones));
        // Sleep for 1677 milliseconds
        solo.sleep(1677);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivUsuario));
        // Sleep for 4670 milliseconds
        solo.sleep(4670);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
    }

    @SmallTest
    public void testGeneral() {

        arranque();

        for (int i = 0; i < 2; i++) {
            Log.i(TAG, "testGeneral, iteración " + i);

            opciones();
            informacion();
            aranceles();
            vivirMejor();
            usuario();
        }

        opciones();

        configuracion();

        comoLlegar();

        opciones();

        usuarioLogin();

        atras();

    }

    @SmallTest
    public void testBarridoMenu() {

        Log.i(TAG, "testBarridoMenu");

        arranque();

        comoLlegarHome();

        for (int i = 0; i < 10; i++) {
            opciones();
            comoLlegar();
            home();
            pedirCita();
            usuario();
        }

        atras();

    }

    /**
     *
     */
    private void atras() {

        solo.goBack();

    }

    /**
     *
     */
    private void arranque() {

        // Wait for activity: 'es.ingenia.clc.app.activities.SplashScreenActivity'
        solo.waitForActivity(SplashScreenActivity.class, 2000);
        // Wait for activity: 'es.ingenia.clc.app.activities.MainActivity'
//        assertTrue("es.ingenia.clc.app.activities.MainActivity is not found!",
//                solo.waitForActivity(es.ingenia.clc.app.activities.MainActivity.class));
        // Sleep...
        solo.sleep(LONG_WAIT);

    }

    /**
     *
     */
    private void opciones() {

        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivOpciones));
        // Sleep...
        solo.sleep(MEDIUM_WAIT);

    }

    /**
     *
     */
    private void informacion() {

        // Click on Información
        solo.clickInList(1, 0);
        // Sleep..
        solo.sleep(MEDIUM_WAIT);

        // Atrás haciendo click en el logo
        solo.clickOnView(solo.getView(R.id.ivLogoCLC));

        // Click otra vez Información
        solo.clickInList(1, 0);
        // Sleep..
        solo.sleep(SHORT_WAIT);

        // Press menu back key
        solo.goBack();

    }

    /**
     *
     */
    private void aranceles() {

        // Click on Aranceles
        solo.clickInList(2, 0);
        // Sleep...
        solo.sleep(MEDIUM_WAIT);
        // Press menu back key
        solo.goBack();

        // Click otra vez
        solo.clickInList(2, 0);
        // Sleep..
        solo.sleep(SHORT_WAIT);

        // Atrás haciendo click en el logo
        solo.clickOnView(solo.getView(R.id.ivLogoCLC));

    }

    /**
     *
     */
    private void vivirMejor() {

        // Click on VivirMejor
        solo.clickInList(3, 0);
        // Sleep...
        solo.sleep(LONG_WAIT);
        // Press menu back key
        solo.goBack();
        //
        // // Click otra vez
        // solo.clickInList(3, 0);
        // // Sleep..
        // solo.sleep(LONG_WAIT);
        //
        // // Atrás haciendo click en el logo
        // solo.clickOnView(solo.getView(R.id.ivLogoCLC));

    }

    /**
     * 
     */
    private void configuracion() {

        Log.i(TAG, "configuracion");

        // Click on Configuración
        solo.clickInList(4, 0);
        // Sleep...
        solo.sleep(SHORT_WAIT);

        // Click on Empty Text View
        solo.clickOnView(solo.getView(R.id.etTelefono));
        // Sleep...
        solo.sleep(SHORT_WAIT);

        final String[] tlfs = { "123456", "654653208544147" };

        for (final String str : tlfs) {

            solo.clearEditText((android.widget.EditText) solo.getView(R.id.etTelefono));
            // Sleep...
            solo.sleep(SHORT_WAIT);
            // Enter the text
            solo.enterText((android.widget.EditText) solo.getView(R.id.etTelefono), str);

            // Sleep...
            solo.sleep(MEDIUM_WAIT);
            // Click on Activar
            solo.clickOnView(solo.getView(R.id.tvActivar));

            // Wait for dialog
            solo.waitForDialogToOpen(LONG_WAIT);
            // Sleep...
            solo.sleep(LONG_WAIT);
            // Click on Aceptar
            solo.clickOnView(solo.getView(android.R.id.button1));
            // Sleep...
            solo.sleep(SHORT_WAIT);

        }

        // Click on Desactivar
        solo.clickOnView(solo.getView(R.id.tvDesactivar));
        // Wait for dialog
        solo.waitForDialogToOpen(5000);
        // Sleep...
        solo.sleep(SHORT_WAIT);
        // Click on Aceptar
        solo.clickOnView(solo.getView(android.R.id.button1));
        // Sleep...
        solo.sleep(SHORT_WAIT);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivLogoCLC));
        // Sleep...
        solo.sleep(SHORT_WAIT);
    }

    /**
     * Cómo llegar...
     */
    private void comoLlegar() {

        Log.i(TAG, "comoLlegar");

        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivComoLlegar));
        // Sleep...
        solo.sleep(SHORT_WAIT);

    }

    /**
     * Botón home
     */
    private void home() {

        Log.i(TAG, "home");

        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep...
        solo.sleep(SHORT_WAIT);
    }

    /**
     *
     */
    private void pedirCita() {

        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivPedirCita));
        // Sleep...
        solo.sleep(MEDIUM_WAIT);

    }

    private void usuario() {

        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivUsuario));
        // Sleep...
        solo.sleep(MEDIUM_WAIT);

    }

    private void usuarioLogin() {

        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivUsuario));
        // Sleep...
        solo.sleep(MEDIUM_WAIT);
        solo.clickOnView(solo.getView(R.id.tvIngresar));
        // Wait for dialog
        solo.waitForDialogToOpen(MEDIUM_WAIT);
        // Sleep for 1144 milliseconds
        solo.sleep(MEDIUM_WAIT);

        // Click on Aceptar
        solo.clickOnView(solo.getView(android.R.id.button1));
        // Sleep...
        solo.sleep(MEDIUM_WAIT);

        // Click on Empty Text View
        solo.clickOnView(solo.getView(R.id.etRUT));
        // Sleep...
        solo.sleep(MEDIUM_WAIT);
        // Enter the text: '13068'
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.etRUT));
        solo.sleep(MEDIUM_WAIT);
        solo.enterText((android.widget.EditText) solo.getView(R.id.etRUT), "13068");

        // Wait for dialog
        solo.waitForDialogToOpen(MEDIUM_WAIT);
        // Sleep...
        solo.sleep(MEDIUM_WAIT);

    }
    /**
     * 
     */
    private void comoLlegarHome() {

        for (int i = 0; i < 10; i++) {
            Log.i(TAG, "testComoLlegarHome,iteración: " + i);
            comoLlegar();
            home();
        }
    }

    public void ttttestStart() {

        // Sleep for 1852 milliseconds
        solo.sleep(1852);
        // Click on Aranceles
        solo.clickInList(2, 0);
        // Sleep for 11249 milliseconds
        solo.sleep(11249);
        // Press menu back key
        solo.goBack();
        // Sleep for 1612 milliseconds
        solo.sleep(1612);
        // Click on Vivir mejor
        solo.clickInList(3, 0);
        // Sleep for 36986 milliseconds
        solo.sleep(36986);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivComoLlegar));
        // Sleep for 5991 milliseconds
        solo.sleep(5991);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivOpciones));
        // Sleep for 10000 milliseconds
        // solo.sleep(10000);
        // Click on SIGUIENTE
        // solo.clickOnWebElement(By.textContent("SIGUIENTE"));
        // Sleep for 2452 milliseconds
        solo.sleep(2452);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivLogoCLC));
        // Sleep for 2202 milliseconds
        solo.sleep(2202);
        // Click on Configuración
        solo.clickInList(4, 0);
        // Sleep for 2370 milliseconds
        solo.sleep(2370);
        // Click on Empty Text View
        solo.clickOnView(solo.getView(R.id.etTelefono));
        // Sleep for 9541 milliseconds
        solo.sleep(9541);
        // Enter the text: '654653208544147'
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.etTelefono));
        solo.enterText((android.widget.EditText) solo.getView(R.id.etTelefono), "654653208544147");
        // Sleep for 1407 milliseconds
        solo.sleep(1407);
        // Click on Activar
        solo.clickOnView(solo.getView(R.id.tvActivar));
        // Wait for dialog
        solo.waitForDialogToOpen(5000);
        // Sleep for 2585 milliseconds
        solo.sleep(2585);
        // Click on Aceptar
        solo.clickOnView(solo.getView(android.R.id.button1));
        // Sleep for 1500 milliseconds
        solo.sleep(1500);
        // Click on Desactivar
        solo.clickOnView(solo.getView(R.id.tvDesactivar));
        // Wait for dialog
        solo.waitForDialogToOpen(5000);
        // Sleep for 1626 milliseconds
        solo.sleep(1626);
        // Click on Aceptar
        solo.clickOnView(solo.getView(android.R.id.button1));
        // Sleep for 2012 milliseconds
        solo.sleep(2012);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivLogoCLC));
        // Sleep for 2663 milliseconds
        solo.sleep(2663);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivComoLlegar));
        // Sleep for 1946 milliseconds
        solo.sleep(1946);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep for 2514 milliseconds
        solo.sleep(2514);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivReservas));
        // Wait for dialog
        solo.waitForDialogToOpen(5000);
        // Sleep for 1801 milliseconds
        solo.sleep(1801);
        // Click on Cancelar
        solo.clickOnView(solo.getView(android.R.id.button2));
        // Wait for dialog to close
        solo.waitForDialogToClose(5000);
        // Sleep for 2460 milliseconds
        solo.sleep(2460);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivPedirCita));
        // Sleep for 11820 milliseconds
        solo.sleep(11820);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep for 3748 milliseconds
        solo.sleep(3748);

        // Click on ImageView
        // solo.clickOnView(solo.getView(R.id.ivFacebook));
        // //Sleep for 6435 milliseconds
        // solo.sleep(6435);

        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivComoLlegar));
        // Sleep for 1389 milliseconds
        solo.sleep(1389);
        // Click on ImageView
        // solo.clickOnView(solo.getView(R.id.ivUbicacionEstoril));
        // //Sleep for 6111 milliseconds
        // solo.sleep(6111);

        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivUsuario));
        // Sleep for 1597 milliseconds
        solo.sleep(1597);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep for 1012 milliseconds
        solo.sleep(1012);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivUsuario));
        // Sleep for 1368 milliseconds
        solo.sleep(1368);
        // Click on Ingresar
        solo.clickOnView(solo.getView(R.id.tvIngresar));
        // Wait for dialog
        solo.waitForDialogToOpen(5000);
        // Sleep for 1144 milliseconds
        solo.sleep(1144);
        // Click on Aceptar
        solo.clickOnView(solo.getView(android.R.id.button1));
        // Sleep for 844 milliseconds
        solo.sleep(844);
        // Click on Empty Text View
        solo.clickOnView(solo.getView(R.id.etRUT));
        // Sleep for 8631 milliseconds
        solo.sleep(8631);
        // Enter the text: '130688624'
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.etRUT));
        solo.enterText((android.widget.EditText) solo.getView(R.id.etRUT), "130688624");
        // Enter the text: '13068862-4'
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.etRUT));
        solo.enterText((android.widget.EditText) solo.getView(R.id.etRUT), "13068862-4");
        // Sleep for 902 milliseconds
        solo.sleep(902);
        // Click on Empty Text View
        solo.clickOnView(solo.getView(R.id.etClaveRUT));
        // Sleep for 7596 milliseconds
        solo.sleep(7596);
        // Enter the text: 'mar1976'
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.etClaveRUT));
        solo.enterText((android.widget.EditText) solo.getView(R.id.etClaveRUT), "mar1976");
        // Sleep for 869 milliseconds
        solo.sleep(869);
        // Click on Ingresar
        solo.clickOnView(solo.getView(R.id.tvIngresar));
        // Sleep for 68307 milliseconds
        solo.sleep(68307);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep for 1015 milliseconds
        solo.sleep(1015);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivUsuario));
        // Sleep for 118876 milliseconds
        solo.sleep(118876);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivPedirCita));
        // Sleep for 1589 milliseconds
        solo.sleep(1589);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep for 1610 milliseconds
        solo.sleep(1610);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivComoLlegar));
        // Sleep for 1249 milliseconds
        solo.sleep(1249);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivOpciones));
        // Sleep for 1840 milliseconds
        solo.sleep(1840);
        // Click on Configuración
        solo.clickInList(4, 0);
        // Sleep for 3982 milliseconds
        solo.sleep(3982);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));
        // Sleep for 850 milliseconds
        solo.sleep(850);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivOpciones));
        // Sleep for 1677 milliseconds
        solo.sleep(1677);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivUsuario));
        // Sleep for 4670 milliseconds
        solo.sleep(4670);
        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivHome));

    }

    public void ttttestLogin() {

        // //Click on ImageView
        // solo.clickOnView(solo.getView(R.id.ivUsuario));
        // //Sleep for 1368 milliseconds
        // solo.sleep(1368);
        // //Click on Ingresar
        // solo.clickOnView(solo.getView(R.id.tvIngresar));
        // //Wait for dialog
        // solo.waitForDialogToOpen(5000);
        // //Sleep for 1144 milliseconds
        // solo.sleep(1144);
        // //Click on Aceptar
        // solo.clickOnView(solo.getView(android.R.id.button1));
        // //Sleep for 844 milliseconds
        // solo.sleep(844);
        // //Click on Empty Text View
        // solo.clickOnView(solo.getView(R.id.etRUT));
        // //Sleep for 8631 milliseconds
        // solo.sleep(8631);
        // //Enter the text: '130688624'
        // solo.clearEditText((android.widget.EditText) solo.getView(R.id.etRUT));
        // solo.enterText((android.widget.EditText) solo.getView(R.id.etRUT), "130688624");
        // //Enter the text: '13068862-4'
        // solo.clearEditText((android.widget.EditText) solo.getView(R.id.etRUT));
        // solo.enterText((android.widget.EditText) solo.getView(R.id.etRUT), "13068862-4");
        // //Sleep for 902 milliseconds
        // solo.sleep(902);
        // //Click on Empty Text View
        // solo.clickOnView(solo.getView(R.id.etClaveRUT));
        // //Sleep for 7596 milliseconds
        // solo.sleep(7596);
        // //Enter the text: 'mar1976'
        // solo.clearEditText((android.widget.EditText) solo.getView(R.id.etClaveRUT));
        // solo.enterText((android.widget.EditText) solo.getView(R.id.etClaveRUT), "mar1976");
        // //Sleep for 869 milliseconds
        // solo.sleep(869);
        // //Click on Ingresar
        // solo.clickOnView(solo.getView(R.id.tvIngresar));
        // //Sleep for 68307 milliseconds
        // solo.sleep(68307);

    }

    public void ttttestErrorLogin() {

        // Click on ImageView
        solo.clickOnView(solo.getView(R.id.ivUsuario));
        // Sleep for 1500 milliseconds
        solo.sleep(1500);
        // Click on Ingresar
        solo.clickOnView(solo.getView(R.id.tvIngresar));
        // Wait for dialog
        solo.waitForDialogToOpen(5000);
        // Sleep for 1144 milliseconds
        solo.sleep(1144);
        // Click on Aceptar
        solo.clickOnView(solo.getView(android.R.id.button1));
        // Sleep for 500 milliseconds
        solo.sleep(500);
        // Click on Empty Text View
        solo.clickOnView(solo.getView(R.id.etRUT));

        // Enter the text: '130688624'
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.etRUT));
        solo.enterText((android.widget.EditText) solo.getView(R.id.etRUT), "130688624");

        // Click on Empty Text View
        solo.clickOnView(solo.getView(R.id.etClaveRUT));
        // Sleep for 7596 milliseconds
        solo.sleep(7596);
        // Enter the text: 'mar1976'
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.etClaveRUT));
        solo.enterText((android.widget.EditText) solo.getView(R.id.etClaveRUT), "mar1976");
        solo.clearEditText((android.widget.EditText) solo.getView(R.id.etClaveRUT));

        // Click on Ingresar
        solo.clickOnView(solo.getView(R.id.tvIngresar));

        // Sleep for 25000 milliseconds
        solo.sleep(25000);

    }

    @Override
    public void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
    }

}
