package es.ingenia.clc.app.test;

import junit.framework.TestCase;
import android.test.suitebuilder.annotation.SmallTest;

import es.ingenia.clc.app.activities.NumberAdder;


/**
 * Created by David Díaz on 02/10/14.
 */
public class NumberAdderTest extends TestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    /**
     *
     */
    @SmallTest
    public void testNumberAdder() {

        int result = NumberAdder.addNumbers(2, 5);

        // Expected answer, real answer
        assertEquals(7, result);

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}
