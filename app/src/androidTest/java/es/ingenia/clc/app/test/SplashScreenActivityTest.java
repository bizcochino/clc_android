package es.ingenia.clc.app.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;
import es.ingenia.clc.app.activities.SplashScreenActivity;

/**
 * Created by David Díaz on 02/10/14.
 */
public class SplashScreenActivityTest extends ActivityInstrumentationTestCase2<SplashScreenActivity> {

    SplashScreenActivity splashActivity;

    public SplashScreenActivityTest() {

        super(SplashScreenActivity.class);

    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        splashActivity = getActivity();
    }

    @SmallTest
    public void testOpcionesMenuNotNull() throws Exception {

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

}
