package es.ingenia.clc.app.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;

import com.robotium.solo.Solo;

import es.ingenia.clc.app.R;
import es.ingenia.clc.app.activities.MainActivity;
import es.ingenia.clc.app.view.ToolbarImageView;

/**
 * Created by David Díaz on 02/10/14.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {

    /**
     * Para trazas.
     */
    private static final String TAG = "MainActivityTest";

    MainActivity                mainActivity;

    private Solo                mSolo;

    public MainActivityTest() {

        super(MainActivity.class);

    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mainActivity = getActivity();
        mSolo = new Solo(getInstrumentation(), mainActivity);
    }

    @SmallTest
    public void testOpcionesMenuNotNull() throws Exception {

        mSolo.unlockScreen();

        ToolbarImageView textView = (ToolbarImageView) mainActivity.findViewById(R.id.ivOpciones);
        assertNotNull(textView);

        textView = (ToolbarImageView) mainActivity.findViewById(R.id.ivComoLlegar);
        assertNotNull(textView);
        textView = (ToolbarImageView) mainActivity.findViewById(R.id.ivHome);
        assertNotNull(textView);
        textView = (ToolbarImageView) mainActivity.findViewById(R.id.ivPedirCita);
        assertNotNull(textView);
        textView = (ToolbarImageView) mainActivity.findViewById(R.id.ivUsuario);
        assertNotNull(textView);

    }

    @SmallTest
    public void testRobotium() throws Exception {

        mSolo.clickOnImage(0);
        mSolo.goBack();
    }

    @SmallTest
    public void testTapPedirCita() throws Exception {

        final ToolbarImageView textView = (ToolbarImageView) mainActivity.findViewById(R.id.ivPedirCita);
        // TouchUtils.tapView(this, textView);

    }

    @Override
    protected void tearDown() throws Exception {

        // try {
        // mSolo.finalize();
        // } catch (final Throwable th) {
        // Log.e(TAG, Log.getStackTraceString(th));
        // }
        // mainActivity.finish();

        super.tearDown();
    }
}
