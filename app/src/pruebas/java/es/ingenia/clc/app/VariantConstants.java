package es.ingenia.clc.app;

/**
 * Clase de constantes para el entorno de pruebas.
 *
 * @author David Díaz
 * @version 1.0, 04/09/2014
 */
public final class VariantConstants {

    /**
     * Constructor privado.
     */
    private VariantConstants() {

    }

    /**
     * Tiempo mínimo para la sincronización de banners (un día, en milisegundos)
     */
    public static final long    BANNERS_SINC_TIMEOUT        = 24 * 60 * 60 * 1000 / 2400;                                                                      // Trampa
                                                                                                                                                                // para
                                                                                                                                                                // hacerlo
                                                                                                                                                                // cada
                                                                                                                                                                // hora

    /**
     * Tiempo mínimo para la sincronización de banners (un día, en milisegundos)
     */
    public static final long    CITAS_SINC_TIMEOUT          = 24 * 60 * 60 * 1000 / 192;                                                                       // Trampa
                                                                                                                                                                // para
                                                                                                                                                                // hacerlo
                                                                                                                                                                // cada
                                                                                                                                                                // dos
                                                                                                                                                                // por
                                                                                                                                                                // tres

    /**
     * Para permitir mostrar trazas adicionales.
     */
    public static final boolean DEBUG                       = true;

    /**
     * Para permitir debug de las peticiones a los servicios web.
     */
    public static final boolean HTTP_TRANSPORT_DEBUG        = true;                                                                                            // true;

    /**
     * Url para la llamada de emergencia
     */
    public static final String  WSDL_URL_RESCUE_CALL        = "https://www.miclc.cl/aplicaciones/wsappclc/appclc.asmx?op=GISEmergencia";

    /**
     * Url para el servicio de banners
     */
    public static final String  WSDL_URL_BANNERS            = "http://198.41.35.14/tst/Internet/aplicaciones/wsappclc/appclc.asmx?op=RecuperaBanners";
    /**
     * Url para login
     */
    public static final String  WSDL_URL_LOGIN              = "http://198.41.35.14/tst/Internet/aplicaciones/wsappclc/appclc.asmx?op=InicioSesion";

    /**
     * Url para recuperar el grupo de familiares
     */
    public static final String  WSDL_URL_GRUPO_FAMILIAR     = "http://198.41.35.14/tst/Internet/aplicaciones/wsappclc/appclc.asmx?op=RecuperaGrupoFamiliar";

    /**
     * Url para recuperar las próximas citas
     */
    public static final String  WSDL_URL_PROXIMAS_CITAS     = "http://198.41.35.14/tst/Internet/aplicaciones/wsappclc/appclc.asmx?op=RecuperaProximasCitas";

    /**
     * Url para recuperar los tipos de prestaciones
     */
    public static final String  WSDL_URL_TIPOS_PRESTACIONES = "http://198.41.35.14/tst/internet/aplicaciones/wsappclc/AppCLCArancel.asmx?op=TiposPrestaciones";

    /**
     * Usuario para el formulario de acceso.
     */
    public static final String  MOCKUP_USER                 = ""; //"130688624";

    /**
     * Contraseña para el formulario de acceso.
     */
    public static final String  MOCKUP_PASSWORD             = "mar1976";

}
